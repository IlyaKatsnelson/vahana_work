/**
** License: BSD-new
** ===============================================================================
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the <organization> nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
**
** License: GPLv2
** ===============================================================================
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**
** ---------------------------------------------------------------------------
**/

/*
 * Kvaser Linux Canlib
 * Have several threads that open and close the same can channel.
 */

#include <canlib.h>

#include <signal.h>
#include <unistd.h>

#include <atomic>
#include <chrono>
#include <thread>
#include <vector>

using namespace std::literals;  // NOLINT

#define ALARM_INTERVAL_IN_S     (1)

std::atomic_bool exit_flag{false};
static unsigned int msgCounter = 0;
static unsigned int openCounter = 0;

static void check(canHandle channel, std::string id, canStatus stat)
{
  if (stat != canOK) {
    char buf[50];
    buf[0] = '\0';
    canGetErrorText(stat, buf, sizeof(buf));
    printf("[%d] %s: failed, stat=%d (%s)\n", channel, id.c_str(), (int)stat, buf);
  }
}

static void sighand(int sig)
{
  static unsigned int last_msg = 0;
  static unsigned int last_open = 0;

  switch (sig) {
  case SIGINT:
    exit_flag.store(true);
    break;
  case SIGALRM:
    if (msgCounter - last_msg) {
      printf("msg/s = %d, total=%d\n",
             (msgCounter - last_msg) / ALARM_INTERVAL_IN_S, msgCounter);
    }
    last_msg = msgCounter;

    if (openCounter - last_open) {
      printf("open+close/s = %d, total=%d\n",
             (openCounter - last_open) / ALARM_INTERVAL_IN_S, openCounter);
    }
    last_open = openCounter;
    alarm(ALARM_INTERVAL_IN_S);
    break;
  }
}

static void printUsageAndExit(char *prgName)
{
  printf("Usage: '%s <channel>'\n", prgName);
  exit(1);
}

static bool worker(canHandle hnd, const long id) {
  char msg[8] = "Kvaser!";
  unsigned long loc_msg_count{0};

  printf("Start writing on: %d[%ld]\n", hnd, id);

  //auto start = std::chrono::steady_clock::now();

  while (!exit_flag.load()) {
    auto stat = canWrite(hnd, id, msg, sizeof(msg) / sizeof(msg[0]), canMSG_STD);
    if (stat != canOK) {
      printf("\ncanWriteWait error: %d\n", errno);
      check(hnd, "canWriteWait", stat);
    }
    if (stat == canOK) {
      msgCounter++;
      loc_msg_count++;

      uint32_t tmp = 0;
      stat = canIoCtl(hnd, canIOCTL_GET_TX_BUFFER_LEVEL, &tmp, sizeof(tmp));
      if (stat != canOK) {
        check(hnd, "TX_LEVEL", stat);
      }

      // if ((loc_msg_count % 10) == 0) {
      //   auto end= std::chrono::steady_clock::now();
      //   printf("[%ld] %ld us|%d\n",
      //          id, std::chrono::duration_cast<std::chrono::microseconds>(end - start).count(), tmp);
      // }
    }
    std::this_thread::sleep_for(10ms);
  }

  return true;
}

int main(int argc, char *argv[])
{
  uint8_t channel_num = 0;
  const uint8_t thread_num = 3;
  canHandle hnd[thread_num];
  canStatus stat[thread_num];
  int bitrate = canBITRATE_500K;

  std::vector<std::thread> threads{};

  if (argc != 2) {
    printUsageAndExit(argv[0]);
  }

  {
    char *endPtr = NULL;
    errno = 0;
    channel_num = strtol(argv[1], &endPtr, 10);
    if ( (errno != 0) || ((channel_num == 0) && (endPtr == argv[1])) ) {
      printUsageAndExit(argv[0]);
    }
  }

  /* Use sighand as our signal handler */
  signal(SIGALRM, sighand);
  signal(SIGINT, sighand);
  alarm(ALARM_INTERVAL_IN_S);

  /* Allow signals to interrupt syscalls */
  siginterrupt(SIGINT, 1);

  canInitializeLibrary();

  for (uint8_t i=0; i<thread_num; i++) {
    /* Open channel, set parameters and go on bus */
    hnd[i] = canOpenChannel(channel_num, canOPEN_REQUIRE_EXTENDED | canOPEN_ACCEPT_VIRTUAL);
    if (hnd[i] < 0) {
      printf("canOpenChannel %d", channel_num);
      check(0, "", static_cast<canStatus>(hnd[i]));
      return -1;
    }
    stat[i] = canSetBusParams(hnd[i], bitrate, 0, 0, 0, 0, 0);
    check(hnd[i], "canSetBusParams", stat[i]);
    if (stat[i] != canOK) {
      goto ErrorExit;
    }
    stat[i] = canBusOn(hnd[i]);
    check(hnd[i], "canBusOn", stat[i]);
    if (stat[i] != canOK) {
      goto ErrorExit;
    }

    int data = 10; // Set resolution to 10 us
    stat[i] = canIoCtl(hnd[i], canIOCTL_SET_TIMER_SCALE, &data, sizeof(data));
    if (stat[i] != canOK) {
      check(hnd[i], "canIoCtl() failed to set 1 ms resolution on the second channel", stat[i]);
    }

    printf("Writing messages on channel %d\n", i);

    // Start communication threads
    threads.emplace_back(std::thread{worker, hnd[i], channel_num+100});
  }

  std::srand(std::time(0)); // use current time as seed for random generator

  while (!exit_flag.load()) {
    auto hnd = canOpenChannel(channel_num, canOPEN_REQUIRE_EXTENDED | canOPEN_ACCEPT_VIRTUAL);
    if (hnd < 0) {
      printf("canOpenChannel %d", channel_num);
      check(0, "", static_cast<canStatus>(hnd));
      continue;
    }
    openCounter++;
    int random_variable = std::rand();

    std::this_thread::sleep_for(std::chrono::microseconds(random_variable / 100000));
    auto stat = canClose(hnd);
    check(hnd, "canClose", stat);

    std::this_thread::sleep_for(10ms);
  }

  for (auto &t : threads) {
    t.join();
  }
  sighand(SIGALRM);

ErrorExit:

  alarm(0);
  for (uint8_t i=0; i<channel_num; i++) {
    stat[i] = canBusOff(hnd[i]);
    check(hnd[i], "canBusOff", stat[i]);
    stat[i] = canClose(hnd[i]);
    check(hnd[i], "canClose", stat[i]);
  }

  printf("total=%d\n", msgCounter);

  canUnloadLibrary();

  return 0;
}
