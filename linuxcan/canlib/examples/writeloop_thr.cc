/**
** License: BSD-new
** ===============================================================================
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the <organization> nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
**
** License: GPLv2
** ===============================================================================
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**
** ---------------------------------------------------------------------------
**/

/*
 * Kvaser Linux Canlib
 * Send out CAN messages on multiple channels using multiple threads until ctrl-c is pressed
 */

#include <canlib.h>

#include <signal.h>
#include <string.h>
#include <unistd.h>

#include <atomic>
#include <chrono>
#include <mutex>
#include <thread>
#include <vector>

using namespace std::literals;  // NOLINT

#define ALARM_INTERVAL_IN_S     (1)
#define WRITE_WAIT_INFINITE     (unsigned long)(-1)
#define WRITE_WAIT_10MS         (10)

static unsigned int msgCounter = 0;

std::atomic_bool exit_flag{false};

static void check(canHandle channel, std::string id, canStatus stat)
{
  if (stat != canOK) {
    char buf[50];
    buf[0] = '\0';
    canGetErrorText(stat, buf, sizeof(buf));
    printf("[%d] %s: failed, stat=%d (%s)\n", channel, id.c_str(), (int)stat, buf);
  }
}

static void sighand(int sig)
{
  static unsigned int last = 0;

  switch (sig) {
  case SIGINT:
    exit_flag.store(true);
    break;
  case SIGALRM:
    if (msgCounter - last) {
      printf("msg/s = %d, total=%d\n",
             (msgCounter - last) / ALARM_INTERVAL_IN_S, msgCounter);
    }
    last = msgCounter;
    alarm(ALARM_INTERVAL_IN_S);
    break;
  }
}

static void printUsageAndExit(char *prgName)
{
  printf("Usage: '%s <bitrate>'\n", prgName);
  exit(1);
}

static bool worker_wr(canHandle hnd, const long id, std::mutex * lock) {
  char msg[8] = "Kvaser!";
  unsigned long loc_msg_count{0};
  unsigned int txErr, rxErr, ovErr;
  unsigned long flags{0};

  printf("Start writing on: %d[%ld]\n", hnd, id);

  auto start = std::chrono::steady_clock::now();

  while (!exit_flag.load()) {
    std::lock_guard<std::mutex> guard(*lock);

    auto stat = canWrite(hnd, id, msg, sizeof(msg) / sizeof(msg[0]), canMSG_STD);
    if (stat != canOK) {
      printf("canWriteWait error: %d\n", errno);
      check(hnd, "canWriteWait", stat);
    }
    if (stat == canOK) {
      msgCounter++;
      loc_msg_count++;

      if ((loc_msg_count % 1000) == 0) {
        auto end= std::chrono::steady_clock::now();
        printf("WR[%ld] %ld us|0x%lX\n",
               id, std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()/1000, flags);
      }
      if ((loc_msg_count % 10) == 0) {
        // Error accounting
        auto rc = canReadErrorCounters(hnd, &txErr, &rxErr, &ovErr);
        if (rc == canOK) {
          rc = canReadStatus(hnd, &flags);
        }
      }
    }
    std::this_thread::sleep_for(2ms);
  }

  return true;
}

static bool worker_rd(canHandle hnd, std::mutex * lock) {
  unsigned long loc_msg_count{0};
  unsigned long loc_tmo_count{0};

  int64_t id;
  uint8_t data[8];
  uint32_t dlc, flags;
  uint64_t timestamp;

  printf("Start reading from:[%d]\n", hnd);
  auto start = std::chrono::steady_clock::now();

  while (!exit_flag.load()) {
    {
      std::lock_guard<std::mutex> guard(*lock);

      auto rc = canReadSync(hnd, 2);
      if (rc == canERR_TIMEOUT) {
        loc_tmo_count++;
        continue;
      } else {
        rc = canRead(hnd, &id, data, &dlc, &flags, &timestamp);
        if (rc == canOK) {
          loc_msg_count++;
        } else {
          printf("Err:%d[%ld] - %d\n", hnd, id, rc);
        }
      }
    }

    if ((loc_msg_count % 1000) == 0) {
      auto end= std::chrono::steady_clock::now();
      printf("RD[%ld] %ld us|%ld %ld\n",
             id, std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()/1000, loc_msg_count, loc_tmo_count);
    }
  }
  return true;
}

static bool stat_worker(const uint8_t channel_num) {
  canInitializeLibrary();

  printf("Stat reading from:[0 - %d]\n", channel_num);
  while (!exit_flag.load()) {
    for (auto i=0;i<channel_num;i++) {
      canHandle handle = canOpenChannel(i, canOPEN_REQUIRE_EXTENDED);
      if (handle >= canOK) {
        auto rc = canRequestBusStatistics (handle);

        if (rc == canOK) {
          canBusStatistics stats;
          canGetBusStatistics(handle, &stats, sizeof(stats));

          unsigned long status = 0;
          canReadStatus(handle, &status);

          printf("STAT[%d] %ld 0x%lX\n", i, stats.stdData, status);
        } else {
          printf("STAT[%d] %d\n", i, rc);
        }
      } else {
        printf("canOpenChannel %d", i);
        check(0, "", static_cast<canStatus>(handle));
      }
    }
    std::this_thread::sleep_for(1s);
  }

  canUnloadLibrary();
  return true;
}

int main(int argc, char *argv[])
{
  const uint8_t channel_num = 4;
  canHandle wr_hnd[channel_num];
  canHandle rd_hnd[channel_num];
  std::mutex lock[channel_num];
  canStatus stat[channel_num];
  int bitrate = canBITRATE_250K;

  std::vector<std::thread> threads{};

  if (argc != 2) {
    printUsageAndExit(argv[0]);
  }

  {
    char *endPtr = NULL;
    int bitrate_param;
    errno = 0;
    bitrate_param = strtol(argv[1], &endPtr, 10);
    if ( (errno != 0) || ((bitrate_param == 0) && (endPtr == argv[1])) ) {
      printUsageAndExit(argv[0]);
    }

    // Convert to Kvaser macros
    if (bitrate_param == 1000000) {
      bitrate = canBITRATE_1M;
    } else if (bitrate_param == 500000) {
      bitrate = canBITRATE_500K;
    } else if (bitrate_param == 250000) {
      bitrate = canBITRATE_250K;
    } else if (bitrate_param == 125000) {
      bitrate = canBITRATE_125K;
    } else if (bitrate_param == 100000) {
      bitrate = canBITRATE_100K;
    } else {
      printf("Unsupported bitrate: %d\n", bitrate_param);
    }
  }

  /* Use sighand as our signal handler */
  signal(SIGALRM, sighand);
  signal(SIGINT, sighand);
  alarm(ALARM_INTERVAL_IN_S);

  /* Allow signals to interrupt syscalls */
  siginterrupt(SIGINT, 1);

  // Set scheduling
  if (0){
    struct sched_param param;
    if (sched_getparam(0, &param)) {
      printf("Cannot obtain current scheduling parameters: %s\n", strerror(errno));
      return -1;
    }

    // Set priority
    param.sched_priority = 90;

    // Set scheduler and prio
    if (sched_setscheduler(0, SCHED_RR, &param)) {
      printf("Cannot set scheduler: %s\n", strerror(errno));
      return -1;
    }
  }

  // Launch the child process to read the statuses
  auto pid = fork();
  if (pid < 0) {
    perror("fork");
    exit(-1);
  } else if (pid == 0) {
    // child
    stat_worker(channel_num);
    return 0;
  }

  canInitializeLibrary();

  for (uint8_t i=0; i<channel_num; i++) {
    /* Open channel, set parameters and go on bus */
    wr_hnd[i] = canOpenChannel(i, canOPEN_REQUIRE_EXTENDED);
    if (wr_hnd[i] < 0) {
      printf("canOpenChannel %d", i);
      check(0, "", static_cast<canStatus>(wr_hnd[i]));
      return -1;
    }
    stat[i] = canSetBusParams(wr_hnd[i], bitrate, 0, 0, 0, 0, 0);
    check(wr_hnd[i], "canSetBusParams", stat[i]);
    if (stat[i] != canOK) {
      goto ErrorExit;
    }
    stat[i] = canBusOn(wr_hnd[i]);
    check(wr_hnd[i], "canBusOn", stat[i]);
    if (stat[i] != canOK) {
      goto ErrorExit;
    }

    rd_hnd[i] = canOpenChannel(i, canOPEN_REQUIRE_EXTENDED);
    if (rd_hnd[i] < 0) {
      printf("canOpenChannel %d", i);
      check(0, "", static_cast<canStatus>(rd_hnd[i]));
      return -1;
    }
    stat[i] = canSetBusParams(rd_hnd[i], bitrate, 0, 0, 0, 0, 0);
    check(rd_hnd[i], "canSetBusParams", stat[i]);
    if (stat[i] != canOK) {
      goto ErrorExit;
    }
    stat[i] = canBusOn(rd_hnd[i]);
    check(rd_hnd[i], "canBusOn", stat[i]);
    if (stat[i] != canOK) {
      goto ErrorExit;
    }

    // Start communication threads
    threads.emplace_back(std::thread{worker_wr, wr_hnd[i], i + 100, &lock[i]});
    threads.emplace_back(std::thread{worker_rd, rd_hnd[i], &lock[i]});
  }

  while (!exit_flag.load()) {
    std::this_thread::sleep_for(1s);
  }

  for (auto &t : threads) {
    t.join();
  }
  sighand(SIGALRM);

ErrorExit:

  alarm(0);
  for (uint8_t i=0; i<channel_num; i++) {
    stat[i] = canBusOff(wr_hnd[i]);
    check(wr_hnd[i], "canBusOff", stat[i]);
    stat[i] = canClose(wr_hnd[i]);
    check(wr_hnd[i], "canClose", stat[i]);

    stat[i] = canBusOff(rd_hnd[i]);
    check(rd_hnd[i], "canBusOff", stat[i]);
    stat[i] = canClose(rd_hnd[i]);
    check(rd_hnd[i], "canClose", stat[i]);
  }
  canUnloadLibrary();

  printf("total=%d\n", msgCounter);

  return 0;
}
