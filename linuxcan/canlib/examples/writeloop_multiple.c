/**
** License: BSD-new
** ===============================================================================
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the <organization> nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
**
** License: GPLv2
** ===============================================================================
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**
** ---------------------------------------------------------------------------
**/

/*
 * Kvaser Linux Canlib
 * Send out CAN messages on multiple channels until ctrl-c is pressed
 */


#include <canlib.h>
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>                // for gettimeofday()

#define ALARM_INTERVAL_IN_S     (1)
#define WRITE_WAIT_INFINITE     (unsigned long)(-1)
#define WRITE_WAIT_10MS         (10)

static unsigned int msgCounter = 0;
static int willExit = 0;

static void check(canHandle channel, char* id, canStatus stat)
{
  if (stat != canOK) {
    char buf[50];
    buf[0] = '\0';
    canGetErrorText(stat, buf, sizeof(buf));
    printf("[%d] %s: failed, stat=%d (%s)\n", channel, id, (int)stat, buf);
  }
}

static void sighand(int sig)
{
  static unsigned int last = 0;

  switch (sig) {
  case SIGINT:
    willExit = 1;
    break;
  case SIGALRM:
    if (msgCounter - last) {
      printf("msg/s = %d, total=%d\n",
             (msgCounter - last) / ALARM_INTERVAL_IN_S, msgCounter);
    }
    last = msgCounter;
    alarm(ALARM_INTERVAL_IN_S);
    break;
  }
}

static void printUsageAndExit(char *prgName)
{
  printf("Usage: '%s <bitrate>'\n", prgName);
  exit(1);
}

int main(int argc, char *argv[])
{
  const uint8_t channel_num = 4;
  canHandle hnd[channel_num];
  canStatus stat[channel_num];
  char msg[8] = "Kvaser!";
  int bitrate = canBITRATE_250K;

  if (argc != 2) {
    printUsageAndExit(argv[0]);
  }

  {
    char *endPtr = NULL;
    int bitrate_param;
    errno = 0;
    bitrate_param = strtol(argv[1], &endPtr, 10);
    if ( (errno != 0) || ((bitrate_param == 0) && (endPtr == argv[1])) ) {
      printUsageAndExit(argv[0]);
    }

    // Convert to Kvaser macros
    if (bitrate_param == 1000000) {
      bitrate = canBITRATE_1M;
    } else if (bitrate_param == 500000) {
      bitrate = canBITRATE_500K;
    } else if (bitrate_param == 250000) {
      bitrate = canBITRATE_250K;
    } else if (bitrate_param == 125000) {
      bitrate = canBITRATE_125K;
    } else if (bitrate_param == 100000) {
      bitrate = canBITRATE_100K;
    } else {
      printf("Unsupported bitrate: %d\n", bitrate_param);
    }
  }

  /* Use sighand as our signal handler */
  signal(SIGALRM, sighand);
  signal(SIGINT, sighand);
  alarm(ALARM_INTERVAL_IN_S);

  /* Allow signals to interrupt syscalls */
  siginterrupt(SIGINT, 1);

  for (uint8_t i=0; i<channel_num; i++) {
    /* Open channel, set parameters and go on bus */
    hnd[i] = canOpenChannel(i, canOPEN_REQUIRE_EXTENDED | canOPEN_ACCEPT_VIRTUAL);
    if (hnd[i] < 0) {
      printf("canOpenChannel %d", i);
      check(0, "", hnd[i]);
      return -1;
    }
    stat[i] = canSetBusParams(hnd[i], bitrate, 0, 0, 0, 0, 0);
    check(hnd[i], "canSetBusParams", stat[i]);
    if (stat[i] != canOK) {
      goto ErrorExit;
    }
    stat[i] = canBusOn(hnd[i]);
    check(hnd[i], "canBusOn", stat[i]);
    if (stat[i] != canOK) {
      goto ErrorExit;
    }

    int data = 10; // Set resolution to 10 us
    stat[i] = canIoCtl(hnd[i], canIOCTL_SET_TIMER_SCALE, &data, sizeof(data));
    if (stat[i] != canOK) {
      check(hnd[i], "canIoCtl() failed to set 1 ms resolution on the second channel", stat[i]);
    }

    printf("Writing messages on channel %d\n", i);
  }

  struct timeval t1, t2;
  double elapsedTime;

  // start timer
  gettimeofday(&t1, NULL);

  while (!willExit) {
    for (uint8_t i=0; i<channel_num; i++) {
      long id = i + 100;

      stat[i] = canWriteWait(hnd[i], id, msg, sizeof(msg) / sizeof(msg[0]), canMSG_EXT, WRITE_WAIT_10MS);
      if (stat[i] != canOK) {
        printf("\ncanWriteWait error: %d\n", errno);
        check(hnd[i], "canWriteWait", stat[i]);
      }
      if (stat[i] == canOK) {
        msgCounter++;

        gettimeofday(&t2, NULL);
        elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
        elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
        printf("[%d] %f ms\n", i, elapsedTime);

      }
    }
  }

  sighand(SIGALRM);

ErrorExit:

  alarm(0);
  for (uint8_t i=0; i<channel_num; i++) {
    stat[i] = canBusOff(hnd[i]);
    check(hnd[i], "canBusOff", stat[i]);
    stat[i] = canClose(hnd[i]);
    check(hnd[i], "canClose", stat[i]);
  }

  printf("total=%d\n", msgCounter);

  return 0;
}
