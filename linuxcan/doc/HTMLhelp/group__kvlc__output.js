var group__kvlc__output =
[
    [ "kvlcGetFirstWriterFormat", "group__kvlc__output.html#ga80ba935c3df3c1b0ad33c3897d7032f6", null ],
    [ "kvlcGetNextWriterFormat", "group__kvlc__output.html#ga13e18fab7981c6814e3c922a784b9ed9", null ],
    [ "kvlcGetWriterDescription", "group__kvlc__output.html#gae1b15aafd380119fc3e3091cb08912f9", null ],
    [ "kvlcGetWriterExtension", "group__kvlc__output.html#ga026dc681eace7fbe133ce685bd0c832a", null ],
    [ "kvlcGetWriterName", "group__kvlc__output.html#gad1662e8a5b3a7bdf4d9066597b1d8a66", null ],
    [ "kvlcIsPropertySupported", "group__kvlc__output.html#ga2f671daa7746e63fb8697f914904f19e", null ]
];