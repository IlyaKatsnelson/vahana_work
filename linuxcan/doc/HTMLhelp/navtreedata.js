var NAVTREE =
[
  [ "Welcome to Kvaser Linux Drivers and SDK!", "index.html", [
    [ "CAN bus API (CANlib)", "page_canlib.html", "page_canlib" ],
    [ "LIN bus API (LINlib)", "page_linlib.html", [
      [ "Using the LIN Bus", "page_linlib.html#section_user_guide_lin_intro", null ]
    ] ],
    [ "Database API (kvaDbLib)", "page_kvadblib.html", [
      [ "Description", "page_kvadblib.html#section_user_guide_kvadblib_1", null ],
      [ "Naming convention", "page_kvadblib.html#section_user_guide_kvadblib_2", null ],
      [ "Build an application", "page_kvadblib.html#section_user_guide_kvadblib_3", [
        [ "Example", "page_kvadblib.html#section_user_guide_kvadblib_3_3", null ]
      ] ]
    ] ],
    [ "Converter API (kvlclib)", "page_kvlclib.html", [
      [ "Description", "page_kvlclib.html#section_user_guide_kvlclib_1", null ],
      [ "Naming convention", "page_kvlclib.html#section_user_guide_kvlclib_2", null ],
      [ "Build an application", "page_kvlclib.html#section_user_guide_kvlclib_3", null ]
    ] ],
    [ "Memorator API (kvmlib)", "page_kvmlib.html", [
      [ "Description", "page_kvmlib.html#section_user_guide_kvmlib_1", null ],
      [ "Naming convention", "page_kvmlib.html#section_user_guide_kvmlib_2", null ],
      [ "Build an application", "page_kvmlib.html#section_user_guide_kvmlib_3", null ]
    ] ],
    [ "Memorator XML API (kvaMemoLibXML)", "page_kvamemolibxml.html", [
      [ "Description", "page_kvamemolibxml.html#section_user_guide_kvamemolibxml_1", null ],
      [ "Build an application", "page_kvamemolibxml.html#section_user_guide_kvamemolibxml_2", null ]
    ] ],
    [ "License and Copyright", "page_license_and_copyright.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ],
    [ "Examples", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"canlib_8h.html#af5ebc7eecceb2a39a6b22371cfe4265b",
"group___l_i_n.html#ga5bf84820248e95fde2718fa46304a5a5",
"group__kvadb__nodes.html#ga0c53e72a3905ce374bb84a90e2d4533d",
"group__kvm__files.html",
"kva_memo_lib_x_m_l_8h_source.html",
"page_user_guide_dev_info.html#section_user_guide_dev_info_status"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';