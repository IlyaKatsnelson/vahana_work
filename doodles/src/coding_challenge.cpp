// --------------------------------------------------------------------------
// Coding challenge problem.
//
// Please fill in as much as possible.
// Please consider API and runtime design.
// Please describe your work.
// --------------------------------------------------------------------------

class Node {
 public:
    // ctors:
    Node() = delete;
    Node(int initialData) : m_data(initialData) {}

    // Print out node's address and data
    friend std::ostream& operator<<(std::ostream&, const Node*);

    // Add new node
    std::size_t append(Node* n);

    /* Search for node within descendants with given value
    * (IMPLEMENT ME)
    * */
    Node* find(int value);

 private:
    int m_data; // data for this node
    std::vector<Node*> m_children; // vector of pointers to child nodes
};

// IMPLEMENT
Node* Node::find(int value) {
   return nullptr;
}

std::size_t Node::append(Node* n) {
   m_children.push_back(n);
   return m_children.size();
}

// Nicely and correctly print the node
std::ostream& operator<<(std::ostream &strm, const Node *n) {
    if (n == nullptr) {
       return strm;
    }

    strm << "[" << n->m_data << "]";
    if (n->m_children.size() > 0) {
        strm << "<-[";
    }

    for (auto child : n->m_children) {
        strm << child;
    }
    if (n->m_children.size() > 0) {
        strm << "]";
    }
    return strm;
}

// --------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    // Create a forest
    auto root_value = clock();
    auto root1 = new Node(root_value);
    for (auto i=0;i<12;i++) {
       auto node = new Node(i);
       root1->append(node);
    }

    std::cout << "[--- root1 ---]" << std::endl;
    std::cout << root1 << std::endl;
    std::cout << root1->find(45) << std::endl;
    std::cout << root1->find(root_value) << std::endl;

    auto root2 = new Node(clock());
    auto child = new Node(clock());
    for (auto i=0;i<12;i++) {
       auto node = new Node(i);
       node->append(child);
       child = node;
    }
    root2->append(child);

    std::cout << "[--- root2 ---]" << std::endl;
    std::cout << root2 << std::endl;
    std::cout << root2->find(4) << std::endl;

    auto root3 = new Node(clock());
    std::cout << "[--- root3 ---]" << std::endl;
    std::cout << root3 << std::endl;
    std::cout << root3->find(4) << std::endl;

    return 0;
}
