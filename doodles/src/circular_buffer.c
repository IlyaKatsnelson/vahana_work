#include <stdio.h>

//     Create a function to push a char into a FIFO. The FIFO should be implemented
//     as a circular buffer of length 20. The FIFO will be used to cache the most
//     recent data from a data stream, therefore, drop the oldest value if the
//     buffer is full.

char fifoBuffer[BUFFER_SIZE];
char printFifoBuffer[BUFFER_SIZE];

unsigned int start = 0;    // index of the first element
unsigned int end = 0;      // index of the first empty

unsigned int distance(unsigned int start, unsigned int end) {
  if (start > end) {
    return (BUFFER_SIZE - (start - end));
  }
  return (end - start);
}

unsigned int increment(unsigned int index) {
  if (index == (BUFFER_SIZE-1))
    return 0;
  return index++;
}

void bufferPush_ISR(char data)
{
    // Answer: TODO
  // Check if empty
  if (start == end) {
    fifoBuffer[start] = data;
    end = increment(end);
    return;
  }

  // Check if full
  if (distance(start, end) == (BUFFER_SIZE -1)) {
    start = increment(start);
  }
  fifoBuffer[end] = data;
  end = increment(end);
  return;
}

//     Create a function to print out and empty the data buffer.
//     Data should be printed in order from oldest to newest, active elements only.

void printAndEmptyBuffer(void)
{
    // Answer: TODO
  // Disable the ISR to copy the data
  unsigned int size = 0;
  disableInterrupts();
  {
    for (unsigned int i=0; i<distance(start, end); i++) {
      printFifoBuffer[size] = fifoBuffer[(start + i) % BUFFER_SIZE];
      size++;
    }

    // Empty the FIFO
    start = end = 0;
  }
  enableInterrupts();

  for (unsigned int i=0; i<size; i++){
    printf("%d: %d\n", i, printFifoBuffer[i]);
  }
}

//     The function bufferPush_ISR() will be called from an interrupt service
//     routine whenever new data is available to be buffered.
//     The function printAndEmptyBuffer() will be called from a periodic task.
//     The functions disableInterrupts() and enableInterrupts() are available
//     for disabling and enabling interrupts, respectively.
//
//     In your implementations of bufferPush_ISR() and printAndEmptyBuffer(),
//     determine whether or not it is necessary to disable/enable interrupts.
//     If so, add comments where the calls are necessary. If not required,
//     briefly comment why not.


