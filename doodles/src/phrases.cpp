#include <iostream>
#include <vector>
#include <sstream>

using namespace std;

// To execute C++, please define "int main()"
int main() {
  auto words = { "Hello, ", "World!", "\n" };
  for (const string& word : words) {
    cout << word;
  }
  return 0;
}


/*
Your previous Plain Text content is preserved below:

 /*
 * "cat cat dog"     is a match for     "red red blue"
 * "cat dog cat"     is a match for     "red blue red"
 * "cat cat dog"     is a match for     "dog dog cat"
 * "cat cat dog"     is not a match for "dog cat cat"
 * "cat cat dog cat" is not a match for "red red blue blue"
 * "cat cat dog dog" is a match for     "red red blue blue"
 * "cat cat dog cat" is a match for     "red red blue red"
 */

 // "foo foo bar"
 // "cat dog pig" == "one two three"

bool match(const string & phrase1, const string & phrase2) {
  // Check for emptiness
  if (phrase1.size() == 0 || phrase2.size() == 0) {
    return false;
  }

  // Split phrases into tokens
  stringstream stream1(phrase1);
  stringstream stream2(phrase2);
  string token;


  vector<string> phrase_tokens1;
  vector<string> phrase_tokens2;

  while(getline(stream1, token, ' ')) {
    phrase_tokens1.push_back(token);
  }
  while(getline(stream2, token, ' ')) {
    phrase_tokens2.push_back(token);
  }

  if (phrase_tokens1.size() != phrase_tokens2.size()) {
    return false;
  }
  if (phrase_tokens1.size() == 1) {
    return true;
  }


  // Create a vector of token counts where each element represents the number of continuous tokens in the phrase

  // i.e. "cat cat dog cat" => [2, 1, 1]
  // "cat cat dog foo" => []

  // "cat cat dog cat" => [0, 0, 1, -1]
  // "cat cat dog foo" => [0, 0, 1, 1]
  // "red red blue blue" => [0, 0, 1, 0]

    /*
    * "cat cat dog"     is a match for     "red red blue"     [0,0,1]  [0,0,1]
    * "cat dog cat"     is a match for     "red blue red"     [0,1,-1]  [0,0,1]
    * "cat cat dog"     is a match for     "dog dog cat"
    * "cat cat dog"     is not a match for "dog cat cat"
    * "cat cat dog cat" is not a match for "red red blue blue"
    * "cat cat dog dog" is a match for     "red red blue blue"
    * "cat cat dog cat" is a match for     "red red blue red"
    */


  vector<unsigned long> phrase_tokens_cnt1{0};
  vector<unsigned long> phrase_tokens_cnt2{0};

  for (auto i = 1u; i<phrase_tokens1.size(); i++) {
    if (phrase_tokens1[i] == phrase_tokens1[i-1]) {
      phrase_tokens_cnt1.push_back(0);
    } else {
      unsigned long j = 0;
      for (j=0; j<i; j++) {
        if (phrase_tokens1[i] == phrase_tokens1[j]) {
          break;
        }
      }

      if (j == i) {
        phrase_tokens_cnt1.push_back(1);
      } else {
        phrase_tokens_cnt1.push_back(-1);
      }
    }
  }

    for (auto i = 1u; i<phrase_tokens2.size(); i++) {
    if (phrase_tokens2[i] == phrase_tokens2[i-1]) {
      phrase_tokens_cnt2.push_back(0);
    } else {
      unsigned long j = 0;
      for (j=0; j<i; j++) {
        if (phrase_tokens2[i] == phrase_tokens2[j]) {
          break;
        }
      }

      if (j == i) {
        phrase_tokens_cnt2.push_back(1);
      } else {
        phrase_tokens_cnt2.push_back(-1);
      }
    }
  }

  for (auto i = 1u; i<phrase_tokens_cnt1.size(); i++) {
    if (phrase_tokens_cnt1[i] != phrase_tokens_cnt2[i]) {
      return false;
    }
  }

  return true;
 }

