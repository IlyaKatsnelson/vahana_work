// Light sensor implementation
//

#include "light_sensor.h"

// Initialize the light sensor
enum light_sensor_status_codes lightSensorInit() {
    return LT_STATUS_OK;
}

// Read one light intensity value
enum light_sensor_status_codes lightSensorRead(float * value) {

    // TBD: read the sensor data, either I2C or SMB
    *value = 0.0;
    if (i2c_read(value) < 0) {
        return LT_STATUS_ERROR;
    }
    return LT_STATUS_OK;
}

