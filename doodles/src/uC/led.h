// LED interface
//

#ifndef LED_H_
#define LED_H_

enum led_state {
    LED_ON  = 1,
    LED_OFF = 0
};

#define LED_SWITCH_STATE(state)    (state == LED_ON ? LED_OFF : LED_ON)

enum led_status_codes {
    LED_STATUS_OK          = 0,
    LED_STATUS_ERROR       = -1
};

// Initialize LED configuration
enum led_status_codes LEDInit();

// Configure the LED
// We assume that there is only one RED and one GREEN
short LEDSetGreen(enum led_state state);
short LEDSetRed(enum led_state state);

#endif  // LED_H_