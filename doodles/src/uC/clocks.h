// CLocks interface
//

#ifndef CLOCKS_H_
#define CLOCKS_H_

enum clocks_status_codes {
    CLOCKS_STATUS_OK          = 0,
    CLOCKS_STATUS_ERROR       = -1
};

#define CLOCKS_1s   1000

// Initialize Clocks configuration
enum clocks_status_codes Clocks_init();

// Let the processor sleep for the set number of milliseconds
void Clocks_sleep(unsigned int ms);

#endif  // CLOCKS_H_