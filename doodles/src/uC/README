This is the writeup on the take home problem.

1. I've tried to make the code as modular as possible. Unfortunately, C99 isn't much help to make it really modular. For example the error codes are essentially global. In addition to that the language will not enforce the enum types.

2. Assuming the wiring has been checked, to some extent (I understand that it is not possible to 100% verify everything, but at least the basics). Write a version of the code that just blinks the LEDs and sends something simple to teh serial port, bypassing the light sensor. This will allow verification of the uC clock and peripherals. Then enable the light sensing code to verify the sensor.

3. Testing and verification has many parts:

    1. It is possible to use Google Test (https://github.com/google/googletest) to write simple mock functions to test the logic. They can also be used during CI to make sure there is no regression.

    2. Sometimes manufacturers provide SIMs for their devices. It can also be used, to some extent. This will certainly depend on how important this component is in the system (and how large is the budget).

    3. The best part is to just setup one unit to be ran continuously with the new code while monitoring the exit on the serial port. Some sort of controlled light will have to be setup to actually produce known input to the light sensor. The LED outputs could be wired to simple DIO lines on a different system to make sure the outputs are correct. As you can see, the description implies that all the testing will be automated. It is a bit of work, but I believe it will pay off later.

    4. For the main debugging the code will have to be modified. #define could be added to add debug code that will not be compiled into the final version. Serial port can be used to output some results, though it is heavy. But one LED can just be used as a serial data line.

4. To improve:

    1. it would be nice to put the light measurement into a separate HW block.
    2. Move to C11 or better - C++ - to use the language provided checks. If this is not possible the code can still be compiled by C++ compiler to check for type errors.

5. HW issues to be mitigated:

    So far I just see one possible issue with light sensor not responding in the timely manner. The code needs to adopt to handle some potential errors. If the sensor blocks for some reason the power consumption goes up. If it fails completely the FW should bypass it and perhaps flash the LEDs in some different pattern.

6. Battery usage:

I'd be honest to say that I've never done this before. Rough estimate:

    1. Light sensor: 1.8 µA operating current.
    2. uC: 150 µA/MHz at 8 MHz, 3.0 V, RAM Program Execution (Typical)
            2.1 µA at 3.0 V Standby Mode
    3. If we assume 10% running time and 90% standby mode (basically inside the sleep() function, which can be improved. I actually haven't measured this time).
        121.89 uAh for uC
        1.8 uAh for the light sensor if operating continuously (also could be optimized)
        123.69 uAh total.

        The TL-4903 battery has 2.4 Ah capacity, giving us 19403.3470774 hours = 2.2 years.

        With 2% working time the battery will last 9.8 years (good reason to optimize the code)
