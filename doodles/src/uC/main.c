/*****************************************************************************/
/* Light sensor application                                                  */
/*****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include "clocks.h"
#include "led.h"
#include "light_sensor.h"

int main(int argc, char *argv[]) {
    // We only need to know about two light levels - current and previous
    float curr_light_level = 0.0;
    float prev_light_level = 0.0;

    enum led_state green_state = LED_OFF;
    enum led_state red_state = LED_OFF;

    unsigned short green_blink_count = 0;
    unsigned short red_blink_count = 0;

    // Count the number of samples
    // Only 16-bit is used. The count will roll over
    unsigned short sample_count = 0;

    int serial_port = 0;

    // Buffer for the output message
#define BUFFER_SIZE 22
    char buffer[BUFFER_SIZE];
    int msg_size = 0;

    // Health monitoring variables
    // They should be one word in size, assuming this is a 16 bit CPU
    unsigned short sample_err_count = 0;
    unsigned short write_err_count = 0;

    // Initialize the system and peripherals
    // TBD: Initialize sys clocks
    // - Initialize DCOCLK clock to 1MHz and set to source MCLK
    // - Initialize and start 32KHz XT1CLK clock
    if (Clocks_init() != CLOCKS_STATUS_OK) {
        return -1;
    }

    // TBD: initialize the light sensor
    if (lightSensorInit() != LT_STATUS_OK) {
        return -2;
    }

    // TBD: initialize the LED
    if (LEDInit() != LED_STATUS_OK) {
        return -3;
    } else {
        LEDSetGreen(green_state);
        LEDSetRed(red_state);
    }

    // TBD: open the serial port
    // Yes, it looks a lot like Linux API, but this is to make the code more familiar and thus easier to read
    serial_port = open("/dev/tty1", O_APPEND);
    if (serial_port == -1) {
        return -4;
    }

    // Main loop
    // Since the requirement document doesn't specify what a "blink" is
    // (we will concider a "blink" to be the change in LED state and back to original)
    // and since the only output that shall be done is once a second
    // and to minimize the battery life,
    // we will run the main loop at 1Hz.
    // The "blinking" couldn't be done with no delay because the user won't see anything.
    while (1) {
        // Skip the data processing if there was an error
        if (lightSensorRead(&curr_light_level) == LT_STATUS_OK) {
            sample_count++;

            if (curr_light_level/prev_light_level >= 2.0) {
                green_blink_count = 1;
                green_state = LED_SWITCH_STATE(green_state);
                LEDSetGreen(green_state);
            } else if (prev_light_level/curr_light_level >= 10.0) {
                red_blink_count = 3;
                red_state = LED_SWITCH_STATE(red_state);
                LEDSetRed(red_state);
            }
            prev_light_level = curr_light_level;

            // Even though the data may not change significantly,
            // there may still be blinking to do
            if (green_blink_count > 0) {
                green_blink_count--;
                green_state = LED_SWITCH_STATE(green_state);
                LEDSetGreen(green_state);
            }
            if (red_blink_count > 0) {
                red_blink_count--;
                red_state = LED_SWITCH_STATE(red_state);
                LEDSetRed(red_state);
            }

            // Format and send the data to the serial port
            msg_size = snprintf(buffer, BUFFER_SIZE, "%u: Lux = %.2f", sample_count, curr_light_level);
            if (write(serial_port, buffer, msg_size+1) == -1) {
                write_err_count++;
            }

        } else {
            sample_err_count++;
        }

        Clocks_sleep(CLOCKS_1s);
    }

    return 0;
}