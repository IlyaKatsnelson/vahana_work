// OPT3001 Light sensor interface
// http://www.ti.com/product/OPT3001
//

#ifndef LIGHT_SENSOR_H_
#define LIGHT_SENSOR_H_

// Status codes
enum light_sensor_status_codes {
    LT_STATUS_OK          = 0,
    LT_STATUS_ERROR       = -1
};

// Initialize the light sensor
enum light_sensor_status_codes lightSensorInit();

// Read one light intensity value
enum light_sensor_status_codes lightSensorRead(float * value);

#endif  // LIGHT_SENSOR_H_