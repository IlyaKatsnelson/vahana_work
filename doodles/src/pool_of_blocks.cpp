/*
-----------------------------------------------
Problem Statement
-----------------------------------------------
- Write a C++ class that implements a memory pool of fixed size blocks.

  Users of this class must be able to specify both the size of the blocks
  and the capacity of the pool (ie, the number of blocks it can hand out).

- Create a method to retrieve a free block from the pool.

- Create a method to return a block to the pool.
-----------------------------------------------
*/

#include <iostream>
#include <unordered_set>

using namespace std;

class memory_pool {
 public:
  memory_pool() = delete;
  memory_pool(unsigned long capacity, unsigned long block_size)
    : m_capacity(capacity), m_block_size(block_size) {};

  bool init() {
    // initialize the available blocks map
    auto m_pool = new unsigned char [m_block_size * m_capacity];

    for (auto i=0ul; i<m_block_size * m_capacity; i=i+m_block_size) {
      available_blocks.insert(&pool[i]);
    }
    return true;
  };

  ~memory_pool() {
    if (m_pool != nullptr) {
      delete [] m_pool;
    }
  }

  void * retrieve_block() {
    if (available_blocks.size() == 0) {
      return nullptr;
    }

    auto block = *available_blocks.begin();
    available_blocks.erase(available_blocks.begin());

    used_blocks.insert(block);

    return block;
  }

  bool return_block(void * block){
    if (block == nullptr) {
      return false;
    }

    auto found_block = used_blocks.find(block);
    if (found_block == used_blocks.end()) {
      return false;
    } else {
      auto block_info = *found_block;
      used_blocks.erase(found_block);

      available_blocks.insert(block_info);
    }

    return true;
  }

 private:
  unsigned long m_capacity{0};    // number of blocks
  unsigned long m_block_size{0};
  unsigned char *m_pool{nullptr};

  unordered_set<void *> available_blocks;
  unordered_set<void *> used_blocks;
};

int main() {
  auto pool1 = memory_pool(3, 17);
  if (pool1.init() == false) {
    cout << "Error in init!" << endl;
  }

  //

  return 0;
}

