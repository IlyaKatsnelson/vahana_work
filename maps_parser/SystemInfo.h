// Emacs type: -*- C++ -*-
/*
 * Header file for the SystemInfo class
 *
 * This class abstracts the parser specific information about the host system
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: SystemInfo.h,v 1.4 2006/09/25 23:08:40 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#ifndef __SYSTEM_INFO_H__
#define __SYSTEM_INFO_H__

#include <iosfwd>
#include <string>

//------------------------------------------------------------------------------
class SystemInfo
{
 public:
  typedef enum _slab_v
    {
      SLAB_V1 = 0x10000,
      SLAB_V2 = 0x20000
    };

 protected:
  static int _use_cnt;
  static bool _RSS_Supported;
  static bool _memmap_supported;
  static unsigned long _slabinfo_version;
  static struct tm *_loc_time;
  static struct utsname _k_info;
  long lStatus;

 public:
  // C'tors & D'tor
  SystemInfo();
  SystemInfo(const SystemInfo &entry);

  ~SystemInfo();

  // Accessor methods
  inline int getUseCnt(void) const { return _use_cnt; };
  inline bool getRSSSupported(void) { return _RSS_Supported; };
  inline bool getMemMapSupported(void) { return _memmap_supported; };
  inline long getStatus(void) const { return lStatus; };
  inline unsigned long getPageSize(void) const { return _page_size; };
  inline unsigned long getSlabInfoVersion(void) const { return _slabinfo_version; };
  inline const struct tm *getLocTime(void) const { return _loc_time; };
  inline const struct utsname &getKernelInfo(void) const { return _k_info; };

  long update(void);

  // output methods
  int pretty_print(std::ostream& out);
  int pretty_print(const char *out_fname);

  int pretty_print_lt(std::ostream& out);
  int pretty_print_lt(const char *out_fname);

 private:
  unsigned long _page_size;

};

#endif // __SYSTEM_INFO_H__

//==============================================================================

