.TH maps_parser 1 "Tues, March 27 2007"
.SH NAME
maps_parser \- generate global system memory usage report using the system statistics sources, 
process' maps files and kernel data.
.SH SYNOPSIS
.B maps_parser
[
.B \-dr
] [
.B \-aluki
] [
.B -p 
.I pid
] [
.B -o 
.I filename
] [
.I interval
[
.I count
]]
.br
.SH DESCRIPTION
.B maps_parser 
is a tool used to analyze the runtime memory usage of a particular process, 
with the given  
.I pid
, or the entire system. The analysis can be performed optional 
.I count
number of times every
.I interval
seconds. 
.B maps_parser 
uses the information provided by the 
.I maps 
files for each process, several global system statistics files,
and special kernel module to calculate the actual memory usage for each segment of 
a process' binary and the global system memory usage. 
.B maps_parser 
will generate a report with detailed memory consumption of each process and each 
memory segment in all processes in the system, the amount of unique memory used by each 
process and if a particular memory segment is shared between several processes. An 
.I estimate 
of the unique memory usage for this process is given at the bottom of the output. 
All the values are given in kilobytes unless stated otherwise.

.SH OPTIONS
.TP
.B \-d
Enable debugging mode.  It will result in storing the 
.I maps
and
.I memmap
, if they are available, files of each process 
.B maps_parser
was working with in the current directory. Each 
.I maps 
file will be appended with the PID of the process it belonged to. 
.I slabinfo
and 
.I meminfo
files are also stored.
.TP
.B \-r
Overwrites the default method that 
.B maps_parser
uses to obtain the RSS data. With this flag set the program will make use of the data from the modified
.I maps
files instead of the kernel module. If the RSS data from
.I maps
files is not available, this flag will be ignored.
.TP
.B \-a
Forces
.B maps_parser
to calculate the entire system memory usage. The program will output one big table listing all 
currently used modules in the system. Total used memory for all segments will also be printed.
.TP
.B \-l
.B maps_parser
reports detailed memory usage for each process.
.TP
.B \-k
output the table of the kernel processes.
.TP
.B \-u
(deprecated) output each process' unique and shared module information.
.TP
.B \-i
output the dirty page information. A page is concidered 
.I dirty
if it is present in RAM and its PG_dirty bit is set or the L_PTE_DIRTY flag is set in
its page table entry.
.TP
.B \-p
analyze only the process specified by 
.I pid.
.
.TP
.B \-o
store the results in the file specified by the
.I filename\. 
If the program was requested to perform several measurements, each one will be stored in
the separate output file. The names of the files will be set to the
.I filename
with appended record number.
.TP
.I interval
time period (in seconds) specifying how often to perform the measurement. Note that this
period should be larger than the time it takes for 
.B maps_parser
to process one system snapshot.
.TP
.I count
total number of times to perform the measurement.

.SH ALGORITHM
.SS General
.PP
In order to find the total system memory usage 
.B maps_parser 
will loop through all the numeric folders in the 
.I /proc
directory and use all the available 
.I maps
files to tally the values. Root privileges are needed to get the complete information.

.SS Threads
.PP
Linux treats all the threads in the user space as 
.I LWPs 
(Light Weight Processes). Each LWP gets its own PID and the appropriate entry in the 
.I /proc
directory. This makes the job of the task scheduler easier. However, all threads in the same 
process have the exactly same 
.I maps
files, which makes the job of this program a bit harder.
.B maps_parser 
uses the services provided by the 
.B parser_module
kernel module to figure out which entries in the
.I /proc
directory represent threads and which represent LWPs in order not to over-count the memory.

.SS Shared and Unique
Many modules (.so libraries or shared memory mapped modules) share pages between 
processes. It is very difficult (impossible) from the user space to directly measure 
the number of pages used by the same library in different processes. As a result 
.B maps_parser
currently 
.B (!)
only estimates the amount of memory consumed by these modules. It generates 
two numbers: best case and worst case.

.TP 
.B Best case
This is the amount of physical memory that a module would occupy if all its pages were shared in 
the most efficient way. It is equal to the maximum of all RSS values from all processes where the 
module is used.
.TP 
.B Worst case
This is the amount of physical memory that a module would occupy if all its pages were shared in 
the least efficient way. It is equal to the lover of the two numbers: (1) the size of the module file 
(cannot use more memory for code then the actual file), (2) the sum of all RSS values from all 
processes where the modules are used.

.SS Output
For the single process option
.B maps_parser
produces result similar to 
.I pmap
utility. When the system option is given the output is a bit more complex. 
Besides the memory usage for each process, the following system tables are generated:

.TP
.B All Modules
Lists all the modules (libraries, processes, memory mapped regions) used in the system and 
their memory consumption. Unnamed segments (Heap, Stack, etc.) are presented as 
.I processname_<pid>_General.
It also specifies if a particular module is unique (used only by a single process) or shared.
.TP
.B Process Unique
Lists all the processes running on the system and the unique memory each of them consume. 
The values also include the memory used by the shared objects uniquely in a process 
(local Data falls into this classification).
.TP
.B System+User Space+Kernel Space
Lists the total system memory from the
.I meminfo
file. User space memory from the 
.I All Modules
table and some of the memory used by the kernel itself.
.TP
.B Executive Summary
The smallest table from all of the above. This kind of data can be given to some hiGHer management.

The output of the
.B maps_parser 
shows the detailed memory consumption for each process, broken into entries of the following 
categories: 

.TP
.BR \ Code
The text segment of the binary as mapped in by the dynamic linker. 
.TP
.BR \ Data
The data segment of the binary, containing global variables, constants and any other 
global data from this binary
that is initialized with nonzero values. 
.TP
.BR \ Heap
Dynamically allocated memory obtained by this binary through its native dynamic memory 
allocation mechanisms (e.g. malloc() and friends). 
.TP
.BR \ Stack
Statically allocated memory for this binary, used for local variables and fixed-length 
memory allocations in the binary. 
Due to the fact that the threads in Linux share memory, the stack of all thread will appear in the 
.B Stack 
category for the process. The stack begins at address C0000000 on the ARM architecture, 
and grows down. 
So as threads are created, 2 MB address ranges will be reserved for that thread's stack right below 
the lowest stack address, with a single boundary page (4 KB) between these stack address ranges. 
NOTE: This is currently calculated as all readable, writable and executable memory above 0xb0000000. 
This should be sufficient for about 256 MB of stack addresses, or about 128 threads. 
.TP
.BR \ SRShared
Shared memory requested by this binary either through the POSIX shmget() mechanism or 
mmap() with the MAP_SHARED flag. The data in the memory segment is read-only. 
.TP
.BR \ SWShared
Shared memory requested by this binary either through the POSIX shmget() mechanism or 
mmap() with the MAP_SHARED flag. The data in the memory segment is writable. 
.TP
.BR \ PRShared
Shared memory requested by this binary either through the POSIX shmget() mechanism or 
mmap() with the MAP_PRIVATE flag. The data in the memory segment is read-only. 
.TP
.BR \ Other
Miscellaneous memory allocations not covered in the categories above (e.g. boundary 
pages used to enforce memory segmentation).

The following table lists the possible segments and their corresponding permissions:  

.nf
.ft CW
.in 12n
 -------- --------------------------------------------- 
| r-xp   | code                                        |
| rw-p   | static data, including BSS                  |
| r--p   | private mmap data, read-only                |
| r--s   | shared mmap data, read-only                 |
| rw-s   | shared mmap data, writable                  |
| -w-s   | mmaped I/O. Rare but possible               |
| -w-p   | mmaped I/O. Rare but possible               |
| ---p   | stack segment separation page.              |
| ---s   | stack segment separation page.              |
| rwxp   | stack and heap                              |
| r-xs   | potential code from JIT                     |
| rw-s   | currently folded into 'Other' category      |
 -------- --------------------------------------------- 
.ft
.fi
.in

.PP
The used memory, reported by the 
.I maps_parser 
utility, is the 
.I RSS 
memory. The 
.I RSS 
(Resident Shared Size) information refers to the memory that is actually 
physically in use by a process, while the 
.I Size 
information is the sum of the reserved virtual memory ranges, which may not actually be in use. 
.br

Does this mean that all RSS memory is unique to this process? In short, no. 
The text segments of the shared libraries may be used by multiple processes. A simple sum 
of the RSS numbers for multiple processes will 
over-count the amount of memory that is being used by these processes.

.\"Therefore, in order to (somewhat) accurately tally the amount of memory that is being 
.\"consumed uniquely by a given process, 
.\".B maps_parser 
.\"will sum the Total RSS for the executable's binary and the RSS of the Data, Heap, Stack and Shared 
.\"categories from each of the shared libraries used by this process. This number can be made 
.\"even more accurate by adding in the Code RSS amounts for each shared library that is only in use by this process.  


.SH FILES
.PD 0
.TP 15
.B parser_module.ko
kernel module used to assist maps_parser

.SH parser_module
.B parser_module
is the Linux kernel module that provides assistance to 
.B maps_parser. 
The module is loaded into the kernel memory space. As a result it has access to the information about 
all processes on the system.

.PP
.B parser_module
adds special file to the 
.I /proc 
file system called
.I parser_module
The output of the module can be controlled by writing specific commands to this file. 
Every time the file is read from the module, it returns the result of the last received command. 
The read operation also clears the command. Writing new command to the 
.B parser_module
overwrites the previously send command. 

.PP
.B parser_module
can also list the relationship information for all the processes currently running on the system.
It will include all the kernel, regular, and lightweight processes (LWPs). 
Simply reading from the 
.I parser_module
file will return a table where rows represent information about each process in the following form: 

  pid: tgid: pgrp: mm     :father:root [{###}]

Where:
.TP
.B pid
- ID of a process which information is printed.
.TP
.B tgid
- ID of the thread group leader this process belongs to.
.TP
.B pgrp
- ID of the process group this process belongs to.
.TP
.B mm
- address of the memory map structure for this process.
.TP
.B father
- ID of the parent process.
.TP
.B root
- ID of the main process this process belongs to. For example, if the process which information 
is printed is a LWP in an application with an extensive tree of threads, this field will specify 
the ID of the main process at the root of the tree.
.TP
.B {###}
- optional IDs of the threads that are in the same group as this process.

.PP
If 
.B parser_module
cannot understand what was written, it will return a negative error number. 

.PP
The following commands are supported:

.TP
.B VER
Query the version of the 
.B parser_module
that is installed on the host system.

.TP
.B LWP <pid> 
Query if the process with the specified
.I <pid>
is a 
.I Light Weight Process 
, i.e. thread in some other process. True/False (1 or 0) is returned upon reading.

NOTE: Linux has a notion of 
.I kernel groups. 
However, it is not the case that all threads in a process are formed into one group. 
Threads in a process can form arbitrary group structures. In this case 
.B parser_module
uses the address of the memory map structure of a particular process to find if it is LWP or not. 
If a processes\' 
.I father 
has the same memory map structure, then this process is a thread 
inside its parent. Otherwise it is a separate process.

.TP
.B MAIN_PID <pid> 
Query the PID of the main process to which the process, whose 
.I <pid>
was supplied, belongs to. This command uses the same method as the
.B LWP <pid> 
to identify which process is LWP and which isn't.

.TP
.B STACK_MAP <pid> 
Query the start and end addresses of the stacks of the process, whose 
.I <pid>
was supplied. The map is returned as the space separated start and end stack
segment addresses in hexadecimal form. Each stack segment in a separate line.

.TP
.B MEM_CNT_MAP <pid> 
Read the memory map of the process, whose 
.I <pid>
was supplied. The map is returned in the format where each line directly corresponds to the line 
in the process' 
.I maps 
file and, in turn, to the process' memory segment. Each line lists the use count 
for each virtual page in 2 character per page format. 

.TP
.B MEM_FLAG_MAP <pid> 
Read the flag map of the process, whose 
.I <pid>
was supplied. The map is returned in the format where each line directly corresponds to the line 
in the process' 
.I maps 
file and, in turn, to the process' memory segment. Each line lists physical page flag value 
for each virtual page in 2 character per page format. 

.TP
.B MEM_RSS_MAP <pid> 
Read the RSS map of the process, whose 
.I <pid>
was supplied. The map is returned in the format of space separated RSS page counters. Each 
value will represent the number of RSS pages used by a particular memory segment. It should 
directly correspond to a line in the process' segment
.I maps
file.

.TP
.B MEM_DIRTY_MAP <pid> 
Read the dirty pages map of the process, whose 
.I <pid>
was supplied. The map is returned in the format of space separated dirty page counters. Each 
value will represent the number of dirty pages used by a particular memory segment. It should 
directly correspond to a line in the process' segment
.I maps
file.

.TP
.B MEM_SEGMENT_MAP <pid> 
Read the memory segment information of the process, whose 
.I <pid>
was supplied. The map is returned in the format of space separated segment information addresses. 
The values returned are the following in the same sequence:

.RS
.TP
\fIstart_code/end_code\fP %.8lX
The start and end address of the code section.
.TP
\fIstart_data/end_data\fP %.8lX
The start and end address of the data section.
.TP
\fIstart_brk/brk\fP %.8lX
The start and end address of the heap.
.TP
\fIstart_stack\fP %.8lX
Predictably enough, the start of the stack region.
.TP
\fIarg_start/arg_end\fP %.8lX
The start and end address of command-line arguments.
.TP
\fIenv_start/env_end\fP %.8lX
The start and end address of environment variables.
.TP
\fIrss\fP %ld
RSS is the number of resident pages for this process. It should be noted that the global 
zero page is not accounted for by RSS.
.TP
\fItotal_vm\fP %ld
The total memory space occupied by all VMA regions in the process.
.TP
\fIlocked_vm\fP %ld
The number of resident pages locked in memory.


.SH Notes:
.TP
.B (1)
There are many possible ways to 
.I mmap 
a file. We assume that the programmer is sane and will
not map a segment with storage permissions ("-wxp" or "rwxp" for example).

.TP
.B (2)
There are cases when the same memory mapped segment set as writable in one process and
readable in several others. Currently we only process one very special case - shared readable/writable
segment with the same starting virtual address and same length. This is done due to the
limitations of the current method of data collection. In this case the used memory for the system
is reported as Shared Writable due to the fact that it cannot be swapped out.

.TP
.B (3)
Sometimes the 
.I memory 
or 
.I flag 
map files can be quite large. The continuous reading is not supported at this moment. 
It is recommended that the user supplies a buffer large enough to read the map files 
in one shot. It is better due to the fact that the map information can 
.I change 
between the readings.

.TP
.B (4)
Zero is returned in memory, flag, and RSS maps for the segments that are mapped to devices.

.TP
.B (5)
Due to the limitations of the current measurement methodology we assume that all pages in 
the 
.I data 
segments are unique. In reality this is not the case. The kernel sets the pages in 
this segment as Copy on Write (COW) which means that these pages can potentially be 
shared until they are modified.

.SH AUTHOR
Original author Matthew Klan \<mklan@motorola.com\>
.br
Modified and extended by Ilya Katsnelson \<ilya.katsnelson@motorola.com\>

.SH COPYRIGHT
Copyright (C) 2004-2007 Motorola, Inc.
