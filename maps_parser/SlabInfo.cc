// Emacs type:  -*- C++ -*-
/*
 * Implementation file for SlabInfo class
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: SlabInfo.cc,v 1.3 2006/09/25 22:37:33 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#include "main.h"
#include "SlabInfo.h"
#include "SystemInfo.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <errno.h>

#include <cctype>
#include <cstring>

using namespace std;

// global system information object
extern SystemInfo hostSystemInfo;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Constructors & destructor

SlabInfo::SlabInfo()
  : lStatus(0)
{
  update();
}


SlabInfo::SlabInfo(const SlabInfo &entry)
  : ulTotCaches(entry.ulTotCaches),
    ulTotActiveMem(entry.ulTotActiveMem),
    ulTotAllocMem(entry.ulTotAllocMem),
    lStatus(entry.lStatus)
{
}

SlabInfo::~SlabInfo()
{
}

//------------------------------------------------------------------------------
// Reload the slabinfo data from the meminfo file
//
// File structure:
// version 1.1
// cache |# of active| # of tot|size of| # of active slabs  |tot # of slabs|# of pages
// name  | objects   | objects | obj   |(blocks) of the objs| of the objs  | per slab.
//
// version 2.1
// name     <active_objs> <num_objs> <objsize> <objperslab> <pagesperslab> :
//  tunables <batchcount> <limit> <sharedfactor> :
//  slabdata <active_slabs> <num_slabs> <sharedavail>
//
//------------------------------------------------------------------------------
long
SlabInfo::update(void)
{
  const int slabv2 = (hostSystemInfo.getSlabInfoVersion() >= (SystemInfo::SLAB_V2));

  // read the slabinfo file, there is no need to wait
  ulTotCaches = ulTotActiveMem = ulTotAllocMem = 0;

  ifstream slabfile;
  slabfile.open("/proc/slabinfo");
  //  slabfile.open("/var/a780/slabinfo_SCM");
  if ((slabfile.rdstate() & ifstream::failbit ) != 0 ) {
    lStatus = slabfile.rdstate();
    cerr << "Error occurred opening slabinfo file: " << lStatus << "\n";
    return lStatus;
  }

  // skip the first line or two
  slabfile.ignore(256,'\n');

  unsigned char c = 0;
  // get the slab caches line by line until the end of the file
  unsigned long i,numActObj, numTotObj, objSize, objPerSlab, numActSlabsObj, totNumSlabs, numPages;
  i=0;
  do{
    //skip the first line if it starts from '#'
    if((c = slabfile.peek()) == '#'){
      slabfile.ignore(256,'\n');
      continue;
    }

    // slabfile.setf(ios_base:: skipws);
    slabfile.ignore(256,' ');
    if (slabv2 == false){
      slabfile >> numActObj >> numTotObj >> objSize >> numActSlabsObj >> totNumSlabs >> numPages;
    }else{
      slabfile >> numActObj >> numTotObj >> objSize >> objPerSlab >> numPages;
      slabfile.ignore(4,':');
      slabfile.ignore(256,':');
      slabfile.ignore(256,' ').ignore(256,' ');
      slabfile >> numActSlabsObj >> totNumSlabs;
    }

    ulTotCaches++;
    ulTotActiveMem += (numActSlabsObj * numPages * hostSystemInfo.getPageSize());
    ulTotAllocMem += (totNumSlabs * numPages * hostSystemInfo.getPageSize());

    // Note: at this point we just skip the rest of the line if there is something there
    // there could be statistical data or SMP data
    // slabfile.unsetf(ios_base:: skipws);
    slabfile.ignore(256,'\n');

  } while (slabfile.eof() == false && slabfile.bad() == false);

  slabfile.close();

  return lStatus;
}

//==============================================================================

