// Emacs type: -*- C++ -*-
/*
 * Header file for AcumMapsEntry class
 *
 * This class abstracts the memory information for a library or program
 * as gathered from the maps entries for the same segment on the entire system
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * AcumMapsEntry.h,v 1.2 2007/05/23 22:00:07 w30205 Exp
 * -----------------------------------------------------------------------------
 */

#ifndef __ACUM_MAPS_ENTRY_H__
#define __ACUM_MAPS_ENTRY_H__

#include <iosfwd>
#include <string>

#include "MapsEntry.h"

#define OFFSET_DEF ~(unsigned int)0x0  // default value for the offset

using namespace std;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AcumMapsEntry : public MapsEntry
{
//   typedef enum _amap_type
//     {
//       Code = 0,
//       PRShared,
//       SRShared,
//       SWShared,
//       Total
//     } amap_type;

 protected:
  unsigned long _maxCodeRSS;
  unsigned long _minCodeRSS;
  unsigned long _maxPRSharedRSS;
  unsigned long _minPRSharedRSS;
  unsigned long _maxSRSharedRSS;
  unsigned long _minSRSharedRSS;
  unsigned long _maxSWSharedRSS;
  unsigned long _minSWSharedRSS;

  unsigned long _sharedCount;
  bool _reset;

 public:

  // C'tors & D'tor
  AcumMapsEntry();
  AcumMapsEntry(const string &name);
  AcumMapsEntry(const AcumMapsEntry &entry);

  ~AcumMapsEntry();

  // Accessor methods
  inline unsigned int getCodeRSS(void) const { return _minCodeRSS; };  // RSS is the min memory, VM is max
  inline unsigned int getCodeMax(void) const { return _maxCodeRSS; };
  inline unsigned int getCodeMin(void) const { return _minCodeRSS; };
  inline unsigned int getSR_SharedVM(void) const { return _maxSRSharedRSS; };
  inline unsigned int getSR_SharedRSS(void) const { return _minSRSharedRSS; };
  inline unsigned int getSW_SharedVM(void) const { return _maxSWSharedRSS; };
  inline unsigned int getSW_SharedRSS(void) const { return _minSWSharedRSS; };
  inline unsigned int getPR_SharedVM(void) const { return _maxPRSharedRSS; };
  inline unsigned int getPR_SharedRSS(void) const { return _minPRSharedRSS; };
  inline unsigned int getSharedCount(void) const { return _sharedCount; };

  unsigned long set_offsetForType(long type, unsigned long offset);

  void add(const MapsEntry &entry);
  void reset(void);

  // Class operators
  AcumMapsEntry& operator=(const AcumMapsEntry &entry);
  const AcumMapsEntry operator+=(const MapsEntry &entry);
  const AcumMapsEntry operator+=(const AcumMapsEntry &entry);

  int table_print(ostream& out);
  int table_print_DP(ostream& out);

  static int table_head_print(ostream& out);
  static int table_head_print_DP(ostream& out);
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

struct ame_cmp{
  bool operator()(const AcumMapsEntry* x, const AcumMapsEntry* y) const{
      return (x->getName() < y->getName());
  }
};

#endif // __ACUM_MAPS_ENTRY_H__

//==============================================================================
