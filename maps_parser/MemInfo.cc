// Emacs type:  -*- C++ -*-
/*
 * Implementation file for MemInfo class
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: MemInfo.cc,v 1.3 2007/03/28 18:14:34 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#include "main.h"
#include "MemInfo.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

#include <cctype>
#include <cstring>

using namespace std;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Constructors & destructor

MemInfo::MemInfo()
  : lStatus(0)
{
  this->update();
}


MemInfo::MemInfo(const MemInfo &entry)
  : ulTotMem(entry.ulTotMem),
    ulTotUsed(entry.ulTotUsed),
    ulTotFree(entry.ulTotFree),
    ulShared(entry.ulShared),
    ulBuffers(entry.ulBuffers),
    ulCached(entry.ulCached),
    lStatus(entry.lStatus)
{
}

MemInfo::~MemInfo()
{
}

//------------------------------------------------------------------------------
// Reload the meminfo data from the meminfo file
// The format is different for 2.4 and 2.6
//  2.4
//         total:    used:    free:  shared: buffers:  cached:
// Mem:  1043357696 991637504 51720192        0 154460160 328241152
// Swap: 1571016704 10346496 1560670208
// MemTotal:      1018904 kB
// MemFree:         50508 kB
//
//  2.6
// MemTotal:        17748 kB
// MemFree:         11364 kB
// Buffers:             0 kB
//
//------------------------------------------------------------------------------
long 
MemInfo::update(void)
{
  // read the meminfo file, there is no need to wait
  // array of keywords. Remember that everything is in order - IMportant
  const char *keywords[] = {"MemTotal:","MemFree:","MemShared:","Buffers:","Cached:","Active:"};

  ifstream memfile;
  int i, fcnt = 0;
  string tmp;
  memfile.open("/proc/meminfo");

  if ((memfile.rdstate() & ifstream::failbit ) != 0 ) {
    lStatus = memfile.rdstate();	
    return lStatus;
  }

  // read line by line and then record the data depending on the keywords
  // algorithm isn't the best, but the quick and dirty type.
  do {
    memfile >> skipws >> tmp;
    //    cerr << tmp << "\n";
    for (i=fcnt;i<MemInfo::INFO_FNUM-1;i++){
      if (tmp == keywords[i]){
	switch(i){
	case 0: memfile >> ulTotMem; break;
	case 1: memfile >> ulTotFree; break;
	case 2: memfile >> ulShared; break;
	case 3: memfile >> ulBuffers; break;
	case 4: memfile >> ulCached; break;
	case 5: memfile >> ulActive; break;
	}
	fcnt++;	
	break;
      }
    }
    memfile.ignore(256,'\n');    
  } while(memfile.eof() == false && memfile.bad() == false && fcnt < MemInfo::INFO_FNUM-1);
  ulTotUsed = ulTotMem - ulTotFree;

  memfile.close();

  return lStatus;
}

//==============================================================================

