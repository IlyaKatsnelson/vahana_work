// Emacs type: -*- C++ -*-
/*
 * Header file for MapsEntry class
 *
 * This class abstracts the memory information for a library
 * or program as gathered from the maps file in the proc entry
 * for that process.
 *
 *                         vm_offset      vm_inode
 * vm_start vm_end  vm_flags       vm_dev         size     filename
 * 00008000-0000b000 r-xp 00000000 00:08 02581564 00003000 /usr/local/bin/testagent
 *
 *
 * Flags:
 * r-xp  -> code pages
 * rw-p  -> data pages
 * rwxp  -> heap (if below 0xb0......) or stack (if at or above 0xb0......)
 * ---p  -> pages for some unknown use
 * rw-s  -> shared memory pages, marked read-write
 * r--s  -> shared memory pages, marked read-only
 * r--p  -> private memory pages, marked read-only
 *
 *
 * Copyright (C) 2005, 2006, 2007 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: MapsEntry.h,v 1.16 2007/05/24 01:13:01 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#ifndef __MAPS_ENTRY_H__
#define __MAPS_ENTRY_H__

#include <iosfwd>
#include <string>

#include "BaseMapsEntry.h"
#include "SystemInfo.h"

#define OFFSET_DEF ~(unsigned int)0x0  // default value for the offset

using namespace std;

class ProcMap;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#define WR_TYPES_NUM  5
#define WR_TYPES {MapsEntry::Data,MapsEntry::BSS,MapsEntry::Heap,\
      MapsEntry::Stack,MapsEntry::Unknown}

#define for_each_type(x)   for(x=MapsEntry::Code;x<=MapsEntry::Total;x++)
#define for_each_utype(x)  for(x=MapsEntry::Code;x<MapsEntry::Total;x++)
#define for_each_wrtype(x) for(x=0;x<WR_TYPES_NUM;x++)

//------------------------------------------------------------------------------
class MapsEntry : public BaseMapsEntry
{

 public:
  typedef enum _map_type
    {
      Code = 0,
      Data,
      BSS,
      Heap,
      Stack,
      PRShared,
      SRShared,
      SWShared,
      Unknown,
      Total,
      AllTypes
    } map_type;

  typedef enum _store_type
    {
      VM = 0,
      RSS,
      DP,
      AllStores
    }store_type;

 protected:
  unsigned long _fileOffsetArr[AllTypes];

  // 2D array of sizes. 1st row - VM; 2nd - RSS; 3rd - dirty pages
  unsigned long _segSizeArr[AllStores][AllTypes];

  bool _unique;
  ProcMap *_pm_parent;

  static short WRSegsArr[MapsEntry::AllTypes];  // it cannot be larger then AllTypes

 public:

  // C'tors & D'tor
  MapsEntry();
  MapsEntry(ProcMap* const parent);
  MapsEntry(const string &name);

  ~MapsEntry();

 public:
  // Accessor methods
  inline const pid_t getPID(void) const;

  inline unsigned long getTotalSize(void) const { return _segSizeArr[VM][Total]; };
  inline unsigned long getCodeSize(void) const { return _segSizeArr[VM][Code]; };
  inline unsigned long getDataSize(void) const { return _segSizeArr[VM][Data]; };
  inline unsigned long getBSSSize(void) const { return _segSizeArr[VM][BSS]; };
  inline unsigned long getHeapSize(void) const { return _segSizeArr[VM][Heap]; };
  inline unsigned long getStackSize(void) const { return _segSizeArr[VM][Stack]; };
  inline unsigned long getSharedSize(void) const { return (_segSizeArr[VM][SWShared]+_segSizeArr[VM][SRShared]); };
  inline unsigned long getSR_SharedSize(void) const { return _segSizeArr[VM][SRShared]; };
  inline unsigned long getSW_SharedSize(void) const { return _segSizeArr[VM][SWShared]; };
  inline unsigned long getPR_SharedSize(void) const { return _segSizeArr[VM][PRShared]; };
  inline unsigned long getUnknownSize(void) const { return _segSizeArr[VM][Unknown]; };

  inline unsigned long totalRSS(void) const { return _segSizeArr[RSS][Total]; };
  inline unsigned long getCodeRSS(void) const { return _segSizeArr[RSS][Code]; };
  inline unsigned long getDataRSS(void) const { return _segSizeArr[RSS][Data]; };
  inline unsigned long getBSSRSS(void) const { return _segSizeArr[RSS][BSS]; };
  inline unsigned long getHeapRSS(void) const { return _segSizeArr[RSS][Heap]; };
  inline unsigned long getStackRSS(void) const { return _segSizeArr[RSS][Stack]; };
  inline unsigned long getSharedRSS(void) const { return (_segSizeArr[RSS][SRShared]+_segSizeArr[RSS][SWShared]); };
  inline unsigned long getSR_SharedRSS(void) const { return _segSizeArr[RSS][SRShared]; };
  inline unsigned long getSW_SharedRSS(void) const { return _segSizeArr[RSS][SWShared]; };
  inline unsigned long getPR_SharedRSS(void) const { return _segSizeArr[RSS][PRShared]; };
  inline unsigned long getUnknownRSS(void) const { return _segSizeArr[RSS][Unknown]; };

  inline unsigned long totalDP(void) const { return _segSizeArr[DP][Total]; };
  inline unsigned long getCodeDP(void) const { return _segSizeArr[DP][Code]; };
  inline unsigned long getDataDP(void) const { return _segSizeArr[DP][Data]; };
  inline unsigned long getBSSDP(void) const { return _segSizeArr[DP][BSS]; };
  inline unsigned long getHeapDP(void) const { return _segSizeArr[DP][Heap]; };
  inline unsigned long getStackDP(void) const { return _segSizeArr[DP][Stack]; };
  inline unsigned long getSharedDP(void) const { return (_segSizeArr[DP][SRShared]+_segSizeArr[DP][SWShared]); };
  inline unsigned long getSR_SharedDP(void) const { return _segSizeArr[DP][SRShared]; };
  inline unsigned long getSW_SharedDP(void) const { return _segSizeArr[DP][SWShared]; };
  inline unsigned long getPR_SharedDP(void) const { return _segSizeArr[DP][PRShared]; };
  inline unsigned long getUnknownDP(void) const { return _segSizeArr[DP][Unknown]; };

  inline bool isUnique(void) const { return _unique; };

  // Mutator methods
  inline void setParent(ProcMap* const parent) { _pm_parent = parent; };
  inline string& getParentName(void);

  // these methods are a bit special. They are used to access all the members
  // of this complicated class.
  inline unsigned long get_offsetForType(long type) const { return _fileOffsetArr[type]; };
  inline unsigned long get_vm_sizeForType(long type) const { return _segSizeArr[VM][type]; };
  inline unsigned long get_rss_sizeForType(long type) const { return _segSizeArr[RSS][type]; };
  inline unsigned long get_dp_sizeForType(long type) const { return _segSizeArr[DP][type]; };

  unsigned long set_offsetForType(long type, unsigned long offset);
  unsigned long set_offsetForType2(long type, unsigned long offset);
  unsigned long set_offsetForTypeOR(long type, unsigned long offset);
  unsigned long set_sizeForType(long type, unsigned long size);
  unsigned long set_sizeForTypeOR(long type, unsigned long size);
  unsigned long set_rss_sizeForType(long type, unsigned long rss);
  unsigned long set_dp_sizeForType(long type, unsigned long rss);
  unsigned long set_size(long store, long type, long size);
  int update_sizes(MapsEntry::map_type type, const unsigned long rss_pgs, 
		   const unsigned long dirty_pgs, const bool rss_ovr);

  inline void setShared(bool a) { _unique = !a; };

  void reset(void);
  void add(const MapsEntry &entry);
  void add_type(const MapsEntry &entry, const MapsEntry::map_type type = MapsEntry::Unknown);
  void addU(const MapsEntry &entry);
  void addUData(const MapsEntry &entry);

  // Class operators
  const MapsEntry operator+=(const MapsEntry &entry);
  bool operator!=(const MapsEntry &a) const;

  unsigned long operator[](const map_type a);
  const unsigned long operator[](const map_type a) const;

  // I/O Stream operators
  friend ostream& operator<<(ostream& out, const MapsEntry& entry);

  int table_print(ostream& out);
  int table_print(ostream& out, const string &name);
  int table_printRSS(ostream& out, const string &name);
  int table_printRSS_tot(ostream& out, const string &name);
  int table_printRSS2(ostream& out);
  int table_print_offs(ostream& out, const string &name);
  int table_print_dp(ostream& out);
  int table_print_dp(ostream& out, const string &name);

  // static header printing functions
  static int table_head_print(ostream& out);
  static int table_head_print_line(ostream& out);
  static int table_head_print_RSS(ostream& out);
  static int table_head_print_RSS_tot(ostream& out);
  static int table_head_print_sum(ostream& out);
  static int table_head_print_RSS_DP(ostream& out);
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// functionals
struct me_cmp{
  bool operator()(const MapsEntry* x, const MapsEntry* y) const{
      return (x->getName() < y->getName());
  }
};

#endif // __MAPS_ENTRY_H__

//==============================================================================
