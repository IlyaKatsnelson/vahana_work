// Emacs type: -*- C++ -*-
/*
 * Header file for the Stat class
 *
 * This class abstracts data manipulation from the stat file for each process
 * 
 * stat   Status information about the process. This is used by ps(1).  
 *        It is defined in  /usr/src/linux/fs/proc/array.c.
 *
 * Format: [pid %d] [comm %s] [state %c] [ppid %d] [pgrp %d] [session %d] [tty_nr %d]
 *         [tpgid %d] [flags %lu] [minflt %lu] [cminflt %lu] [majflt %lu] [cmajflt %lu]
 *         [utime %lu] [stime %lu] [cutime %ld] [cstime %ld] [priority %ld]
 *         [nice %ld] [0 %ld] [itrealvalue %ld] [starttime %lu] [vsize %lu] [rss %ld]
 *         [rlim %lu] [startcode %lu] [endcode %lu] [startstack %lu] [kstkesp %lu]
 *         [kstkeip %lu] [signal %lu] [blocked %lu] [sigignore %lu] [sigcatch %lu]
 *         [wchan %lu] [nswap %lu] [cnswap %lu] [exit_signal %d] [processor %d]
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: Stat.h,v 1.2 2006/09/25 22:37:33 w30205 Exp $
 * -----------------------------------------------------------------------------
 */


#ifndef __STAT_H__
#define __STAT_H__

#include <iosfwd>
#include <string>

using namespace std;

//------------------------------------------------------------------------------
class Stat
{
protected:
  pid_t _pid;
  pid_t _ppid;
  pid_t _pgrp;
  pid_t _tpgid;

  string _cmd_name;
  char _state;
  int _session;
  int _tty_nr;

  unsigned long _flags;
  unsigned long _minflt;
  unsigned long _cminflt;
  unsigned long _majflt;
  unsigned long _cmajflt;
  unsigned long _utime;
  unsigned long _stime;
  long _cutime;
  long _cstime;
  long _priority;
  long _nice;
  long _itrealvalue;
  unsigned long _starttime;
  unsigned long _vsize;
  long _rss;
  unsigned long _rlim;

  unsigned long _startcode;
  unsigned long _endcode;
  unsigned long _startstack;   // Stores the address right above that of main()'s
                               // return address.

  unsigned long _kstkesp;
  unsigned long _kstkeip;
  unsigned long _signal;
  unsigned long _blocked;
  unsigned long _sigignore;
  unsigned long _sigcatch;
  unsigned long _wchan;
  unsigned long _nswap;
  unsigned long _cnswap;
  long _exit_signal;
  long _processor;

  long lStatus;

public:
  // C'tors & D'tor
  Stat();

  ~Stat();

  // Accessor methods
  inline const pid_t getPID() const { return _pid; };
  inline const pid_t getPPID() const { return _ppid; };
  inline const pid_t getPGRP() const { return _pgrp; };
  inline const char get_state() const { return _state; };
  inline string& get_cmd_name() const { return (string &)(_cmd_name); };

  inline const unsigned long get_startcode() const { return _startcode; };
  inline const unsigned long get_endcode() const { return _endcode; };
  inline const unsigned long get_startstack() const { return _startstack; };

  inline const unsigned long get_esp() const { return _kstkesp; };
  inline const unsigned long get_eip() const { return _kstkeip; };

  inline pid_t setPID(pid_t pid) { _pid=pid; return _pid; };

  long update(void);
  long reset(void);

  int StoreDebug(void);
};

#endif // __STAT_H__

//==============================================================================

