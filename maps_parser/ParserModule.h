// Emacs type: -*- C++ -*-
/*
 * Header file for the ParserModule class
 *
 * This class abstracts the communication with the parser_module kernel module
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: ParserModule.h,v 1.9 2007/05/03 00:06:10 w30205 Exp $
 * -----------------------------------------------------------------------------
 */ 

#ifndef __PARSER_MODULE_H__
#define __PARSER_MODULE_H__

#include <iosfwd>
#include <string>
#include <signal.h>
#include <vector>

using namespace std;

//#define MODULE_NAME "parser_module"

//------------------------------------------------------------------------------
class ParserModule
{
 protected:
  long lStatus;
  bool bModuleLoaded;
  bool bDoUnLoad;
  static const char *kmodule_name;
  FILE *kmodule_file;
  static int iLoadCnt;
  static char _kmodule_ver[64];

 public:
  // C'tors & D'tor
  ParserModule(bool do_load = true);
  ParserModule(const ParserModule &entry);
  
  ~ParserModule();

  // Accessor methods
  inline long getStatus(void) const { return lStatus; };
  inline bool getModuleLoaded(void) const { return bModuleLoaded; };
  inline bool getDoUnLoad(void) const { return bDoUnLoad; };
  inline const char* const getKModuleName(void) const { return kmodule_name; };
  inline FILE* getKModuleFile(void) const { return kmodule_file; };
  inline const char* const getKModuleVer(void) const { return _kmodule_ver; };
  
  // Kernel module communication functions
  int isLWP(pid_t pid);
  int isKProcess(pid_t pid);
  pid_t readMainPID(pid_t pid);
  int readStackInfo(pid_t pid, unsigned long *vm_start, unsigned long *vm_end);
  int readStackMap(pid_t pid, char *map, size_t msize);
  int readStackMapVector(pid_t pid, vector<unsigned long > &v);
  int readMemMap(pid_t pid, char *map, size_t msize);
  int readFlagMap(pid_t pid, char *map, size_t msize);
  int readBuf(char *buf, size_t msize);
  int readModError(char *buf, char *err_buf, size_t bsize);
  int readRSSMap(pid_t pid, char *map, size_t msize);
  int readRSSMapVector(pid_t pid, vector<unsigned long > &v);
  int readDirtyMap(pid_t pid, char *map, size_t msize);
  int readDirtyMapVector(pid_t pid, vector<unsigned long > &v);
  int readModuleVer(char *ver_str, const size_t msize);
  int readMemSegmentMap(pid_t pid, char *map, size_t msize);

  // Kernel module control functions
  int iseof() {return feof(getKModuleFile());};
  int iserror() {return ferror(getKModuleFile());};
  void clearerror() {return clearerr(getKModuleFile());};

  // Kernel module control functions
  long loadModule();
  long unloadModule();
  long isModuleLoaded();

  static int loadIncr() { return ++iLoadCnt; };
  static int loadDecr() { 
    if(iLoadCnt == 0)  return iLoadCnt; 
    else  return --iLoadCnt; };
  static int getLoadCnt() { return iLoadCnt; };
};

#endif // __PARSER_MODULE_H__

//==============================================================================

