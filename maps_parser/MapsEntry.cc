// Emacs type:  -*- C++ -*-
/*
 * Implementation file for MapsEntry class
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: MapsEntry.cc,v 1.21 2007/05/24 01:13:01 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#include <iostream>
#include <iomanip>
#include <string>

#include <cctype>
#include <cstring>
#include <assert.h>

#include "main.h"
#include "MapsEntry.h"
#include "ProcMap.h"

using namespace std;

//------------------------------------------------------------------------------
short MapsEntry::WRSegsArr[MapsEntry::AllTypes] = WR_TYPES;

// global system information object
extern SystemInfo hostSystemInfo;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Constructors & destructor

MapsEntry::MapsEntry()
{
  reset();
}

MapsEntry::MapsEntry(ProcMap* const parent)
{
  reset();
  setParent(parent);
}

MapsEntry::MapsEntry(const std::string &name)
{
  reset();
  setName(name);
}

MapsEntry::~MapsEntry()
{
  // No implementation
}

// Accessor member methods

inline const pid_t 
MapsEntry::getPID(void) const 
{ 
  return (_pm_parent == NULL ? 0 : (_pm_parent->getPID())); 
}

//------------------------------------------------------------------------------
// Change values methods.
// One for VM, one for RSS, and one for everyone
// each method returns the new value (easier)
//------------------------------------------------------------------------------
unsigned long int
MapsEntry::set_sizeForType(long type, unsigned long size)
{
  assert(Code <= type && type < AllTypes);

  return (_segSizeArr[VM][type] = size);
}

// Same method as above, but it sets the size value to the max of the two
unsigned long int
MapsEntry::set_sizeForTypeOR(long type, unsigned long size)
{
  unsigned long int out_val = get_vm_sizeForType(type);

  if (out_val < size){
    out_val = set_sizeForType(type,size);
  }

  return out_val;
}

unsigned long int
MapsEntry::set_rss_sizeForType(long type, unsigned long rss)
{
  assert(Code <= type && type < AllTypes);

  return (_segSizeArr[RSS][type] = rss);
}

unsigned long int
MapsEntry::set_offsetForType(long type, unsigned long offset)
{
  assert(Code <= type && type < AllTypes);

  // Exit if there was already module with the same offset and same type
  assert(_fileOffsetArr[type]==OFFSET_DEF);
  _fileOffsetArr[type] = offset;

  return offset;
}

unsigned long int
MapsEntry::set_offsetForType2(long type, unsigned long offset)
{
  assert(Code <= type && type < AllTypes);

  // no verification
  _fileOffsetArr[type] = offset;

  return offset;
}

// this function performs (sort of) OR operation between two offset values
unsigned long int
MapsEntry::set_offsetForTypeOR(long type, unsigned long offset)
{
  assert(Code <= type && type < AllTypes);

  // no verification
  _fileOffsetArr[type] = offset;

  return offset;
}

// this function sets the dirty page count for the specified type
unsigned long int
MapsEntry::set_dp_sizeForType(long type, unsigned long dp)
{
  assert(Code <= type && type < AllTypes);

  return (_segSizeArr[DP][type] = dp);
}

unsigned long int
MapsEntry::set_size(long store, long type, long int size)
{
  if (store == MapsEntry::VM){
    return (set_sizeForType(type, size));
  }else if (store == MapsEntry::RSS){
    return (set_rss_sizeForType(type, size));
  }else{
    return (set_dp_sizeForType(type, size));
  }

  return 0;
}

//------------------------------------------------------------------------------
// Just reset the values, sometimes
//------------------------------------------------------------------------------
void
MapsEntry::reset(void)
{
  BaseMapsEntry::reset();

  // loop through the values
  memset(_segSizeArr[VM],0,sizeof(_segSizeArr[VM][0])*AllTypes);
  memset(_segSizeArr[RSS],0,sizeof(_segSizeArr[VM][0])*AllTypes);
  memset(_segSizeArr[DP],0,sizeof(_segSizeArr[VM][0])*AllTypes);
  memset(_fileOffsetArr,OFFSET_DEF,sizeof(_fileOffsetArr[0])*AllTypes);

  _unique = true;
}

void
MapsEntry::add(const MapsEntry &entry)
{
  // add the filenames unless they are equal
  // this will take care of the adding to the default init value
  if (_filename.length() == 0)
    _filename = entry._filename;

  // loop through the values
  int i;
  for_each_type(i){
    set_sizeForType(i, get_vm_sizeForType(i) + entry.get_vm_sizeForType(i));
    set_rss_sizeForType(i, get_rss_sizeForType(i) + entry.get_rss_sizeForType(i));
    set_dp_sizeForType(i, get_dp_sizeForType(i) + entry.get_dp_sizeForType(i));
    set_offsetForTypeOR(i, entry.get_offsetForType(i));
  }
}

//------------------------------------------------------------------------------
// Version of add that only adds for the specified type.
// In this case ther eis no need for all the looping
//------------------------------------------------------------------------------
void 
MapsEntry::add_type(const MapsEntry &entry, const MapsEntry::map_type type)
{
  // add the filenames unless they are equal
  // this will take care of the adding to the default init value
  if (_filename.length() == 0)
    _filename = entry._filename;

  set_sizeForType(type, get_vm_sizeForType(type) + entry.get_vm_sizeForType(type));
  set_rss_sizeForType(type, get_rss_sizeForType(type) + entry.get_rss_sizeForType(type));
  set_dp_sizeForType(type, get_dp_sizeForType(type) + entry.get_dp_sizeForType(type));
  set_offsetForTypeOR(type, entry.get_offsetForType(type));
}

//------------------------------------------------------------------------------
// Version of add that only adds the unique memory segments
//------------------------------------------------------------------------------
void
MapsEntry::addU(const MapsEntry &entry)
{
  // only add code if it is unique
  if (entry.isUnique() == false)
    return;

  // add the filenames unless they are equal
  // this will take care of the adding to the default init value
  if (_filename.length() == 0)
    _filename = entry._filename;

  // loop through the values
  int i;
  for_each_type(i){
    set_sizeForType(i, (get_vm_sizeForType(i) + entry.get_vm_sizeForType(i)));
    set_rss_sizeForType(i, (get_rss_sizeForType(i) + entry.get_rss_sizeForType(i)));
    set_dp_sizeForType(i, (get_dp_sizeForType(i) + entry.get_dp_sizeForType(i)));
    set_offsetForTypeOR(i, entry.get_offsetForType(i));
  }
}

//------------------------------------------------------------------------------
// Version of add method that adds the memory used uniquely.
// i.e. - even though the library is shared by many processes, its data is local
//        and not shared
//------------------------------------------------------------------------------
void
MapsEntry::addUData(const MapsEntry &entry)
{
  // add the filenames unless they are equal
  // this will take care of the adding to the default init value
  if (_filename.length() == 0)
    _filename = entry._filename;

  // loop through the values - but not completely
  // only add the RO segments for unique maps
  // but all for WR segments
  int i;
  if (entry.isUnique() == true){
    for_each_type(i){
      set_sizeForType(i, (get_vm_sizeForType(i) + entry.get_vm_sizeForType(i)));
      set_rss_sizeForType(i, (get_rss_sizeForType(i) + entry.get_rss_sizeForType(i)));
      set_dp_sizeForType(i, (get_dp_sizeForType(i) + entry.get_dp_sizeForType(i)));
      set_offsetForTypeOR(i, entry.get_offsetForType(i));
    }
  }else{
    unsigned long totalSize = 0;
    unsigned long totalRSS = 0;
    unsigned long totalDP = 0;

    for_each_wrtype(i){
      set_size(MapsEntry::VM, WRSegsArr[i], (get_vm_sizeForType(WRSegsArr[i]) + entry.get_vm_sizeForType(WRSegsArr[i])));
      set_size(MapsEntry::RSS, WRSegsArr[i], (get_rss_sizeForType(WRSegsArr[i]) + entry.get_rss_sizeForType(WRSegsArr[i])));
      set_size(MapsEntry::DP, WRSegsArr[i], (get_dp_sizeForType(WRSegsArr[i]) + entry.get_dp_sizeForType(WRSegsArr[i])));
      set_offsetForTypeOR(WRSegsArr[i], entry.get_offsetForType(WRSegsArr[i]));

      // update total
      totalSize += entry.get_vm_sizeForType(WRSegsArr[i]);
      totalRSS += entry.get_rss_sizeForType(WRSegsArr[i]);
      totalDP += entry.get_dp_sizeForType(WRSegsArr[i]);
    }
    set_size(MapsEntry::VM, MapsEntry::Total, (get_vm_sizeForType(MapsEntry::Total) + totalSize));
    set_size(MapsEntry::RSS, MapsEntry::Total, (get_rss_sizeForType(MapsEntry::Total) + totalRSS));
    set_size(MapsEntry::DP, MapsEntry::Total, (get_dp_sizeForType(MapsEntry::Total) + totalDP));
  }
}

const MapsEntry
MapsEntry::operator+=(const MapsEntry &entry)
{
  // use the add method
  this->add(entry);

  return *this;
}

//------------------------------------------------------------------------------
// Return the VM value of the segment specified by the map_type
// This operator provides some sort of help in taming the growing list of segments
//------------------------------------------------------------------------------
unsigned long int
MapsEntry::operator[](const map_type a)
{
  return (this->get_vm_sizeForType(a));
}

const unsigned long int
MapsEntry::operator[](const map_type a) const
{
  return (this->get_vm_sizeForType(a));
}

string& 
MapsEntry::getParentName(void) 
{
  return _pm_parent->getCmdName(); 
}

//------------------------------------------------------------------------------
// Update the sizes for several parameters
//  rss_pgs - RSS pages read by some other method besides /maps files
//  rss_ovr - overwrite the default RSS source => read the RSS data from maps
//            files or memmap files, if they are available.
//------------------------------------------------------------------------------
int 
MapsEntry::update_sizes(MapsEntry::map_type type, 
			const unsigned long rss_pgs, 
			const unsigned long dirty_pgs, 
			const bool rss_ovr)
{
  // Update sizes & RSSs based on the flags for this line
  // Update the total
  set_sizeForType(MapsEntry::Total, getTotalSize() + KB(get_vm_size()));
  set_rss_sizeForType(MapsEntry::Total, totalRSS() + rss_pgs);
  set_dp_sizeForType(MapsEntry::Total, totalDP() + dirty_pgs);
  
  // Update this type
  set_sizeForType(type, get_vm_sizeForType(type) + KB(get_vm_size()));
  set_rss_sizeForType(type, get_rss_sizeForType(type) + rss_pgs);
  set_dp_sizeForType(type, get_dp_sizeForType(type) + dirty_pgs);
  
  return 0;
}

//------------------------------------------------------------------------------
// Friend methods
//------------------------------------------------------------------------------

ostream&
operator<<(ostream& out, const MapsEntry& entry)
{
  out << "Statistics for [" << entry._filename << "]\n";
  out << setiosflags(ios::left)
      << "\tCode Size:\t" << setw(14) << entry.get_vm_sizeForType(MapsEntry::Code)
      << "\tRSS:\t" << entry.get_rss_sizeForType(MapsEntry::Code) << "\n"
      << "\tData Size:\t" << setw(14) << entry.get_vm_sizeForType(MapsEntry::Data)
      << "\tRSS:\t" << entry.get_rss_sizeForType(MapsEntry::Data) << "\n"
      << "\tBSS Size:\t" << setw(14) << entry.get_vm_sizeForType(MapsEntry::BSS)
      << "\tRSS:\t" << entry.get_rss_sizeForType(MapsEntry::BSS) << "\n"
      << "\tHeap Size:\t" << setw(14) << entry.get_vm_sizeForType(MapsEntry::Heap)
      << "\tRSS:\t" << entry.get_rss_sizeForType(MapsEntry::Heap) << "\n"
      << "\tStack Size:\t" << setw(14) << entry.get_vm_sizeForType(MapsEntry::Stack)
      << "\tRSS:\t" << entry.get_rss_sizeForType(MapsEntry::Stack) << "\n"
      << "\tPRShared Size:\t" << setw(14) << entry.get_vm_sizeForType(MapsEntry::PRShared)
      << "\tRSS:\t" << entry.get_rss_sizeForType(MapsEntry::PRShared) << "\n"
      << "\tSRShared Size:\t" << setw(14) << entry.get_vm_sizeForType(MapsEntry::SRShared)
      << "\tRSS:\t" << entry.get_rss_sizeForType(MapsEntry::SRShared) << "\n"
      << "\tSWShared Size:\t" << setw(14) << entry.get_vm_sizeForType(MapsEntry::SWShared)
      << "\tRSS:\t" << entry.get_rss_sizeForType(MapsEntry::SWShared) << "\n"
      << "\tOther Size:\t" << setw(14) << entry.get_vm_sizeForType(MapsEntry::Unknown)
      << "\tRSS:\t" << entry.get_rss_sizeForType(MapsEntry::Unknown) << "\n"
      << "\tTotal Size:\t" << setw(14) << entry.get_vm_sizeForType(MapsEntry::Total)
      << "\tRSS:\t" << entry.get_rss_sizeForType(MapsEntry::Total) << endl;

  return out;
}

//------------------------------------------------------------------------------
int MapsEntry::table_print(ostream& out)
{
  table_print(out, _filename);
  return 0;
}

//------------------------------------------------------------------------------
int MapsEntry::table_print(ostream& out, const std::string &name)
{
  out << setiosflags(ios::left);

  unsigned long segment_tot_RSS = 0;

  int i;
  for_each_utype(i){
    out << setw(8) << get_vm_sizeForType(i) << "|" 
	<< setw(8) << get_rss_sizeForType(i) << "|" ;

    segment_tot_RSS += get_rss_sizeForType(i);
  }

  out << setw(1) << (isUnique() ? "U" : "S") << "|"
      << setw(8) << segment_tot_RSS << "|";
  out << name.c_str() << "\n";
  return 0;
}

//------------------------------------------------------------------------------
int MapsEntry::table_print_offs(ostream& out, const std::string &name)
{
  out << setiosflags(ios::left);
  int i;
  for_each_utype(i){
    out << setw(8) << get_vm_sizeForType(i) << "|" << setw(8) << get_rss_sizeForType(i) << "|" ;
    out << hex << _fileOffsetArr[i] << dec << "|" ;
  }
  out << setw(1) << (isUnique() ? "U" : "S") << "|";
  out << name << "\n";
  return 0;
}

//------------------------------------------------------------------------------
int MapsEntry::table_printRSS(ostream& out, const std::string &name)
{
  this->table_printRSS2(out);
  out << "|" << setw(1) << (isUnique() ? "U" : "S") << "|" << name << "\n";
  return 0;
}

//------------------------------------------------------------------------------
int MapsEntry::table_printRSS2(ostream& out)
{
  out << setiosflags(ios::left);
  int i;
  for_each_utype(i){
    out << setw(8) << get_rss_sizeForType(i) << "|" ;
  }
  return 0;
}

//------------------------------------------------------------------------------
int MapsEntry::table_printRSS_tot(ostream& out, const std::string &name)
{
  this->table_printRSS2(out);
  out << "|" << setw(8) << (get_rss_sizeForType(Total)) << "|" << name << "\n";
  return 0;
}

//------------------------------------------------------------------------------
int MapsEntry::table_print_dp(ostream& out)
{
  table_print_dp(out, _filename);
  return 0;
}

//------------------------------------------------------------------------------
int MapsEntry::table_print_dp(ostream& out, const std::string &name)
{
  out << setiosflags(ios::left);

  unsigned long segment_tot_RSS = 0;
  unsigned long segment_tot_DP = 0;

  int i;
  for_each_utype(i){
    out << setw(8) << get_vm_sizeForType(i) << "|" 
	<< setw(8) << get_rss_sizeForType(i) << "|" 
	<< setw(8) << get_dp_sizeForType(i) << "|" ;

    segment_tot_RSS += get_rss_sizeForType(i);
    segment_tot_DP += get_dp_sizeForType(i);
  }

  out << setw(1) << (isUnique() ? "U" : "S") << "|"
      << setw(8) << segment_tot_RSS << "|" 
      << setw(8) << segment_tot_DP << "|";
  out << name.c_str() << "\n";
  return 0;
}

//------------------------------------------------------------------------------
// A couple of functions to output the headers of the tables
// They are put here so the users will not have to be concerned with these items
// All the functions are static so that they can be called from anywhere
//------------------------------------------------------------------------------
int MapsEntry::table_head_print(std::ostream& out)
{
  out << " Code   | RSS    |"
      << " Data   | RSS    |"
      << " BSS    | RSS    |"
      << " Heap   | RSS    |"
      << " Stack  | RSS    |"
      << "PRShared| RSS    |"
      << "SRShared| RSS    |"
      << "SWShared| RSS    |"
      << " Other  | RSS    |"
      << "U| RSS Tot|Module name\n";
  return 0;
}

int MapsEntry::table_head_print_line(std::ostream& out)
{
  out << "--------------------------------------------------------------------"
      << "--------------------------------------------------------------------\n";
  return 0;
}

int MapsEntry::table_head_print_RSS(std::ostream& out)
{
  out << " Code   | Data   | BSS    | Heap   | Stack  |PRShared|SRShared|SWShared| Other  |U|Module name\n";
  return 0;
}

int MapsEntry::table_head_print_RSS_tot(std::ostream& out)
{
  out << " Code   | Data   | BSS    | Heap   | Stack  |PRShared|SRShared|SWShared| Other  | Total  |Module name\n";
  return 0;
}

int MapsEntry::table_head_print_RSS_DP(std::ostream& out)
{
  out << " Code   | RSS    | Dirty  |"
      << " Data   | RSS    | Dirty  |"
      << " BSS    | RSS    | Dirty  |"
      << " Heap   | RSS    | Dirty  |"
      << " Stack  | RSS    | Dirty  |"
      << "PRShared| RSS    | Dirty  |"
      << "SRShared| RSS    | Dirty  |"
      << "SWShared| RSS    | Dirty  |"
      << " Other  | RSS    | Dirty  |"
      << "U| RSS Tot| Dt Tot |Module name\n";
  return 0;
}

int MapsEntry::table_head_print_sum(std::ostream& out)
{
  out << " Non-sys + slubs |        | Code U |NCodeRO U|NCodeWR U|Code S|PRShared|SRShared|SWShared| PCache | slabs  | free \n";
  out << " A11+A12+A2+B+C+D+E+G|    | A11    | A12    | A2     | B      | C      | D      | E      | F      | G      | H    \n";
  return 0;
}

//------------------------------------------------------------------------------
// compare if two entries are not equal
// if equal return 0 => not true.
// Note: we don't compare the code RSS sizes in order to make the program a bit more
// robust.
//------------------------------------------------------------------------------
bool MapsEntry::operator!=(const MapsEntry &a) const
{

  // compare all the elements
  if (a.getDeviceMajorNumber() == getDeviceMajorNumber() &&
      a.getDeviceMinorNumber() == getDeviceMinorNumber() &&
      a.getFileInode() == getFileInode() &&
      a.getCodeSize() == getCodeSize() &&
      a.getDataSize() == getDataSize() &&
      a.getDataRSS() == getDataRSS() &&
      a.getBSSSize() == getBSSSize() &&
      a.getBSSRSS() == getBSSRSS() &&
      a.getHeapRSS() == getHeapRSS() &&
      a.getStackRSS() == getStackRSS() &&
      a.getPR_SharedRSS() == getPR_SharedRSS() &&
      a.getSR_SharedRSS() == getSR_SharedRSS() &&
      a.getSW_SharedRSS() == getSW_SharedRSS() &&
      a.getUnknownRSS() == getUnknownRSS() &&
      a.getCodeDP() == getCodeDP() &&
      a.getDataDP() == getDataDP() &&
      a.getBSSDP() == getBSSDP() &&
      a.getHeapDP() == getHeapDP() &&
      a.getStackDP() == getStackDP())
    return 0;

  return 1;
}

//------------------------------------------------------------------------------
//==============================================================================
