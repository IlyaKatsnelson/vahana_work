// Emacs type:  -*- C++ -*-
/*
 * Implementation file for Stat class
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: Stat.cc,v 1.2 2006/09/25 22:37:33 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#include "main.h"
#include "Stat.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

#include <cctype>
#include <cstring>
#include <linux/limits.h>

using namespace std;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Constructors & destructor

Stat::Stat()
{
  this->reset();
}

Stat::~Stat()
{
}

//------------------------------------------------------------------------------
// Reset the object data
//------------------------------------------------------------------------------
long
Stat::reset(void)
{
  _pid = _ppid = _pgrp = _tpgid = MAP_ERR_INVALID_PID;

  _cmd_name.clear();
  _state = 0;
  _session = _tty_nr = 0;
  _flags = _minflt = _cminflt = _majflt = _cmajflt = _utime = _stime;
  _cutime = _cstime = _priority = _nice = _itrealvalue = 0;
  _starttime = _vsize = _rss = _rlim = 0;

  _startcode = _endcode = _startstack = 0;
  _kstkesp = _kstkeip = _signal = _blocked = _sigignore = _sigcatch
    = _wchan = _nswap = _cnswap = _exit_signal = _processor = 0;

  lStatus = 0;
  return lStatus;
}

//------------------------------------------------------------------------------
// Reload the statm data from the statm file
//------------------------------------------------------------------------------
long
Stat::update(void)
{
  lStatus = MAP_ERR_OK;

  // check if we have proper process ID
  if (getPID() < 0)
    return (lStatus = MAP_ERR_INVALID_PID);

  // read the statm file, there is no need to wait
  char fname[40];
  long tmp;
  ifstream stat_file;
  sprintf(fname,"/proc/%d/stat",getPID());
  stat_file.open(fname);

  if ((stat_file.rdstate() & ifstream::failbit ) != 0 ) {
    lStatus = stat_file.rdstate();
    return lStatus;
  }

  stat_file >> _pid;
  stat_file >> _cmd_name;
  stat_file >> _state >> _ppid >> _pgrp >> _session >> _tty_nr >> _tpgid;
  stat_file >> _flags >> _minflt >> _cminflt >> _majflt >> _cmajflt >> _utime
	    >> _stime >> _cutime >> _cstime >> _priority >> _nice >> tmp
	    >> _itrealvalue >> _starttime >> _vsize >> _rss >> _rlim;
  stat_file >> _startcode >> _endcode >> _startstack
	    >> _kstkesp >> _kstkeip >> _signal >> _blocked >> _sigignore >> _sigcatch
	    >> _wchan >> _nswap >> _cnswap >> _exit_signal >> _processor;

  // remove the parenthesis
  _cmd_name = _cmd_name.substr(1, _cmd_name.length()-2);

  stat_file.close();

  return lStatus;
}

//------------------------------------------------------------------------------
// store the statm file for a process (for debugging purposes).
// The storage is in the current running directory;
// no errors are reported
//------------------------------------------------------------------------------
int Stat::StoreDebug(void)
{
  char command[PATH_MAX*2];
  sprintf(command, "cp -f /proc/%d/stat ./stat_%d", getPID(), getPID());
  system(command);

  return 0;
}
//==============================================================================

