/* -*- c -*- */
/*
 * parser_module.c
 *
 * Module to read lots of memory information from the inside of the kernel.
 * Just cat on the /proc/parser_module file to see all the
 * supported commands.
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * Adopted from: "The Linux Kernel Module Programming Guide"
 *                by Peter Jay Salzman & Ori Pomerantz
 *
 * Parts of the code in generating the memory and flag page maps -
 * proc_pid_read_map_info() and below - were adopted from MontaVista
 * Linux CEE version 4.0.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * -----------------------------------------------------------------------------
 * $Id: parser_module.c,v 1.24 2007/05/23 22:00:07 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

/* Standard headers for LKMs */
//#include <linux/config.h>
#include <linux/kernel.h>   /* We're doing kernel work */
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/pagemap.h>
#include <linux/highmem.h>
#include <linux/version.h>
#include <linux/string.h>

// Some architecture specific stuff
#if defined(__i386__)
#endif

#if defined(__arm__)
//#error "arm defined"
#endif

/* Necessary because we use dynamic memory allocation */
#include <linux/slab.h>
#include <linux/smp.h>
#include <linux/mm.h>

#if (LINUX_VERSION_CODE > KERNEL_VERSION(2,4,20))
#include <linux/mm_inline.h>
#endif

/* Necessary because we use proc fs */
#include <linux/proc_fs.h>
#include <linux/time.h>

//#include <asm/segment.h>
#include <asm/uaccess.h>  /* for get_user and put_user */
#include <asm/pgtable.h>
#include <unistd.h>
#include <asm/semaphore.h>
#include <asm/processor.h>

#include <linux/resource.h>

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,4,1))
#error "Only kernels 2.4 and above are supported."
#endif

//#include <stdio.h>

// End of file character.
// Some things throughout the library rely on this being -1.
#ifndef EOF
#define EOF (-1)
#endif

/**
 * Some useful macros
 */
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,1) && !REDHAT)
#define TASK_PARENT(p) (p->p_pptr)
#else
#define TASK_PARENT(p) (p->parent)
#endif

// for versions 2.6 the p_pptr was replaced with parent
// more desriptive but the same thing
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,1))
#define TASK_GROUP(p) (p->pgrp)
#else
#define TASK_GROUP(p) process_group(p)
#endif

#define CEL2PAGE_SIZE(x) ((int)(x/PAGE_SIZE))*PAGE_SIZE + (x%PAGE_SIZE == 0 ? 0 : PAGE_SIZE);
#define CUP_CNT_VAL(x) ((x > 99) ? 99 : x)

/**
 * Here we keep the last message received, to prove that we can process our input
 */
#define MESSAGE_LENGTH 80
#define MS_EXTRA 100

enum parser_cmds_enum{
  INVALID_CMD = -1,
  CMD_VER = 0,
  CMD_LWP,
  CMD_KPROC,
  CMD_MAIN_PID,
  CMD_STACK_MAP,
  CMD_MEM_CNT_MAP,
  CMD_MEM_FLAG_MAP,
  CMD_MEM_RSS_MAP,
  CMD_MEM_DIRTY_MAP,
  CMD_MEM_SEGMENT_MAP,
  CMD_LAST
};

/* number of commands that the module supports */
#define CMD_NUM  CMD_LAST
static const char *const cmd_name[CMD_NUM] = {"VER",
					      "LWP",
					      "KPROC",
					      "MAIN_PID",
					      "STACK_MAP",
					      "MEM_CNT_MAP",
					      "MEM_FLAG_MAP",
					      "MEM_RSS_MAP",
					      "MEM_DIRTY_MAP",
					      "MEM_SEGMENT_MAP"};
static const char *const cmd_name_help[CMD_NUM] =
  {"string. Returns the numeric version of the module.",
   "T/F. Returns the flag if the supplied process is LWP.",
   "T/F. Returns the flag if the supplied process is the kernel process",
   "int. Returns the PID of the main process to which the process whose PID was sent belongs to.",
   "string. Returns the space separated start and end virtual addresses of stack segments "
   "of the supplied process.",
   "string. Returns the memory map of the process specified by the passed PID.",
   "string. Returns the memory flag map of the process specified by the passed PID.",
   "string. Returns the RSS map of the process specified by the passed PID.",
   "string. Returns the dirty pages map of the process specified by the passed PID.",
   "string. Rerurns the process' address space memory segment information. See manual for format."
  };

enum err_codes{
  ERR_SYSTEM = 0,
  ERR_PARSE_CMD,
  ERR_INVALID_CMD,
  ERR_NO_SPACE,
  ERR_INVALID_PID,
  ERR_LAST
};
static char *err_message[] = {"Unknown system error (miscellaneous error)",
			      "Error parsing input command",
			      "Unrecognized command",
			      "Not enough space in the kernel to perform the required operation",
			      "Task with the specified PID is not found"};

enum map_type {
  MAP_TYPE_CNT = 0,
  MAP_TYPE_FLAG,
  MAP_TYPE_LAST
};
enum count_type {
  MAP_CNT_RSS = 0,
  MAP_CNT_DIRTY,
  MAP_CNT_LAST
};
#define MODULE_VERS "1.5"
#define MODULE_NAME "parser_module"

/******************************************************************************/
/* Global static variables                                                    */
/******************************************************************************/
static char in_command[MESSAGE_LENGTH] = "";
static struct proc_dir_entry *Our_Proc_File;
static char * loc_str = NULL;
static size_t loc_len = 0;

static int parser_finished = 0;
static struct task_struct *last_task = NULL;
static int which_vma = 0;
struct task_struct *task;
static struct semaphore pm_sem;

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int pm_parser_tok_cmd(char *in_cmd, long *cmd, long *value);
static int pm_get_main_task_pid(int value);
static int do_error_msg(char *buf, unsigned int len, long err_code,
			int line_num, char *misc_buf);
static int proc_pid_read_map_info(char *buf, const int len, const long pid,
				  const int map_type);
static int pm_map_pmd_pages(pmd_t *dir, int map_type, unsigned long address,
			    unsigned long end, unsigned char *data_buf);
static int pm_map_pgd_pages(pgd_t *dir, int map_type, unsigned long address,
			    unsigned long end, unsigned char *data_buf);
static int pm_map_physical_pages(struct mm_struct *mm, struct vm_area_struct *vma,
				 int map_type, unsigned char *data_buf);
static int pm_map_segment(char *buf, const int len, const long pid,
			  const int type);
static int proc_pid_read_module_count(char *buf, const int len, const long pid,
				      const int type);
static int proc_pid_read_stack_info(char *buf, const int len, const long pid,
				    const int type);
/* static int proc_pid_read_active_map(char *buf, const int len, const long pid, const int type); */
static inline int do_clean(void);
static inline struct task_struct *pm_get_task_by_pid(const int pid);
static inline struct mm_struct *pm_get_task_mm_struct(struct task_struct *task);
static inline int pm_put_task_mm_struct(struct mm_struct *mm);

/******************************************************************************/
/* Small useful utility function that parses the command sent by the user.
 * So far this function only supports one parameter for one initial command.
 * negative number is returned in the case of error. */
/* For future reference: ./fs/autofs4/inode.c#100 has one useful parsing code */
static int pm_parser_tok_cmd(char *in_cmd, long *cmd, long *value)
{
  char *stp, *endp;
  unsigned int cmd_len = 0;

  if (strlen(in_cmd) == 0)
    return -1;

  if ((stp = strchr(in_cmd,' ')) != NULL){
    *stp++;
    cmd_len = stp - in_cmd - 1;
  }else if ((stp = strchr(in_cmd,';')) != NULL){
    *stp++;
    cmd_len = stp - in_cmd - 1;
  }

  *cmd = INVALID_CMD;
  if (strncmp(in_cmd, cmd_name[CMD_VER], cmd_len) == 0){
    if (!stp || !*stp){
      return 0;
    }

    *cmd = CMD_VER;
    *value = 0;
  }else if (strncmp(in_cmd, cmd_name[CMD_LWP], cmd_len) == 0){
    if (!stp || !*stp){
      return 0;
    }

    *cmd = CMD_LWP;
    *value = simple_strtol(stp,&endp,0);
  }else if (strncmp(in_cmd, cmd_name[CMD_KPROC], cmd_len) == 0){
    if (!stp || !*stp){
      return 0;
    }

    *cmd = CMD_KPROC;
    *value = simple_strtol(stp,&endp,0);
  }else if (strncmp(in_cmd, cmd_name[CMD_MAIN_PID], cmd_len) == 0){
    if (!stp || !*stp){
      return 0;
    }

    *cmd = CMD_MAIN_PID;
    *value = simple_strtol(stp,&endp,0);
  }else if (strncmp(in_cmd, cmd_name[CMD_STACK_MAP], cmd_len) == 0){
    if (!stp || !*stp){
      return 0;
    }

    *cmd = CMD_STACK_MAP;
    *value = simple_strtol(stp,&endp,0);
  }else if (strncmp(in_cmd, cmd_name[CMD_MEM_CNT_MAP], cmd_len) == 0){
    if (!stp || !*stp){
      return 0;
    }

    *cmd = CMD_MEM_CNT_MAP;
    *value = simple_strtol(stp,&endp,0);
  }else if (strncmp(in_cmd, cmd_name[CMD_MEM_FLAG_MAP], cmd_len) == 0){
    if (!stp || !*stp){
      return 0;
    }

    *cmd = CMD_MEM_FLAG_MAP;
    *value = simple_strtol(stp,&endp,0);
  }else if (strncmp(in_cmd, cmd_name[CMD_MEM_RSS_MAP], cmd_len) == 0){
    if (!stp || !*stp){
      return 0;
    }

    *cmd = CMD_MEM_RSS_MAP;
    *value = simple_strtol(stp,&endp,0);
  }else if (strncmp(in_cmd, cmd_name[CMD_MEM_DIRTY_MAP], cmd_len) == 0){
    if (!stp || !*stp){
      return 0;
    }

    *cmd = CMD_MEM_DIRTY_MAP;
    *value = simple_strtol(stp,&endp,0);
  }else if (strncmp(in_cmd, cmd_name[CMD_MEM_SEGMENT_MAP], cmd_len) == 0){
    if (!stp || !*stp){
      return 0;
    }

    *cmd = CMD_MEM_SEGMENT_MAP;
    *value = simple_strtol(stp,&endp,0);
  } else {
    /*    *cmd = INVALID_CMD; */
  }

  return 0;
}

static int do_error_msg(char *buf, unsigned int len, long err_code, int line_num, char *misc_buf)
{
  printk(KERN_ERR "Error:%s.%d: %s.\n",__FILE__,line_num,err_message[err_code]);

  if (misc_buf == NULL || (strlen(err_message[err_code])+11) == len)
    sprintf(buf,"-1, \"%.*s\"\n",
	    (len-9),err_message[err_code]);
  else
    sprintf(buf,"-1, \"%.*s:%*s\"\n",
	    (len-9),err_message[err_code],(len-10-strlen(err_message[err_code])),misc_buf);

  return 0;
}

/*----------------------------------------------------------------------------*/
/* Just clean the module srtuctures.                                          */
/*----------------------------------------------------------------------------*/
static inline int do_clean(void)
{
  parser_finished = 0;
  last_task = NULL;
  in_command[0]='\0';
  which_vma = 0;
  task = NULL;

  if (loc_str != NULL){
    kfree(loc_str);
    loc_str = NULL;
    loc_len = 0;
  }

  return 0;
}

/*----------------------------------------------------------------------------*/
/* Wrapper for the task structure getting functionality                       */
/*----------------------------------------------------------------------------*/
static inline struct task_struct *pm_get_task_by_pid(const int pid)
{
  struct task_struct *task;

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,1) || REDHAT)
  read_lock_irq(&tasklist_lock);
#else
  read_lock(&tasklist_lock);
#endif

  task = find_task_by_pid(pid);
  if (task)
    get_task_struct(task);

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,1) || REDHAT)
  read_unlock_irq(&tasklist_lock);
#else
  read_unlock(&tasklist_lock);
#endif

  return task;
}

/*----------------------------------------------------------------------------*/
/* This function will find the PID of the main process that has the passed process
 * as the thread. */
/*----------------------------------------------------------------------------*/
static inline int pm_get_main_task_pid(int value)
{
  int main_pid = 0;
  struct task_struct *p;

  p = pm_get_task_by_pid(value);
  if (p == NULL)
    return -1;

  /* Loop until we hit the process 0 or find the parent */
  while ((p->pid != 0) && (p !=  TASK_PARENT(p))) {
    if (p->mm == TASK_PARENT(p)->mm){
      p = TASK_PARENT(p);
    }else{
      main_pid = (p->pid);
      break;
    }
  }

  return main_pid;
}

/*----------------------------------------------------------------------------*/
/* This function will get the memory map structure pointer for the
 * specified task.
 * NOTE: don't forget to release the semaphore after use */
/*----------------------------------------------------------------------------*/
static inline struct mm_struct *pm_get_task_mm_struct(struct task_struct *task)
{

  struct mm_struct *mm = NULL;

  /* Get the mm from this task. All this code is in a macro in 2.6 */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,1) || REDHAT)
  mm = get_task_mm(task);
#else
  task_lock(task);
  mm = task->mm;
  if (mm){
    atomic_inc(&mm->mm_users);
  }
  task_unlock(task);
#endif

  return mm;
}

/*----------------------------------------------------------------------------*/
/* This function will release the mm semaphore after use */
/*----------------------------------------------------------------------------*/
static inline int pm_put_task_mm_struct(struct mm_struct *mm)
{
  if (mm == NULL)
    return -1;

  /* Older kernels (<2.6) don't have mmput exported. RedHat, however, does.
   * As a result we will just decrement the map reference counter and hope it won't cause
   * significant damage. If you know a better solution, let me know. */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,1) || REDHAT)
  mmput(mm);
#else
  atomic_dec(&mm->mm_users);
#endif

  return 0;
}

/*----------------------------------------------------------------------------*/
/* The last function in the physical page info output.                        */
/*----------------------------------------------------------------------------*/
static int pm_map_pmd_pages(pmd_t *pmd, int map_type,
			    unsigned long address, unsigned long end,
			    unsigned char *data_buf)
{
  pte_t *ptep, pte;
  unsigned long pmd_end;
  unsigned long value;
  struct page *page;
  int index;
  const unsigned long mask = ~((~0x0) << (sizeof(char)*8));

#if (LINUX_VERSION_CODE > KERNEL_VERSION(2,4,20) || REDHAT)
  unsigned long pfn;
#endif

  if (pmd_none(*pmd) || pmd_bad(*pmd))
    return 0;

  pmd_end = (address + PMD_SIZE) & PMD_MASK;
  if (end > pmd_end)
    end = pmd_end;

  index = 0;
#if (LINUX_VERSION_CODE > KERNEL_VERSION(2,4,20) || REDHAT)
  do {
    ptep = pte_offset_map(pmd, address);

    if (ptep){
      pte = *ptep;
      pte_unmap(ptep);

      if (!pte_none(pte)){
	if (pte_present(pte)){

	  pfn = pte_pfn(pte);
	  if (pfn_valid(pfn)){
	    page = pfn_to_page(pfn);
	    if (map_type == MAP_TYPE_CNT) {
	      value = (unsigned long)page_count(page);
	    } else {
	      value = (page->flags);
	    }
#ifdef DEBUG2
	    if (value > 99) printk(KERN_DEBUG "%s.%d: big|value=%lX, %X, %X, addr=0x%lX\n",
				   __FILE__,__LINE__,value,page_count(page),(page->flags),address);
#endif
	    data_buf[index] = value & mask;
	  }
	}
      }
    }
    address += PAGE_SIZE;
    index++;
  } while (address && (address < end));

#else

  ptep = pte_offset(pmd, address);

  do {
    pte = *ptep;

    if (!pte_none(pte)){
      if (pte_present(pte)){

	page = pte_page(pte);
	if (VALID_PAGE(page) && (!PageReserved(page))){
	  if (map_type == MAP_TYPE_CNT) {
	    value = (unsigned long)page_count(page);
	  } else {
	    value = (page->flags);
	  }
#ifdef DEBUG2
	  if (value > 99) printk(KERN_DEBUG "%s.%d: big|value=%lX, %X, %X, addr=0x%lX\n",
				 __FILE__,__LINE__,value,page_count(page),(page->flags),address);
#endif
	  data_buf[index] = value & mask;
	}
      }
    }

    ptep++;
    address += PAGE_SIZE;
    index++;
  } while (address && (address < end));
#endif

  return 0;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
static int pm_map_pgd_pages(pgd_t *dir, int map_type,
			    unsigned long address, unsigned long end,
			    unsigned char *data_buf)
{
  pmd_t * pmdir;
  unsigned long pgd_end;

  if (pgd_none(*dir) || pgd_bad(*dir))
    return 0;

  pmdir = pmd_offset(dir, address);
  pgd_end = (address + PGDIR_SIZE) & PGDIR_MASK;
  //address &= ~PGDIR_MASK;
  if (pgd_end && (end > pgd_end))
    end = pgd_end;

  do {
    pm_map_pmd_pages(pmdir, map_type, address, end, data_buf);
    address = (address + PMD_SIZE) & PMD_MASK;
    pmdir++;
  } while (address && (address < end));

  return 0;
}

/*----------------------------------------------------------------------------*/
/* Set the page counters to the specified data buffer.
 * This function assumes that the buffer has enough space for all the pages */
/*----------------------------------------------------------------------------*/
static int pm_map_physical_pages(struct mm_struct *mm,
				 struct vm_area_struct *vma,
				 int map_type,
				 unsigned char *data_buf)
{
  pgd_t *pgdir;
  unsigned long end, address, next_addr;

  end = vma->vm_end;
  address = vma->vm_start;
  if (address > VMALLOC_END)
    return 0;

  spin_lock(&mm->page_table_lock);
  {
    pgdir = pgd_offset(mm, address);
    do {
      pm_map_pgd_pages(pgdir, map_type, address, end, data_buf);
      next_addr = (address + PGDIR_SIZE) & PGDIR_MASK;
      data_buf += ((next_addr - address) >> PAGE_SHIFT);
      address = next_addr;
      pgdir++;
    } while (address && (address < end));
  }
  spin_unlock(&mm->page_table_lock);

  return 0;
}

/*----------------------------------------------------------------------------*/
/* This function will output the specific page data to the passed buffer.
 * For the memmap page usage count each char will contain a number from zero
 * to the share depth.
 * A zero means the page is not in memory for this virtual address.
 * A non-zero means the page is being used by the app, a number
 * larger than 1 means the page is shared, and represents the share depth.
 * It will also check if the current PID wasn't used before and the output was incomplete
 * For the page flag output each character will contain the flag of each
 * physical page.
 * See http://lxr.linux.no/source/include/linux/mm.h?v=2.4.20#L201 for more info */
/*----------------------------------------------------------------------------*/
int proc_pid_read_map_info(char *buf, const int len, const long pid, const int map_type)
{
  struct mm_struct *mm = NULL;
  struct vm_area_struct * map = NULL;
  unsigned char *page_cnt_arr = NULL;
  long retval = 0;
  int index, pages=0, line_amount, array_size;
  int this_incarnation = 0;
  const int buf_tail = 6;

  if (which_vma == -1 || len <= 0) {
    return 0;
  }

#ifdef DEBUG
  printk(KERN_DEBUG "%s.%d: proc_read_pid_info|len=%d,pid=%ld\n", __FILE__,__LINE__,len,pid);
#endif
  buf[0] = 0;

  //  lock_kernel();
  if ((task = pm_get_task_by_pid(pid)) == NULL){
    return -1;
  }

  mm = pm_get_task_mm_struct(task);
  if (!mm) {
    return 0;
  }
  /* -- */

  /* get the initial memory map */
  down_read(&mm->mmap_sem);
  {
    map = mm->mmap;

    /* Since we can't usually get all the data back in a single call, especially with
     * 1K buffers, we mark the vma we are processing with which_vma. If we have to
     * reiterate through we'll skip the vma's we processed the first time through.
     * This is how we keep track of where we are in a non-persistent file without
     * being a fixed length and fixed format file.
     */
    if (which_vma > 0) {
      this_incarnation = which_vma;
      while ((this_incarnation-- >= 0) && map != NULL) {
	map = map->vm_next;
      }
    }
    /* ----------------- */

    /* ================= */
    /* main process loop */
    this_incarnation = 0;
    page_cnt_arr = NULL;
    retval = 0;
    array_size = 0;
    while (map) {

      /* Little caveat here. */
      /* In case this vm_area_struct points to a device, not a file, all the fields will be 0 */
      /* At this point we are only interested in the user space memory */
      struct file *file = map->vm_file;
      umode_t mode = 0;

      if (file)
	mode = map->vm_file->f_dentry->d_inode->i_mode;

      if ((file == NULL) || ((file) && (S_ISREG(mode) || S_ISDIR(mode)))) {
	pages = (map->vm_end - map->vm_start) / PAGE_SIZE;
	if (pages == 0)
	  pages = 1;
      } else {
	pages = 1;
      }

      /* allocate internal buffer. The out buffer came from the outside */
      /* NOTE: potential optimization is not to allocate if it is already big enough */
      if (pages > array_size){
	array_size = (pages / PAGE_SIZE + 1)*PAGE_SIZE;

	if (page_cnt_arr != NULL)
	  kfree(page_cnt_arr);
	page_cnt_arr = NULL;
	if ((page_cnt_arr = (char *)kmalloc(array_size, GFP_KERNEL)) == NULL) {
	  retval = -ENOMEM;
	  break;
	}
      }
      for (index = 0; index < array_size; index++)
	page_cnt_arr[index] = 0;

      line_amount = (pages * 2) + 10;
      if (retval + line_amount > len - buf_tail) {
	break;
      }

      /* get the page counters in the array */
      if ((file == NULL) || ((file) && (S_ISREG(mode) || S_ISDIR(mode))))
	pm_map_physical_pages(map->vm_mm, map, map_type, page_cnt_arr);

      map = map->vm_next;

      which_vma++;
      this_incarnation++;
      if (map_type == MAP_TYPE_CNT) {
	for (index = 0; (index < pages - 1) && (retval < len - buf_tail); index++) {
	  retval += sprintf(buf + retval, "%2d", CUP_CNT_VAL(page_cnt_arr[index]));
	}
      } else {
	for (index = 0; (index < pages - 1) && (retval < len - buf_tail); index++) {
	  retval += sprintf(buf + retval, "%2X", page_cnt_arr[index]);
	}
      }
      if (map_type == MAP_TYPE_CNT) {
	retval += sprintf(buf + retval, "%2d\n",CUP_CNT_VAL(page_cnt_arr[index]));
      } else {
	retval += sprintf(buf + retval, "%2X\n", page_cnt_arr[index]);
      }
    }
    /* ----------------- */
  }
  up_read(&mm->mmap_sem);

  pm_put_task_mm_struct(mm);
  // unlock_kernel();

  // set the end of the buffer properly
  if (map == NULL) {
    which_vma = 0;
    index = strlen(buf);
    buf[index] = EOF;
    buf[index+1] = 0;

#ifdef DEBUG
    printk(KERN_DEBUG "%s.%d: index=%d\n",__FILE__,__LINE__,index);
#endif
  }
  if (page_cnt_arr != NULL)
    kfree(page_cnt_arr);

  return retval;
}

/*----------------------------------------------------------------------------*/
/* This function will output the array of per module accumulated counters in
 * this process to the passed buffer. The type of counters that the function
 * will collect is specified by the type parameter. */
/*----------------------------------------------------------------------------*/
int proc_pid_read_module_count(char *buf, const int len, const long pid, const int type)
{
  struct mm_struct *mm = NULL;
  struct vm_area_struct * map = NULL;
  unsigned int RSS_pages = 0;
  long retval = 0;
  int index, array_size;
  const int buf_tail = 10;

  if (len <= 0)
    return 0;

#ifdef DEBUG
  printk(KERN_DEBUG "%s.%d: proc_pid_read_module_count|len=%d,pid=%ld\n",
	 __FILE__,__LINE__,len,pid);
#endif
  buf[0] = 0;

  //  lock_kernel();
  if ((task = pm_get_task_by_pid(pid)) == NULL){
    return -1;
  }

  mm = pm_get_task_mm_struct(task);
  if (!mm) {
    return 0;
  }
  /* -- */

  /* get the initial memory map */
  down_read(&mm->mmap_sem);
  {
    map = mm->mmap;

    /* ================= */
    /* main process loop */
    retval = array_size = 0;
    while (map) {

      /* Little caveat here. */
      /* In case this vm_area_struct points to a device, not a file, the RSS = 0 */
      /* At this point we are only interested in the user space memory */
      /* NOTE: This method is useful to the entire memory map output.  */
      /* However, it will nto be used for the RSS data due to the concistency purposes. */

      /* get the number of pages in this segment */

      pgd_t *pgdir;
      pmd_t *pmdir;
      pte_t *ptep, pte;
      unsigned long page_addr;
      unsigned long end, address;
      struct page *page;

#if (LINUX_VERSION_CODE > KERNEL_VERSION(2,4,20) || REDHAT)
      unsigned long pfn;
#endif

      end = map->vm_end;
      address = map->vm_start;
      RSS_pages = 0;

      spin_lock(&mm->page_table_lock);
      {
	for (page_addr = address; page_addr < end; page_addr += PAGE_SIZE) {
	  pgdir = pgd_offset(mm, page_addr);
	  if (pgd_none(*pgdir) || pgd_bad(*pgdir)) continue;
	  pmdir = pmd_offset(pgdir, page_addr);
	  if (pmd_none(*pmdir) || pmd_bad(*pmdir)) continue;

#if (LINUX_VERSION_CODE > KERNEL_VERSION(2,4,20) || REDHAT)
	  ptep = pte_offset_map(pmdir, page_addr);
	  if (ptep){
	    pte = *ptep;
	    pte_unmap(ptep);

	    if (!pte_none(pte)){
	      if (pte_present(pte)){
		if (type == MAP_CNT_RSS){
		  RSS_pages++;
		}else{
		  pfn = pte_pfn(pte);
		  if (pfn_valid(pfn)){
		    page = pfn_to_page(pfn);
		    RSS_pages += (PageDirty(page) || pte_dirty(pte)) ? 1 : 0;
		  }
		}
	      }
	    }
	  }
#else
	  ptep = pte_offset(pmdir, page_addr);
	  pte = *ptep;

	  if (!pte_none(pte)){
	    if (pte_present(pte)){
	      if (type == MAP_CNT_RSS){
		RSS_pages++;
	      }else{
		page = pte_page(pte);
		if (VALID_PAGE(page) && (!PageReserved(page))){
		  RSS_pages += PageDirty(page);
		}
	      }
	    }
	  }
#endif
	}
      }
      spin_unlock(&mm->page_table_lock);

      map = map->vm_next;

      if (retval < len-buf_tail){
	retval += sprintf(buf + retval, "%u ", RSS_pages);
      }else{
	// leave the loop, let the reader come back with the bigger buffer.
	break;
      }
    }
    /* ----------------- */
  }
  up_read(&mm->mmap_sem);

  pm_put_task_mm_struct(mm);
  // unlock_kernel();

  // set the end of the buffer properly
  if (map == NULL) {
    index = strlen(buf);
    buf[index] = EOF;
    buf[index+1] = 0;

#ifdef DEBUG
    printk(KERN_DEBUG "%s.%d: index=%d\n",__FILE__,__LINE__,index);
#endif
  }

  return retval;
}

/*----------------------------------------------------------------------------*/
/* This function will output the array of RSS sizes for each module in
 * this process to the passed buffer.*/
/*----------------------------------------------------------------------------*/
static int proc_pid_read_stack_info(char *buf,
				    const int len,
				    const long pid,
				    const int type)
{
  struct mm_struct *mm = NULL;
  struct vm_area_struct * map = NULL;
  long retval = 0;
  int index;
  const int buf_tail = 10;

  if (len <= 0)
    return 0;

#ifdef DEBUG
  printk(KERN_DEBUG "%s.%d: proc_pid_read_stack_info|len=%d,pid=%ld\n",
	 __FILE__,__LINE__,len,pid);
#endif
  buf[0] = 0;

  //  lock_kernel();
  if ((task = pm_get_task_by_pid(pid)) == NULL){
    return -1;
  }

  mm = pm_get_task_mm_struct(task);
  if (!mm) {
    return 0;
  }
  /* -- */

  /* get the initial memory map */
  down_read(&mm->mmap_sem);
  {
    map = mm->mmap;

    /* ================= */
    /* main process loop */
    while (map) {

      /* Stack is the segment that grows down: VM_GROWSDOWN */
      if (map->vm_flags & VM_GROWSDOWN){
	if (retval < len-buf_tail){
	  retval += sprintf(buf + retval, "%.8lX %.8lX\n", map->vm_start, map->vm_end);
	} else {
	  map = NULL;
	  break;
	}
      }
      map = map->vm_next;
    }
    /* ----------------- */
  }
  up_read(&mm->mmap_sem);

  pm_put_task_mm_struct(mm);
  // unlock_kernel();

  // set the end of the buffer properly
  if (map == NULL) {
    index = retval;
    buf[index] = EOF;
    buf[index+1] = 0;

#ifdef DEBUG
    printk(KERN_DEBUG "%s.%d: index=%d: %s\n",__FILE__,__LINE__,index,buf);
#endif
  }

  return retval;
}

/*----------------------------------------------------------------------------*/
/* This function will output the memory segment values (and some other stuff)
 * from the particular process' address space into the supplied buffer.       */
/*----------------------------------------------------------------------------*/
static int pm_map_segment(char *buf,
			  const int len,
			  const long pid,
			  const int type)
{
  struct mm_struct *mm = NULL;
  long retval = 0;
  int index;
  const int buf_tail = 10;

  if (len <= 0)
    return 0;

#ifdef DEBUG
  printk(KERN_DEBUG "%s.%d: %s|len=%d,pid=%ld\n",
	 __FILE__,__LINE__,__FUNCTION__,len,pid);
#endif
  buf[0] = 0;

  //  lock_kernel();
  if ((task = pm_get_task_by_pid(pid)) == NULL){
    return -1;
  }

  mm = pm_get_task_mm_struct(task);
  if (!mm) {
    return 0;
  }
  /* -- */

  /* get the initial memory map */
  down_read(&mm->mmap_sem);
  {
    retval += sprintf(buf + retval, "%.8lX %.8lX %.8lX %.8lX %.8lX %.8lX %.8lX",
		      mm->start_code, mm->end_code, mm->start_data, mm->end_data,
		      mm->start_brk, mm->brk, mm->start_stack);

    retval += sprintf(buf + retval, " %.8lX %.8lX %.8lX %.8lX",
		      mm->arg_start, mm->arg_end, mm->env_start, mm->env_end);

    retval += sprintf(buf + retval, " %ld %ld %ld\n",
		      mm->rss, mm->total_vm, mm->locked_vm);
  }
  up_read(&mm->mmap_sem);

  pm_put_task_mm_struct(mm);
  // unlock_kernel();

  // set the end of the buffer properly
  index = retval;
  buf[index] = EOF;
  buf[index+1] = 0;

#ifdef DEBUG
    printk(KERN_DEBUG "%s.%d: index=%d\n",__FILE__,__LINE__,index);
#endif

  return retval;
}

/*----------------------------------------------------------------------------*/
/* This function will output the array of active and inactive page counts
 * for each module in this process to the passed buffer.*/
/*----------------------------------------------------------------------------*/
/* int proc_pid_read_active_map(char *buf, const int len, const long pid, const int type) */

/*----------------------------------------------------------------------------*/
/* Since we use the file operations struct, we can't
 * use the special proc output provisions - we have to
 * use a standard read function, which is this function
 * NOTE: this code is not thread-safe. */
/*----------------------------------------------------------------------------*/
static ssize_t module_output(struct file *file,   /* The file read */
			     char *buf,           /* The buffer to put data to (in the user segment) */
			     size_t len,          /* The length of the buffer */
			     loff_t *offset)      /* Offset in the file - ignore */
{
  unsigned int i = 0;
  int flag = 0;
  int off;
  long retval = 0;
  size_t t_len = 0;
  long cmd;
  struct timeval tv;
  struct task_struct *p, *task;

  off = (int)*offset;

#ifdef DEBUG
  printk(KERN_DEBUG "%s.%d: module_output:len=%d|loc_len=%d|in_command=%s|offset=%d|l_task=0x%X\n",
	 __FILE__,__LINE__,len,loc_len,in_command,off,last_task);
#endif

  /* Insanity check */
  if (*offset > LONG_MAX)
    return retval;
  if (len == 0)
    return retval;

  if (down_interruptible(&pm_sem))
    return -ERESTARTSYS;

  /* We return 0 to indicate end of file, that we have no more information.
   * Otherwise, processes will continue to read from us in an endless loop.  */
  if (parser_finished == 1) {
    parser_finished = 0;
    in_command[0]='\0';
    last_task = NULL;
    *offset=0;

    up(&pm_sem);
    return 0;
  }

  if (off == 0 && last_task != NULL)
    last_task = NULL;

  /* allocate an array based on the passed length, if needed.
   * One extra page as a buffer */
  t_len = CEL2PAGE_SIZE(len);

  if (t_len > loc_len){
    retval = -ENOMEM;
    if (loc_str != NULL)
      kfree(loc_str);

    loc_str = (char *)kmalloc(t_len+PAGE_SIZE,GFP_KERNEL);
    if (loc_str == NULL){
      loc_len = 0;

      loc_str = (char *)kmalloc(PAGE_SIZE,GFP_KERNEL);
      if (loc_str != NULL){
	loc_len = PAGE_SIZE;
	do_error_msg(loc_str, loc_len, ERR_NO_SPACE, __LINE__, NULL);
	goto out;
      }
      /* This should never happen */
      up(&pm_sem);
      return retval;
    }
    loc_len = t_len;
  }

  loc_str[0] = '\0';
  t_len = 0;

  /* We use put_user to copy the string from the kernel's
   * memory segment to the memory segment of the process
   * that called us. get_user, BTW, is used for the reverse. */

  // if the user just reads the file, print all the information and help
  if (strlen(in_command) == 0){

    // we can write to loc_str because it cannot be smaller then 4k
    // NOTE: make sure there is less help than one page.
    if (last_task == NULL) {
      t_len = sprintf(loc_str, " %s:%s\n", MODULE_NAME, MODULE_VERS);

      for(i=0;i<CMD_NUM;i++){
	t_len += sprintf(&loc_str[t_len],"%s: %s\n",cmd_name[i], cmd_name_help[i]);
      }
      t_len += sprintf(&loc_str[t_len],"\n  pid: tgid: pgrp: mm     :father: main\n");

     // if we wrote more than the user wants to see - leave.
      if (t_len > (len+off)){
	flag = 1;
	goto out;
      }
    } else {
      off = 0;
    }

    // loop through all the processes and list them and their threads
    // for some reason they changed tasks to processes after 2.4.20
    //
    //for (p = &init_task ; (p = p->next_task) != &init_task ; ){
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,1) || REDHAT)
    read_lock_irq(&tasklist_lock);
    for_each_process(p)
#else
    read_lock(&tasklist_lock);
    for_each_task(p)
#endif
    {
      // we need to check if there are some processors that we still didn't print from
      // the previous run of this function. Pointer to the last task_struct is stored in
      // last_task variable. Here we need to skip all the tasks before last one.
      // Last_task could have been used directly and starting the loop from it,
      // but it is less secure (what if the task was killed in the mean time).
      if (last_task != NULL && last_task != p)
	continue;
      else
	last_task = NULL;

      t_len += sprintf(&loc_str[t_len],"%5d:%5d:%5d:%p:%6d:%5d ",
		       p->pid,p->tgid, TASK_GROUP(p), p->mm, TASK_PARENT(p)->pid,
		       pm_get_main_task_pid(p->pid));

      //for_each_thread(task){
      for(task=next_thread(p);task!=p;task=next_thread(task)){
	t_len += sprintf(&loc_str[t_len],"{%03d}", task->pid);
	if (t_len > (off+len-MESSAGE_LENGTH)){
	  flag = 1;
	  break;
	}
      }

      strcat(loc_str,"\n");
      t_len++;
      if (t_len > (off+len-MESSAGE_LENGTH)){
	flag = 1;
	break;
      }
    }

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,1) || REDHAT)
    read_unlock_irq(&tasklist_lock);
#else
    read_unlock(&tasklist_lock);
#endif

    last_task = p;
    if (flag == 0)
      t_len += sprintf(&loc_str[t_len]," E%d\n", t_len);

  }else{
    long value, tmp;

    // give the user what he asked for
    if (pm_parser_tok_cmd(in_command, &cmd, &value) < 0){
      do_error_msg(loc_str, loc_len, ERR_SYSTEM, __LINE__,in_command);
    }else{
      switch(cmd){
      case CMD_VER:
	sprintf(loc_str, "%s [%s %s]", MODULE_VERS, __DATE__, __TIME__);
	loc_str[strlen(loc_str)] = '\0';

	break;
      case CMD_LWP:
	p = pm_get_task_by_pid(value);
	if(p == NULL){
	  do_error_msg(loc_str, loc_len, ERR_INVALID_PID, __LINE__, NULL);
	}else{
	  task_lock(p);
	  loc_str[0] = ((p->mm == TASK_PARENT(p)->mm) ? '1' : '0');
	  task_unlock(p);
	  loc_str[1] = '\0';
	}

	break;
      case CMD_KPROC:
	p = pm_get_task_by_pid(value);
	if(p == NULL){
	  do_error_msg(loc_str, loc_len, ERR_INVALID_PID, __LINE__, NULL);
	}else{
	  task_lock(p);
	  loc_str[0] = ((p->mm == 0x0) ? '1' : '0');
	  task_unlock(p);
	  loc_str[1] = '\0';
	}

	break;
      case CMD_MAIN_PID:
	tmp = pm_get_main_task_pid(value);
	if (tmp < 0){
	  do_error_msg(loc_str, loc_len, ERR_INVALID_PID, __LINE__, NULL);
	}else{
	  sprintf(loc_str,"%li",tmp);
	}

	break;
      case CMD_STACK_MAP:
	if(proc_pid_read_stack_info(loc_str, loc_len, value, 0) < 0){
	  do_error_msg(loc_str, loc_len, ERR_PARSE_CMD, __LINE__, NULL);
	}

	break;
      case CMD_MEM_CNT_MAP:
	if(proc_pid_read_map_info(loc_str, loc_len, value, MAP_TYPE_CNT) < 0){
	  do_error_msg(loc_str, loc_len, ERR_INVALID_PID, __LINE__, NULL);
	}

	break;
      case CMD_MEM_FLAG_MAP:
	if(proc_pid_read_map_info(loc_str, loc_len, value, MAP_TYPE_FLAG) < 0){
	  do_error_msg(loc_str, loc_len, ERR_INVALID_PID, __LINE__, NULL);
	}

	break;
      case CMD_MEM_RSS_MAP:
	if(proc_pid_read_module_count(loc_str, loc_len, value, MAP_CNT_RSS) < 0){
	  do_error_msg(loc_str, loc_len, ERR_PARSE_CMD, __LINE__, NULL);
	}

	break;
      case CMD_MEM_DIRTY_MAP:
	if(proc_pid_read_module_count(loc_str, loc_len, value, MAP_CNT_DIRTY) < 0){
	  do_error_msg(loc_str, loc_len, ERR_PARSE_CMD, __LINE__, NULL);
	}

	break;
      case CMD_MEM_SEGMENT_MAP:
	if(pm_map_segment(loc_str, loc_len, value, 0) < 0){
	  do_error_msg(loc_str, loc_len, ERR_PARSE_CMD, __LINE__, NULL);
	}

	break;
      default:
	do_error_msg(loc_str, loc_len, ERR_INVALID_CMD, __LINE__, in_command);
      }
    }

    //in_command[0]='\0';
  }

 out:
  for(i=0; i<len && loc_str[off+i]; i++)
    put_user(loc_str[off+i], buf+i);
  *offset += i;

#ifdef DEBUG
  printk(KERN_DEBUG "%s.%d: module_output:i=%d|loc_len=%d|offset=%d|off=%d\n",
	 __FILE__,__LINE__,i,loc_len,(int)*offset,off);
#endif

  /* Notice, we assume here that the size of the message is below len, or it
   * will be received cut. In a real life situation, if the size of the message
   * is more than len then we'd return len and on the second call start filling
   * the buffer with the len+1'th byte of the message. */
  if (flag == 1 && i > 0)
    parser_finished = 0;
  else
    parser_finished = 1;

  up(&pm_sem);
  //kfree(loc_str);

  return i;  /* Return the number of bytes "read" or 0 if done */
}

/*----------------------------------------------------------------------------*/
/* This function receives input from the user when the
 * user writes to the /proc file. */
/*----------------------------------------------------------------------------*/
static ssize_t module_input(struct file *file,   /* The file itself */
			    const char *buf,     /* The buffer with input */
			    size_t length,       /* The buffer's length */
			    loff_t *offset)      /* offset to file - ignore */
{
  unsigned int i;

  if (down_interruptible(&pm_sem))
    return -ERESTARTSYS;
  {
    do_clean();
    /* Put the input into Message, where module_output
     * will later be able to use it */
    for(i=0; i<MESSAGE_LENGTH-1 && i<length; i++)
      get_user(in_command[i], buf+i);

    in_command[i] = '\0';  /* we want a standard, zero terminated string */
  }
  up(&pm_sem);

#ifdef DEBUG
  if (i == 0)
    printk(KERN_INFO "%s.%d: %d-%ld enpty message received.\n",
	   __FILE__,__LINE__,length,(long)*offset);
  else
    printk(KERN_INFO "%s.%d: %d-%ld %s message received.\n",
	   __FILE__,__LINE__,length,(long)*offset,in_command);
#endif

  *offset = 0;
  return i;
}

/*----------------------------------------------------------------------------*/
/* This function decides whether to allow an operation (return zero) or
 * not allow it (return a non-zero which indicates why it is not allowed).
 *
 * The operation can be one of the following values:
 * 0 - Execute (run the "file" - meaningless in our case)
 * 2 - Write (input to the kernel module)
 * 4 - Read (output from the kernel module)
 *
 * This is the real function that checks file permissions. The permissions
 * returned by ls -l are for referece only, and can be overridden here. */
/*----------------------------------------------------------------------------*/
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,1))
static int module_permission(struct inode *inode, int op, struct nameidata *nidata)
#else
static int module_permission(struct inode *inode, int op)
#endif
{
  printk(KERN_INFO "%s.%d: module_permission op=%x\n",__FILE__,__LINE__,op);

  /* We allow everybody to read and write to it */
  if ((op & (MAY_READ | MAY_WRITE)) != 0)
    return 0;

  /* If it's anything else, access is denied */
  return -EACCES;
}


/*----------------------------------------------------------------------------*/
/* The file is opened - we don't really care about that, but it does mean
 * we need to increment the module's reference count. */
/*----------------------------------------------------------------------------*/
int module_open(struct inode *inode, struct file *file)
{
  printk(KERN_INFO "%s.%d: module_open\n",__FILE__,__LINE__);

  // try_module_get(THIS_MODULE);
  in_command[0]='\0';
  return 0;
}


/*----------------------------------------------------------------------------*/
/* The file is closed - again, interesting only because of the reference count. */
/*----------------------------------------------------------------------------*/
int module_close(struct inode *inode, struct file *file)
{
  printk(KERN_INFO "%s.%d: module_close\n",__FILE__,__LINE__);

  // module_put(THIS_MODULE);
  do_clean();

  return 0;  /* success */
}


/*----------------------------------------------------------------------------*/
/* The file is flashed. */
int module_flush(struct file *file)
{
  do_clean();
  return 0;  /* success */
}


/* The file is synced. */
int module_fsync(struct file *file, struct dentry *dentry, int datasync)
{
  do_clean();
  return 0;  /* success */
}


/* Structures to register as the /proc file, with
 * pointers to all the relevant functions. ********** */


/* File operations for our proc file. This is where we
 * place pointers to all the functions called when
 * somebody tries to do something to our file. NULL
 * means we don't want to deal with something. */
static struct file_operations File_Ops_4_Our_Proc_File = {
  .read = module_output,
  .write = module_input,
  .open = module_open,
  .release = module_close,
  .flush = module_flush,
  .fsync = module_fsync,
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,1))
  .owner = THIS_MODULE,
#endif
};


/* Inode operations for our proc file. We need it so
 * we'll have some place to specify the file operations
 * structure we want to use, and the function we use for
 * permissions. It's also possible to specify functions
 * to be called for anything else which could be done to
 * an inode (although we don't bother, we just put NULL). */
static struct inode_operations Inode_Ops_4_Our_Proc_File = {
  .permission = module_permission,	/* check for permissions */
};



/*----------------------------------------------------------------------------*/
/* Module initialization and cleanup ******************* */
/*----------------------------------------------------------------------------*/
//int init_module(const char *name, struct module *image)
static int __init parm_init_module(void)
{
    printk(KERN_INFO "%s. The process is \"%s\" (pid %i)\n",
	   MODULE_NAME, current->comm, current->pid);

    // create a file in the /proc/ file system with this module's name
    // allow anybody to write and read from it
    Our_Proc_File = create_proc_entry(MODULE_NAME, S_IWUGO|S_IRUGO, NULL);

    if (Our_Proc_File == NULL){
      printk(KERN_INFO "Error: Could not initialize /proc/%s\n",MODULE_NAME);
      return -ENOMEM;
    }

    Our_Proc_File->owner = THIS_MODULE;
    Our_Proc_File->proc_iops = &Inode_Ops_4_Our_Proc_File;
    Our_Proc_File->proc_fops = &File_Ops_4_Our_Proc_File;
    Our_Proc_File->mode = S_IFREG | S_IRUGO | S_IWUSR;
    Our_Proc_File->uid = 0;
    Our_Proc_File->gid = 0;
    Our_Proc_File->size = MESSAGE_LENGTH;

    loc_str = NULL;
    loc_len = 0;
    init_MUTEX(&pm_sem);

    return 0;
}

static void __exit parm_cleanup_module(void)
{
  printk("<1>%s module Exiting.\n", MODULE_NAME);
  if (loc_str != NULL){
    kfree(loc_str);
    loc_str = NULL;
    loc_len = 0;
  }

  remove_proc_entry(MODULE_NAME, &proc_root);
}

/***********************************************************************************/
module_init(parm_init_module);
module_exit(parm_cleanup_module);

/***********************************************************************************/
/*  macros
 */
MODULE_LICENSE("GPL");                /* et rid of taint message by declaring code as GPL. */
MODULE_AUTHOR("Ilya Katsnelson");

/*  Or with defines, like this:
 */
MODULE_DESCRIPTION("Simple module designed to facilitate parsing the maps files "
		   "in Linux and do namy other things."); // What does this module do?

/***********************************************************************************/
