// Emacs type: -*- C++ -*-
/*
 * Header file for BaseMapsEntry class
 *
 * This class abstracts the base operations with the entries in the maps files
 *
 *                         vm_offset      vm_inode
 * vm_start vm_end  vm_flags       vm_dev         size        filename
 * 00008000-0000b000 r-xp 00000000 00:08 02581564 00003000    /usr/local/bin/testagent
 *
 *
 * Flags:
 * r-xp  -> code pages
 * rw-p  -> data pages
 * rwxp  -> heap (if below 0xb0......) or stack (if at or above 0xb0......)
 * ---p  -> pages for some unknown use
 * rw-s  -> shared memory pages, marked read-write
 * r--s  -> shared memory pages, marked read-only
 * r--p  -> private memory pages, marked read-only
 *
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * BaseMapsEntry.h,v 1.2 2007/05/23 22:00:07 w30205 Exp
 * -----------------------------------------------------------------------------
 */

#ifndef __BASE_MAPS_ENTRY_H__
#define __BASE_MAPS_ENTRY_H__

#include <iosfwd>
#include <string>

using namespace std;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class BaseMapsEntry
{
 protected:
  string _filename;
  string _flags;
  unsigned short int _deviceMajor;
  unsigned short int _deviceMinor;
  unsigned long _fileInode;
  unsigned long _vm_start;
  unsigned long _vm_end;
  unsigned long _offset;

 public:

  // C'tors & D'tor
  BaseMapsEntry();
  BaseMapsEntry(const string &name);
  BaseMapsEntry(const BaseMapsEntry &entry);

  virtual ~BaseMapsEntry();

 public:
  // Accessor methods
  inline string&  getName(void) const { return (string &)(_filename); };
  inline unsigned short int getDeviceMajorNumber(void) const { return _deviceMajor; };
  inline unsigned short int getDeviceMinorNumber(void) const { return _deviceMinor; };
  inline unsigned long getFileInode(void) const { return _fileInode; };
  inline unsigned long get_offset(void) const { return _offset; };
  inline unsigned long get_vm_start(void) const { return _vm_start; };
  inline unsigned long get_vm_end(void) const { return _vm_end; };
  inline unsigned long get_vm_size(void) const { return (_vm_end - _vm_start); };
  inline string&  get_flags(void) const { return (string &)(_flags); };
  inline const char*  get_flags_c(void) const { return (_flags.c_str()); };

  inline void setName(const string &name) { _filename = name; };
  inline void setDeviceMajor(const unsigned int majorNumber) { _deviceMajor = majorNumber; };
  inline void setDeviceMinor(const unsigned int minorNumber) { _deviceMinor = minorNumber; };
  inline void setInode(const unsigned long inode) { _fileInode = inode; };

  void reset(void);

  int WorkLine(istream& in);
};

#endif // __BASE_MAPS_ENTRY_H__

//==============================================================================
