// Emacs type:  -*- C++ -*-
/*
 * Implementation file for BaseMapsEntry class
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * BaseMapsEntry.cc,v 1.2 2007/05/23 22:00:07 w30205 Exp
 * -----------------------------------------------------------------------------
 */

#include <iostream>
#include <iomanip>
#include <string>

#include <cctype>
#include <cstring>
#include <assert.h>

#include "main.h"
#include "BaseMapsEntry.h"

using namespace std;

//==============================================================================
//------------------------------------------------------------------------------
// BaseMapsEntry - base MapsEntry class. Just to support the basic reading of maps entry.
//------------------------------------------------------------------------------

// Constructors & destructor

BaseMapsEntry::BaseMapsEntry()
{
  reset();
}

BaseMapsEntry::BaseMapsEntry(const std::string &name)
{
  reset();
  setName(name);
}

BaseMapsEntry::BaseMapsEntry(const BaseMapsEntry& entry)
  : _filename(entry._filename),
    _flags(entry._flags),
    _deviceMajor(entry._deviceMajor),
    _deviceMinor(entry._deviceMinor),
    _fileInode(entry._fileInode),
    _vm_start(entry._vm_start),
    _vm_end(entry._vm_end),
    _offset(entry._offset)
{
  // No implementation
}

BaseMapsEntry::~BaseMapsEntry()
{
  // No implementation
}

void
BaseMapsEntry::reset(void)
{
  _filename.clear();
  _flags.clear();

  _deviceMajor = 0;
  _deviceMinor = 0;
  _fileInode = 0;

  _vm_start = 0;
  _vm_end = 0;
  _offset = 0;
}

//------------------------------------------------------------------------------
// Process one line in the passed maps file. 
//  in      - input data stream
//------------------------------------------------------------------------------
int
BaseMapsEntry::WorkLine(istream& in)
{
  if (in.eof()) {
    return -1;
  }

  //        address           perms offset  dev   inode      pathname
  //        08048000-08056000 r-xp 00000000 03:0c 64593      /usr/sbin/gpm

  char c = 0;

  // First check to see if this is an empty line, or all whitespace.
  c = in.peek();
  if (in.eof() || c == (char)-1) {
    in.clear(ios::eofbit);
    return -1;
  }

  // Get the unchanged entry attributes
  // We want to ignore whitespace, and the first numbers are in hex.
  // Only inode is in decimal.
  //
  // From the system information we know weather RSS is installed or not
  // as a result, just scan the whole string

  in >> skipws >> hex 
     >> _vm_start >> c >> _vm_end >> _flags >> _offset >> _deviceMajor >> c >> _deviceMinor
     >> dec >> _fileInode;

  // ugly way to skip some of the whitespaces
  in.unsetf(ios_base::skipws);
  //  in >> c;
  do {
    in >> c;
  } while ((isspace(c)) && (c != '\n'));

  _filename.clear();
  if (c == '\n'){
    // ... it's just an empty space
  } else {
    in.putback(c);
    // the rest of the line contain the information about the module (or not)
    // we have to keep reading untill \n
    do{
      in >> c;
      if(c != '\n')
	_filename += c;
    }while(c != '\n');
  }

  c = in.peek();
  if (in.eof() || c == (char)-1) {
    in.clear(ios::eofbit);
  }

  in.setf(ios_base::skipws);

  return 0;
}

//==============================================================================
