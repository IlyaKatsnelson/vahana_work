// Emacs type: -*- C++ -*-
/*
 * Header file for the MemMapInfo class
 *
 * This class abstracts data manipulation from the memmap file
 * 
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: MemMapInfo.h,v 1.5 2007/03/09 19:32:14 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#ifndef __MEM_MAP_INFO_H__
#define __MEM_MAP_INFO_H__

#include <iosfwd>
#include <string>
#include <vector>

using namespace std;

//------------------------------------------------------------------------------
class MemMapInfo
{
 public:
  typedef vector<unsigned long > _RSS_vector_type;

  typedef enum _map_error_type
    {
      NO_ERROR = 0,
      ERR_NO_MEMMAP = -1,
      ERR_MEMMAP_FILE_IO = -2,
      ERR_MEMMAP_FILE_PARSE = -3,
      AllErrors
    };

 protected:
  pid_t _pid;
  _RSS_vector_type RSSVector;
  long lStatus;
  
  string _errorMsg;

 public:
  // C'tors & D'tor
  MemMapInfo();
  MemMapInfo(pid_t pid);
  MemMapInfo(const MemMapInfo &entry);

  ~MemMapInfo();

  // Properties Get/Set methods
  inline const pid_t getPID() const { return _pid; };

  // Accessor methods
  inline long getMapNumElem(void) const { return RSSVector.size(); };
  inline long getStatus(void) const { return lStatus; };

  unsigned long getRSS_index(int index);
  int getRSS_vector_copy(vector<unsigned long > &v);

  // Public working methods
  int RSSPopulate(int pid);
  int MapPopulate(int pid);
  int Populate(int pid);

 protected:
  int WorkMapLineRSS(std::istream& in, unsigned long *RSSSize);
  int WorkMapLineArr(std::istream& in, unsigned long **);
};

#endif // __MEM_MAP_INFO_H__

//==============================================================================

