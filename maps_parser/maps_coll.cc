// Emacs type: -*- C++ -*-
/*
 * Driver program for maps_coll
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: maps_coll.cc,v 1.10 2007/04/04 02:05:14 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <unistd.h>
#include <fcntl.h>

#include <dirent.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>

#include "ParserModule.h"
#include "SystemInfo.h"

extern int errno;

//------------------------------------------------------------------------------
// Global flags
//------------------------------------------------------------------------------
#define MAPC_STORE_MEMINFO    (1<<0)   // store meminfo
#define MAPC_STORE_SLABINFO   (1<<1)   // store slabinfo
#define MAPC_STORE_CPUINFO    (1<<2)   // store cpuinfo
#define MAPC_STORE_MEMMAP     (1<<3)   // store each memmap
#define MAPC_STORE_FLAGMAP    (1<<4)   // store each flagmap
#define MAPC_STORE_MAPS       (1<<5)   // store each memps
#define MAPC_STORE_STAT       (1<<6)   // store each stat
#define MAPC_STORE_CMDLINE    (1<<7)   // store each cmdline
#define MAPC_STORE_PS_EF      (1<<8)   // store 'ps -ef'
#define MAPC_STORE_NLWP       (1<<9)   // store only the main processes
#define MAPC_STORE_RSSMAP     (1<<10)  // store each RSSmap
#define MAPC_STORE_FD_LIST    (1<<11)  // store the list of file descriptors opened
#define MAPC_STORE_STACK_MAP  (1<<12)  // store the stack segment virtual address map

#define USEC                1000000

#define KERNEL_MOD_NAME           "/proc/parser_module"
#define TSYSINFO_FILE_NAME        "tsysinfo.txt"

#define MAP_THRSHLD (0.98)
#define BUF_SIZE_THRSHLD (PAGE_SIZE*16)

static long global_debug = 0;
long interval = 0;

static char *work_buf = NULL;
static unsigned int buf_size = PAGE_SIZE*1;

const char *map_name[] = {"mem", "flag", "rss", "stack"};

enum map_type {
  MAP_TYPE_MEM = 0,
  MAP_TYPE_FLAG,
  MAP_TYPE_RSS,
  MAP_TYPE_STACK,
  MAP_TYPE_LAST
};

//------------------------------------------------------------------------------
// Print the help information.
//------------------------------------------------------------------------------
static void
usage(const char *progname)
{
  printf("v.1.4 [%s %s]\n", __DATE__, __TIME__);
  printf("Usage: %s [ -d ] [ -mbceMFRasSwl ] [ -p pid ] [ interval {sec} [ count ]]\n\n", progname);
  printf("OPTIONS:\n"
	 "\t-d  Enable debugging mode. It will result in output of some extra debugging information.\n"
	 "\t-m  store the meminfo file.\n"
	 "\t-b  store the slabinfo file.\n"
	 "\t-c  store the cpuinfo file.\n"
	 "\t-e  store the output of the 'ps -ef' command.\n"
	 "\t-M  store the memmap file for each running process.\n"
	 "\t-F  store the flagmap file for each running process.\n"
	 "\t-R  store the RSSmap file for each running process.\n"
	 "\t-a  store the maps file for each running process.\n"
	 "\t-s  store the stat file for each running process.\n"
	 "\t-S  store the stack segment virtual map for each running process.\n"
	 "\t-w  store the files only for each process and not for its LWPs.\n"
	 "\t-l  store the cmdline file for each running process.\n"
	 "\t-p  store the maps and stat stutistics only for the process specified by pid.\n");
}

//------------------------------------------------------------------------------
// SIGALRM signal handler
//------------------------------------------------------------------------------
void
alarm_handler(int sig)
{
   signal(SIGALRM, alarm_handler);
   alarm(interval);
}

//------------------------------------------------------------------------------
// store the maps file for a process (for debugging purposes).
// The storage is in the current running directory;
// no errors are reported
//------------------------------------------------------------------------------
static void
storeMaps(int pid)
{
  sprintf(work_buf, "cp -f /proc/%d/maps ./maps_%d", pid, pid);
  system(work_buf);
}

static void
storeStatFile(char *name, int cnt)
{
  time_t timep = time(NULL);
  struct tm *ltm = localtime(&timep);
  sprintf(work_buf, "cp -f /proc/%s ./%s_%d-%d-%d_%02dh%02dm%02ds_%d",
	  name, name, ltm->tm_mon, ltm->tm_mday, 1900+ltm->tm_year,
          ltm->tm_hour, ltm->tm_min, ltm->tm_sec, cnt);
  system(work_buf);
}

//------------------------------------------------------------------------------
// make the main directory we will be working with
//------------------------------------------------------------------------------
static int
mkRootDir(char *root_dir, int len)
{
  char dir_name[128];
  time_t timep = time(NULL);
  struct tm *ltm = localtime(&timep);

  // create the main directory
  sprintf(dir_name,"./coll_store_%d-%d-%d_%02dh%02dm%02ds",
	  ltm->tm_mon+1, ltm->tm_mday, 1900+ltm->tm_year,
          ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
  strncpy(root_dir, dir_name, len-1);
  root_dir[len-1] =  '\0';

  if (mkdir(dir_name, 0777) == -1){
    fprintf(stderr, "Can't create storage directory %s: %s\n",
	    dir_name, strerror(errno));
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
// make the working directory for the current iteration
//------------------------------------------------------------------------------
static int
mkWkDir(const long count, const char *root_dir, char *wk_dir, const int len)
{
  // create the directory
  sprintf(work_buf,"%s/%ld", root_dir, count);
  if (mkdir(work_buf, 0777) == -1){
    fprintf(stderr, "Can't create storage directory %s: %s\n",
	    work_buf, strerror(errno));
    return -1;
  }

  strncpy(wk_dir, work_buf, len-1);
  wk_dir[len-1] =  '\0';

  return 0;
}

//------------------------------------------------------------------------------
// file copy function using the FILE descriptors passed
// Return 0 if no error.
//------------------------------------------------------------------------------
static int
copy_file_raw(FILE *src, FILE *dest)
{
  const size_t size = sizeof(char);
  size_t nmemb;

  // insanity check
  if (src == NULL || dest == NULL)
    return -1;

  // read one work buffer at a time
  do {
    nmemb = fread(work_buf, size, buf_size, src);
    fwrite(work_buf, size, nmemb, dest);
  } while (nmemb == buf_size);

  if (ferror(src) != 0 || ferror(dest) != 0){
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
// file copy function using the two names
// Return 0 if no error.
//------------------------------------------------------------------------------
static int
copy_file(const char *src, const char *dest)
{
  FILE *fsrc, *fdest;
  int status = 0;

  fsrc = fopen(src, "r");
  if (fsrc == NULL){
    fprintf(stderr,"cannot open %s. %d:%s\n", src, errno, strerror(errno));
    return -1;
  }

  fdest = fopen(dest, "w+");
  if (fdest == NULL){
    fprintf(stderr,"cannot open %s. %d:%s\n", dest, errno, strerror(errno));
    return -1;
  }

  // copy the files
  if (copy_file_raw(fsrc, fdest) < 0){
    fprintf(stderr,"error occured during copy %s to %s. %d:%s\n",
	    src, dest, errno, strerror(errno));
    status = -1;
  }

  fclose(fsrc);
  fclose(fdest);

  return status;
}

//------------------------------------------------------------------------------
// Store the information about the host system on which the data was
// collected.
//------------------------------------------------------------------------------
static int
store_sysinfo(const char *root_dir, const int len, const int pid,
	      const long interval, const long flag, const long count)
{
  // global system information object
  FILE *fsysinfo;
  int status = 0;

  assert(work_buf != NULL);
  sprintf(work_buf,"%s/%s", root_dir, TSYSINFO_FILE_NAME);

  fsysinfo = fopen(work_buf, "w");
  if (fsysinfo == NULL){
    fprintf(stderr,"cannot open %s. %d:%s\n", work_buf, errno, strerror(errno));
    return -1;
  }

  // print the general information
  struct utsname _k_info;
  struct tm *_loc_time;

  if (uname(&(_k_info)) == -1){
    fprintf(stderr,"error returned by uname(). %d:%s\n", errno, strerror(errno));
    return -1;
  }

  fprintf(fsysinfo, " System Name:%s\n Node Name:%s\n",
          _k_info.sysname, _k_info.nodename);
  fprintf(fsysinfo, " Release:%s\n Version:%s\n Machine:%s\n",
          _k_info.release, _k_info.version, _k_info.machine);
  fprintf(fsysinfo, " Memory Page Size:%lu\n", PAGE_SIZE);

  // get the time of the run
  time_t timep = time(NULL);
  _loc_time = localtime(&timep);
  fprintf(fsysinfo, " Test time: %02d-%02d-%02d %02d:%02d:%02d\n",
          _loc_time->tm_mon+1, _loc_time->tm_mday, (1900+_loc_time->tm_year),
          _loc_time->tm_hour, _loc_time->tm_min, _loc_time->tm_sec);

  // print the collector-specific information
  fprintf(fsysinfo, " Flags:%ld\n Interval:%ld\n Count:%ld\n PID:%d\n",
          flag, interval, count, pid);

  fclose(fsysinfo);

  return status;
}

//------------------------------------------------------------------------------
// store the buffer in the file specified by the name. If a file with the same
// name exists, it will be truncated.
// Return 0 if no error.
//------------------------------------------------------------------------------
static int
store_buf_file(const char *dest, const char *buf, const int len)
{
  FILE *fdest;
  int status = 0;

  fdest = fopen(dest, "w+");
  if (fdest == NULL){
    fprintf(stderr,"cannot open %s. %d:%s\n", dest, errno, strerror(errno));
    return -1;
  }

  status = fwrite(buf, sizeof(char), len, fdest);
  if(status < len){
    fprintf(stderr,"error occured buffered storage %s. %d:%s\n",
	    dest, errno, strerror(errno));
    status = -1;
  }

  fclose(fdest);

  return status;
}

//------------------------------------------------------------------------------
// collector functions
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static int
store_meminfo(const char *wk_dir, const int len)
{
  const char *const meminfo_src = "/proc/meminfo";
  char meminfo_dest[len+10];

  sprintf(meminfo_dest, "%s/meminfo", wk_dir);

  return copy_file(meminfo_src, meminfo_dest);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static int
store_slabinfo(const char *wk_dir, const int len)
{
  const char *const slabinfo_src = "/proc/slabinfo";
  char slabinfo_dest[len+10];

  sprintf(slabinfo_dest, "%s/slabinfo", wk_dir);

  return copy_file(slabinfo_src, slabinfo_dest);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static int
store_cpuinfo(const char *wk_dir, const int len)
{
  const char *const cpuinfo_src = "/proc/cpuinfo";
  char cpuinfo_dest[len+10];

  sprintf(cpuinfo_dest, "%s/cpuinfo", wk_dir);

  return copy_file(cpuinfo_src, cpuinfo_dest);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static int
store_psinfo(const char *wk_dir, const int len)
{
  char psinfo_cmd[len+25];

  sprintf(work_buf, "ps -ef > %s/psinfo", wk_dir);

  return system(psinfo_cmd);
}

//------------------------------------------------------------------------------
// Helper function to read the maps
//------------------------------------------------------------------------------
inline int
read_buffer_map(char *buf, const int len, const int pid,
		const unsigned int map_type, ParserModule *pmodule)
{
  int status = -1;

  switch (map_type){
  case MAP_TYPE_MEM:
    status = pmodule->readMemMap(pid, buf, len);
    break;
  case MAP_TYPE_FLAG:
    status = pmodule->readFlagMap(pid, buf, len);
    break;
  case MAP_TYPE_RSS:
    status = pmodule->readRSSMap(pid, buf, len);
    break;
  case MAP_TYPE_STACK:
    status = pmodule->readStackMap(pid, buf, len);
    break;
  default:
    status = -1;
  }
  return status;
}

//------------------------------------------------------------------------------
// Combined map reading function
//------------------------------------------------------------------------------
static int
store_buffer_map(const char *wk_dir, const int len, const int pid,
		 const unsigned int map_type, ParserModule *pmodule)
{
  int status = -1;
  int flag = 0;
  int init_flag = 0;
  char flag_dest[len+12];

  // insanity check
  if (pmodule == NULL || (map_type >= MAP_TYPE_LAST))
    return status;

  if (work_buf == NULL){
    buf_size = PAGE_SIZE;
    work_buf = (char *)malloc(buf_size);
    if (work_buf == NULL){
      fprintf(stderr,"pid=%d.cannot allocate data buffer.\n",pid);
      return status;
    }
  }
  // first read only one page
  status = read_buffer_map(work_buf, PAGE_SIZE, pid, map_type, pmodule);

  if (pmodule->iserror()){
    fprintf(stderr,"Error reading %s map: %d.\n", map_name[map_type], status);
    pmodule->clearerror();
    return status;
  }

  if (global_debug) fprintf(stderr,"%smap. pid=%d status=%d work_buf_s=%d.\n",
			    map_name[map_type], pid, status, buf_size);

  // increase the buffer, if needed, and read again
  // only increase the buffer initially if it is one page
  if (pmodule->iseof() == false && buf_size < BUF_SIZE_THRSHLD){

    while (flag == 0){
      if (init_flag == 1 || buf_size == PAGE_SIZE){
	buf_size *= 2;
	work_buf = (char *)realloc(work_buf,buf_size);
	if (work_buf == NULL){
          fprintf(stderr,"pid=%d. cannot reallocate data buffer (size=%dB).\n",pid,buf_size);
	  buf_size = 0;
	  return status;
	}
      }
      init_flag = 1;

      //printf ("flag. pid=%d|s=%d\n",pid,buf_size);
      status = read_buffer_map(work_buf, buf_size, pid, map_type, pmodule);
      if (pmodule->iserror()){
      	fprintf(stderr,"Error reading %s map: %d.\n", map_name[map_type], status);
	return status;
      }

      // check the end status
      if (work_buf[status-1] == (char)EOF || buf_size >= BUF_SIZE_THRSHLD){
	flag = 1;
	break;
      }
    }
  }

  sprintf(flag_dest, "%s/%smap_%d", wk_dir, map_name[map_type], pid);
  store_buf_file(flag_dest, work_buf, status-1);

  pmodule->clearerror();
  return 0;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static int
store_maps(const char *wk_dir, const int len, const int pid)
{
  char maps_dest[len+12];

  sprintf(work_buf, "/proc/%d/maps", pid);
  sprintf(maps_dest, "%s/maps_%d", wk_dir, pid);

  return copy_file(work_buf, maps_dest);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static int
store_stat(const char *wk_dir, const int len, const int pid)
{
  char stat_dest[len+12];

  sprintf(work_buf, "/proc/%d/stat", pid);
  sprintf(stat_dest, "%s/stat_%d", wk_dir, pid);

  return copy_file(work_buf, stat_dest);
}

//------------------------------------------------------------------------------
// Same function to store the memmap files, but on the system
// that already has support for them
//------------------------------------------------------------------------------
static int
store_memmap_sup(const char *wk_dir, const int len, const int pid)
{
  char stat_dest[len+12];

  sprintf(work_buf, "/proc/%d/memmap", pid);
  sprintf(stat_dest, "%s/memmap_%d", wk_dir, pid);

  return copy_file(work_buf, stat_dest);
}

//------------------------------------------------------------------------------
// main is main
//------------------------------------------------------------------------------
int
main(int argc, char *argv[])
{
  const int dir_name_len = 128;
  long flag = 0;
  int opt;
  int pid = -1;
  long count = 0;

  /* Parse any options */
  if (argc == 1){
    usage(argv[0]);
    return 0;
  }

  while ((opt=getopt(argc, argv, "dmlbceMFRasSwhvp:")) != -1) {
    switch(opt) {
    case 'd':
      global_debug = 1;
      break;
    case 'm':
      flag |= MAPC_STORE_MEMINFO;
      break;
    case 'b':
      flag |= MAPC_STORE_SLABINFO;
      break;
    case 'c':
      flag |= MAPC_STORE_CPUINFO;
      break;
    case 'M':
      flag |= MAPC_STORE_MEMMAP;
      break;
    case 'F':
      flag |= MAPC_STORE_FLAGMAP;
      break;
    case 'R':
      flag |= MAPC_STORE_RSSMAP;
      break;
    case 'a':
      flag |= MAPC_STORE_MAPS;
      break;
    case 's':
      flag |= MAPC_STORE_STAT;
      break;
    case 'S':
      flag |= MAPC_STORE_STACK_MAP;
      break;
    case 'l':
      flag |= MAPC_STORE_CMDLINE;
      break;
    case 'e':
      flag |= MAPC_STORE_PS_EF;
      break;
    case 'w':
      flag |= MAPC_STORE_NLWP;
      break;
    case 'p':
      pid = atoi(optarg);
      break;
    case 'v':
    case 'h':
      usage(argv[0]);
      return 0;
      break;
    default:
      usage(argv[0]);
      return 0;
    }
  }

  // check the tail of the input after the options
  // if there was no tail in the first place - just do one sample
  if (optind < argc){
    interval = atol(argv[optind]);
    if (optind+1 < argc)
      count = atol(argv[optind+1]);
  }

  // simple check
  if (interval < 0 || count < 0){
    fprintf(stderr, "What were you thinking sending negative values?\n");
    fprintf(stderr, "\tinterval = %ld|count = %ld\n", interval, count);
    usage(argv[0]);
    return 0;
  }
  if (flag == 0){
    fprintf(stderr, "No data to collect.\n");
    usage(argv[0]);
    return 0;
  }

  if (pid != -1){
    if (kill(pid,0) == -1 && errno == ESRCH){
      fprintf(stderr, "The process with PID=%d does not exist.\n", pid);
      return 0;
    }
  }

  // just for fun we check how long it took to run this
  struct timeval tv1, tv2;
  struct timezone tz;
  gettimeofday(&tv1, &tz);

  // --------------------------------------------------
  char root_dir[dir_name_len];
  char wk_dir[dir_name_len];
  int i = 1;
  ParserModule *pmodule = NULL;
  DIR *procDir = NULL;

  if (mkRootDir(root_dir, dir_name_len) < 0){
    fprintf(stderr, "Error creating root directory: %s\n", root_dir);
    return -1;
  }

  // create parser module if needed
#ifdef HAS_MEMMAP_SUPPORT
  if ((flag & MAPC_STORE_FLAGMAP) != 0 ||
      (flag & MAPC_STORE_NLWP) != 0 || (flag & MAPC_STORE_STACK_MAP) != 0){
#else
  if ((flag & MAPC_STORE_MEMMAP) != 0 || (flag & MAPC_STORE_FLAGMAP) != 0 ||
      (flag & MAPC_STORE_NLWP) != 0 || (flag & MAPC_STORE_RSSMAP) != 0 ||
      (flag & MAPC_STORE_STACK_MAP) != 0){
#endif
    pmodule = new ParserModule(true);
    if (pmodule->getStatus() < 0){
      fprintf(stderr,"Error loading kernel module.\n");
      fprintf(stderr,"The statistics that requires its support will not be collected.\n");
      delete pmodule;
      pmodule = NULL;
    }
  }

  // open the /proc directory.
  // we only have to do it once per run if needed at all
  if ((flag & MAPC_STORE_MEMMAP) != 0 || (flag & MAPC_STORE_FLAGMAP) != 0 ||
       (flag & MAPC_STORE_MAPS) != 0 || (flag & MAPC_STORE_STAT) != 0 ||
      (flag & MAPC_STORE_RSSMAP) != 0 || (flag & MAPC_STORE_STACK_MAP) != 0){
    if ((procDir = opendir("/proc")) == NULL){
      fprintf(stderr,"Can't open /proc directory: %s\n", strerror(errno));
    }
  }

  // Create the main working buffer that will be used everywhere
  if ((work_buf = (char *)malloc(buf_size)) == NULL){
    fprintf(stderr,"Not enough memory to allocate the program buffer.\n");
    return -1;
  }

  // store the host system info
  store_sysinfo(root_dir, dir_name_len, pid, interval, flag, count);

  // main loop
  do {
    if (mkWkDir(i++, root_dir, wk_dir, dir_name_len) < 0)
      break;

    // Fire up the alarm
    alarm_handler(0);

    // process flags
    if ((flag & MAPC_STORE_MEMINFO) != 0)
      store_meminfo(wk_dir, dir_name_len);
    if ((flag & MAPC_STORE_SLABINFO) != 0)
      store_slabinfo(wk_dir, dir_name_len);
    if ((flag & MAPC_STORE_CPUINFO) != 0)
      store_cpuinfo(wk_dir, dir_name_len);
    if ((flag & MAPC_STORE_PS_EF) != 0)
      store_psinfo(wk_dir, dir_name_len);

    // do the PID dependent statistics
    // if procDir == NUL then we don't need that statistics
    if (pid != -1 && procDir != NULL){

      // simple check to see if the process in question is still there
      if (kill(pid,0) == -1 && errno == ESRCH){
	fprintf(stderr, "%d.The process with PID=%d is not running.\n", count, pid);
	break;
      }

#ifdef HAS_MEMMAP_SUPPORT
      if ((flag & MAPC_STORE_MEMMAP) != 0){
	store_memmap_sup(wk_dir, dir_name_len, pid);
      }
#endif

      // these two require parser module
      if (pmodule != NULL){
#ifndef HAS_MEMMAP_SUPPORT
	if ((flag & MAPC_STORE_MEMMAP) != 0)
	  store_buffer_map(wk_dir, dir_name_len, pid, MAP_TYPE_MEM, pmodule);
#endif
	if ((flag & MAPC_STORE_FLAGMAP) != 0)
	  store_buffer_map(wk_dir, dir_name_len, pid, MAP_TYPE_FLAG, pmodule);
	if ((flag & MAPC_STORE_RSSMAP) != 0)
	  store_buffer_map(wk_dir, dir_name_len, pid, MAP_TYPE_RSS, pmodule);
	if ((flag & MAPC_STORE_STACK_MAP) != 0)
	  store_buffer_map(wk_dir, dir_name_len, pid, MAP_TYPE_STACK, pmodule);
      }

      if ((flag & MAPC_STORE_MAPS) != 0)
	store_maps(wk_dir, dir_name_len, pid);
      if ((flag & MAPC_STORE_STAT) != 0)
	store_stat(wk_dir, dir_name_len, pid);

    }else if (procDir != NULL){
      // loop through all the current processes in the system
      // this set can change during the stat collection
      struct dirent *entry;
      int pid_l;
      int status;

      // read the data for all the processes in the /proc directory
      // skip the kernel threads
      while ((entry = readdir(procDir)) != NULL) {
	pid_l = atoi(entry->d_name);
	if (pid_l <= 0){
	  continue;
	}
	if (getpgid(pid_l) == 1)
	  continue;

	// if the user set the option, maybe we don't even have to read anything
	if ((flag & MAPC_STORE_NLWP) != 0 && (pmodule != NULL)){
	  if (pmodule->isLWP(pid_l) == 1)
	    continue;
	}

	status = 0;
	if ((flag & MAPC_STORE_MAPS) != 0)
	  status |= store_maps(wk_dir, dir_name_len, pid_l);
	if ((flag & MAPC_STORE_STAT) != 0)
	  status |= store_stat(wk_dir, dir_name_len, pid_l);

	// if the status isn't 0 then 'stat' or 'maps' wasn't stored
	// so maps shouldn't be stored too
	if (status != 0)
	  continue;

#ifdef HAS_MEMMAP_SUPPORT
	if ((flag & MAPC_STORE_MEMMAP) != 0){
	  store_memmap_sup(wk_dir, dir_name_len, pid_l);
	}
#endif

	// these two require parser module
	if (pmodule != NULL){
#ifndef HAS_MEMMAP_SUPPORT
	  if ((flag & MAPC_STORE_MEMMAP) != 0)
	    store_buffer_map(wk_dir, dir_name_len, pid_l, MAP_TYPE_MEM, pmodule);
#endif
	  if ((flag & MAPC_STORE_FLAGMAP) != 0)
	    store_buffer_map(wk_dir, dir_name_len, pid_l, MAP_TYPE_FLAG, pmodule);
	  if ((flag & MAPC_STORE_RSSMAP) != 0)
	    store_buffer_map(wk_dir, dir_name_len, pid_l, MAP_TYPE_RSS, pmodule);
	  if ((flag & MAPC_STORE_STACK_MAP) != 0)
	    store_buffer_map(wk_dir, dir_name_len, pid_l, MAP_TYPE_STACK, pmodule);
	}
      }
      rewinddir(procDir);
    }

    if (--count > 0)
      pause();
  } while(count > 0);

  if (pmodule != NULL){
    delete pmodule;
  }

  // just for fun we check how long it took to run this
  gettimeofday(&tv2, &tz);
  printf(" Running time: ");
  if (tv2.tv_sec == tv1.tv_sec)
    printf("0.%06ld", (tv2.tv_usec-tv1.tv_usec));
  else{
    if ((USEC-tv1.tv_usec)+tv2.tv_usec < USEC){
      printf("%ld.%06ld", (tv2.tv_sec-tv1.tv_sec - 1), ((USEC-tv1.tv_usec)+tv2.tv_usec));
    }else{
      printf("%ld.%06ld", (tv2.tv_sec-tv1.tv_sec), ((USEC-tv1.tv_usec)+tv2.tv_usec - USEC));
    }
  }
  printf(" sec.\n");

  if (work_buf != NULL)
    free(work_buf);

  if (global_debug == 1){
    char msg[128];
    // debugging
    sprintf(msg,"dmesg > dmesg_%06ld.txt", (long)tv2.tv_sec);
    system(msg);
    system("sync");
  }
  return 0;
}

//------------------------------------------------------------------------------

