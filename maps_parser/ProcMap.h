// Emacs type: -*- C++ -*-
/*
 * Header file for ProcMaps class
 *
 * This class abstracts the memory information for an entire process
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: ProcMap.h,v 1.8 2007/05/23 22:00:07 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#ifndef __PROC_MAP_H__
#define __PROC_MAP_H__

#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <set>
#include <iosfwd>

#include "Statm.h"
#include "Stat.h"
#include "MapsEntry.h"
#include "ParserModule.h"

using namespace std;

typedef map<std::string, MapsEntry> Entries;
typedef set<int> LWPSet;

class ProcMap {

 protected:

  pid_t _pid;
  string _cmdLine;
  string _errorMsg;

  unsigned long _start_code, _end_code, _start_data, _end_data;
  unsigned long _start_brk, _brk, _start_stack;
  unsigned long _arg_start, _arg_end, _env_start, _env_end;
  long _rss, _total_vm, _locked_vm;

  unsigned int _totalSize;

  LWPSet _LWPList;
  vector<unsigned long > _StackVector;

  Entries _map;
  MapsEntry _uniqueAccumMap;
  MapsEntry _totAccumMap;
  Statm _statm;
  Stat _stat;
  ParserModule *_parserModule;
     
 public:
  
  // C'tors & D'tor
  ProcMap();
  ProcMap(pid_t pid);
  ProcMap(ParserModule *pmodule);
  ProcMap(const ProcMap &entry);

  ~ProcMap();

  // Properties Get/Set methods
  inline const pid_t getPID() const { return _pid; };
  inline const pid_t getPGRP() const { return _stat.getPGRP(); };
  inline const pid_t getPPID() const { return _stat.getPPID(); };
  inline const char getState() const { return _stat.get_state(); };
  inline const unsigned long get_esp() const { return _stat.get_esp(); };
  inline string& getCmdName() const { return _stat.get_cmd_name(); };
  inline string& getCmdLine() const { return (string &)(_cmdLine); };
  inline string& getErrorMsg() const { return (string &)(_errorMsg); };
  inline const Entries& getMapC() const { return _map; };
  inline Entries* getMap() { return &_map; };
  inline MapsEntry& getUAccumMap() { return _uniqueAccumMap; };
  inline MapsEntry& getTAccumMap() { return _totAccumMap; };
  inline const unsigned int getTotalSize() const { return _totalSize; };
  inline const LWPSet & getLWPList() const { return _LWPList; };
  string getLWPListStr(void);
  inline const Statm & getStatm() const { return _statm; };
  inline const ParserModule * getParserModule() const { return _parserModule; };

  inline pid_t setPID(pid_t pid) { _pid=pid;return _pid; };

  int addLWP(int lwp);

  // Working methods
  int HeadPopulate(int pid);
  int MapsPopulate(int pid, const bool rss_ovr=false);
  int Populate(int pid, const bool rss_ovr=false);
  int CollectMemory(void);
  int table_print(ostream& out, int flag);
  int cmpMap(const ProcMap &second);
  int cmpTree(const ProcMap &second);
  void reset(void);
  int StoreDebug(void);

  MapsEntry::map_type figure_segment(const BaseMapsEntry &a, 
				     const MapsEntry::map_type ptype = MapsEntry::Unknown);
  int set_map_name(BaseMapsEntry &a);
  inline string & get_gen_name(const MapsEntry::map_type type, string &name_str);

 protected:
  int WorkCmdLine(void);
  int WorkStat(void);
  int WorkStatm(void);

  int WorkMemSegment(void);

 public:
  // I/O Stream operators
  friend std::istream& operator>>(std::istream& in, ProcMap& entry);
  friend std::ostream& operator<<(std::ostream& out, const ProcMap& entry);

  int table_printRSS_tot(std::ostream& out);
  int table_print_U_S(std::ostream& out);
  int table_print_U_S_Sw(std::ostream& out);

  // static header printing functions
  static int table_head_printRSS(std::ostream& out);
  static int table_head_print_U_S(std::ostream& out);
  static int table_head_print_U_S_Sw(std::ostream& out);
};


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
class KernProcMap : public ProcMap {
 private:

 public:

  // C'tors & D'tor
  KernProcMap();
  KernProcMap(int pid);
  KernProcMap(ParserModule *pmodule);

  ~KernProcMap();

  // Working methods
  int HeadPopulate(int pid);

  // I/O Stream operators
  friend std::ostream& operator<<(std::ostream& out, KernProcMap& entry);

  int table_print(std::ostream& out);

  // static header printing functions
  static int table_head_print(std::ostream& out);
};

#endif // __PROC_MAP_H__

//==============================================================================
