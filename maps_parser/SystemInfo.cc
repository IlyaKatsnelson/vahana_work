// Emacs type:  -*- C++ -*-
/*
 * Implementation file for SystemInfo class
 *
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: SystemInfo.cc,v 1.6 2006/09/25 22:37:33 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#include "main.h"
#include "SystemInfo.h"
#include "ParserModule.h"

#include <sys/types.h>
#include <sys/utsname.h>
#include <unistd.h>

#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int SystemInfo::_use_cnt = 0;
bool SystemInfo::_RSS_Supported = false;
bool SystemInfo::_memmap_supported = false;
unsigned long SystemInfo::_slabinfo_version = 0;
struct tm * SystemInfo::_loc_time = NULL;
struct utsname SystemInfo::_k_info;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Constructors & destructor

SystemInfo::SystemInfo()
  : lStatus(0), _page_size(sysconf(_SC_PAGESIZE))
{
  this->update();
  _use_cnt++;
}

SystemInfo::SystemInfo(const SystemInfo &entry)
  : lStatus(entry.lStatus)
{
  _use_cnt++;
}

SystemInfo::~SystemInfo()
{
  _use_cnt--;
}

//------------------------------------------------------------------------------
// Reload the host system information
//------------------------------------------------------------------------------
long
SystemInfo::update(void)
{
  // read the maps file of this process
  pid_t this_pid = getpid();

  char this_maps_fname[32];
  FILE *fmaps;
  int scan_cnt;

  sprintf(this_maps_fname,"/proc/%d/maps", this_pid);
  if ((fmaps = fopen(this_maps_fname, "r")) == NULL){
    lStatus = errno;
    return lStatus;
  }

  //                         vm_offset      vm_inode
  // vm_start vm_end  vm_flags       vm_dev         size     rss      rss%    filename
  // 00008000-0000b000 r-xp 00000000 00:08 02581564 00003000 00003000 100%    /usr/local/bin/testagent
  //
  // all we have to do is scan first 12 fields, without even reading,
  // if they were correctly returned - RSS is there.
  unsigned long vm_inode,a,b;
  int c;
  char d;
  scan_cnt = fscanf(fmaps,"%*x%*c%*x %*4s %*x %*x%*c%*x %lu %lx %lx %d%c", &vm_inode,&a,&b,&c,&d);

  if (scan_cnt > 1)
    _RSS_Supported = true;

  fclose(fmaps);

  // check if the memmap files are supported
  sprintf(this_maps_fname,"/proc/%d/memmap", this_pid);
  if ((fmaps = fopen(this_maps_fname, "r")) != NULL){
    _memmap_supported = true;
    fclose(fmaps);
  }

  // get the slabinfo version
  // the version is printed in the first line [slabinfo - version: 1.1]
  if ((fmaps = fopen("/proc/slabinfo", "r")) != NULL){
    scan_cnt = fscanf(fmaps,"%*s %*c %*s%*c %lu%*c%lu",&a,&b);
    _slabinfo_version = (a << 16) + b;
    fclose(fmaps);
  }

  // get the time of the run
  time_t timep = time(NULL);
  _loc_time = localtime(&timep);

  // get the kernel information
  if (uname(&(_k_info)) == -1){
    lStatus = errno;
    return lStatus;
  }

  return lStatus;
}

//------------------------------------------------------------------------------
// Pretty print the host system information
//------------------------------------------------------------------------------
int
SystemInfo::pretty_print(std::ostream& out)
{
  ParserModule kmodule;

  out << "-------------------------------------------------------------\n";

  out << " System Name: " << _k_info.sysname << endl;
  out << " Node Name: " << _k_info.nodename << endl;
  out << " Release: " << _k_info.release << endl;
  out << " Version: " << _k_info.version << endl;
  out << " Machine: " << _k_info.machine << endl;

  out << "-------------------------------------------------------------\n";

  out << " RSS Supported: " << getRSSSupported() << endl;
  out << " MemMap Supported: " << getMemMapSupported() << endl;
  out << " Memory Page Size: " << getPageSize() << endl;
  out << " Slabinfo Version: " << (getSlabInfoVersion() >> 16) << "."
      << (getSlabInfoVersion() & 0xFFFF) << endl;

  out << " Tool Version: " << MAP_VERSION_FULL << endl;
  out << " KModule Version: " << kmodule.getKModuleVer() << endl;

  out << "-------------------------------------------------------------\n";

  char prev = out.fill ('0');
  out << " Test time: ";
  out << setw(2) << _loc_time->tm_mon+1 << "-" << setw(2) << _loc_time->tm_mday
      << "-" << 1900+_loc_time->tm_year << " "
      << setw(2) << _loc_time->tm_hour << ":" << setw(2) << _loc_time->tm_min
      << ":" << setw(2) << _loc_time->tm_sec << endl;
  out.fill(prev);

  out << "-------------------------------------------------------------\n";

  return lStatus;
}

//------------------------------------------------------------------------------
// Pretty print the host system information in light form
//------------------------------------------------------------------------------
int
SystemInfo::pretty_print_lt(std::ostream& out)
{
  out << " System Name: " << _k_info.sysname << endl;
  out << " Node Name: " << _k_info.nodename << endl;
  out << " Release: " << _k_info.release << endl;
  out << " Version: " << _k_info.version << endl;
  out << " Machine: " << _k_info.machine << endl;

  out << " RSS Supported: " << getRSSSupported() << endl;
  out << " MemMap Supported: " << getMemMapSupported() << endl;
  out << " Memory Page Size: " << getPageSize() << endl;
  out << " Slabinfo Version: " << (getSlabInfoVersion() >> 16) << "."
      << (getSlabInfoVersion() & 0xFFFF) << endl;

  char prev = out.fill ('0');
  out << " Test time: ";
  out << setw(2) << _loc_time->tm_mon+1 << "-" << setw(2) << _loc_time->tm_mday
      << "-" << 1900+_loc_time->tm_year << " "
      << setw(2) << _loc_time->tm_hour << ":" << setw(2) << _loc_time->tm_min
      << ":" << setw(2) << _loc_time->tm_sec << endl;
  out.fill(prev);

  return lStatus;
}

//------------------------------------------------------------------------------
// Pretty print the host system information
//------------------------------------------------------------------------------
int
SystemInfo::pretty_print(const char *out_fname)
{
  ofstream fsysinfo(out_fname);

  this->pretty_print(fsysinfo);
  fsysinfo.close();

  return lStatus;
}

//------------------------------------------------------------------------------
// Pretty print the host system information in light form
//------------------------------------------------------------------------------
int
SystemInfo::pretty_print_lt(const char *out_fname)
{
  ofstream fsysinfo(out_fname);

  this->pretty_print_lt(fsysinfo);
  fsysinfo.close();

  return lStatus;
}

//==============================================================================

