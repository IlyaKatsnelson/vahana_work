// Emacs type:  -*- C++ -*-
/*
 * Implementation file for MemMapInfo class
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: MemMapInfo.cc,v 1.6 2006/09/25 22:37:33 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#include "main.h"
#include "MemMapInfo.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

#include <cctype>
#include <cstring>
#include <errno.h>
#include <linux/limits.h>

using namespace std;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Constructors & destructor

MemMapInfo::MemMapInfo()
  : _pid(-1),
    RSSVector(0),
    lStatus(0)
{
}

MemMapInfo::MemMapInfo(pid_t pid)
  : RSSVector(0),
    lStatus(0)
{
  _pid = pid;
}

MemMapInfo::MemMapInfo(const MemMapInfo &entry)
  : RSSVector(entry.RSSVector),
    lStatus(entry.lStatus)
{
}

MemMapInfo::~MemMapInfo()
{
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Populate the RSS array
//------------------------------------------------------------------------------
int MemMapInfo::RSSPopulate(int pid)
{
  if (lStatus < MemMapInfo::NO_ERROR)
    return lStatus;

  // open the map file and read the lines from it
  char filepath[PATH_MAX];

  // Check if the memmap file is there
  sprintf(filepath, "/proc/%d/memmap", pid);

  ifstream mapfile(filepath);
  if (! mapfile) {
    _errorMsg += "Unable to open file [" + string(filepath) + "]: " + string(strerror(errno));
    lStatus = MemMapInfo::ERR_MEMMAP_FILE_IO;
    return lStatus;
  }

  // Populate the RSS Vector for this memory map
  unsigned long RSSSize;
  while (WorkMapLineRSS(mapfile, &RSSSize) == 0){
    RSSVector.push_back(RSSSize);
    RSSSize = 0;

    // leave if the end of file was found
    if (mapfile.eof())
      break;
  }

  if ((mapfile.bad()) || (mapfile.fail())){
    _errorMsg += "Error parsing file [" + string(filepath) + "]";
    lStatus = MemMapInfo::ERR_MEMMAP_FILE_PARSE;
    return lStatus;
  }
  mapfile.close();

  return lStatus;
}

//------------------------------------------------------------------------------
// Process one line in the memmap file.
// Very simple - just count the number of page counters that are not 0
//------------------------------------------------------------------------------
int MemMapInfo::WorkMapLineRSS(std::istream& in, unsigned long *RSSSize)
{
  // insanity check
  if (lStatus < MemMapInfo::NO_ERROR)
    return lStatus;

  // go though one line of data
  // the counters will be either in the form [##] or [ #]. But they all come in twos
  if (in.eof()) {
    return lStatus;
  }

  char c[4];
  int value = 0;
  unsigned long cnt = 0;

  in.unsetf(ios_base::skipws);
  do{

    c[0] = in.peek();
    if (in.eof() || c[0] == '\n') {
      in.read(c,1);
      break;
    }

    in.read(c,2);
    if (in.eof() || in.fail()){
      break;
    }

    // insanity check
    if ((isdigit(c[0]) == 0 && isspace(c[0]) == 0) || (isdigit(c[1]) == 0)){
      // what t'hell? Some unexpected character in the file.
      lStatus = MemMapInfo::ERR_MEMMAP_FILE_PARSE;
#ifdef DEBUG
      cerr << "Parsing error occured: " << c << " PID:" << getPID() << endl;
#endif
      break;
    }

    value = atoi(c);
    if (value != 0) cnt++;

  } while(c[1] != '\n' && c[0] != '\n' && in.eof() == false);
  in.setf(ios_base::skipws);

  *RSSSize = cnt;

  c[0] = in.peek();
  if (in.eof() || c[0] == (char)-1) {
    in.clear(ios::eofbit);
  }

  return lStatus;
}

//------------------------------------------------------------------------------
// Process one line in the memmap file.
// Very simple - just count the number of page counters that are not 0
//------------------------------------------------------------------------------
unsigned long MemMapInfo::getRSS_index(int index)
{
  // insanity check
  if (index < 0 || index >= getMapNumElem()){
    cerr << "Invalid RSS data access. Data will be inconcistent. PID: " << getPID() << endl;
    return 0;
  }
  return RSSVector[index];
}

//------------------------------------------------------------------------------
// Copy the internal vector of RSS data to the supplied vector
//------------------------------------------------------------------------------
int MemMapInfo::getRSS_vector_copy(vector<unsigned long > &v)
{
  v.assign(RSSVector.begin(), RSSVector.end());
  return 0;
}

//==============================================================================

