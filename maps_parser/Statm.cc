// Emacs type:  -*- C++ -*-
/*
 * Implementation file for Statm class
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: Statm.cc,v 1.3 2006/09/25 22:37:33 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#include "main.h"
#include "Statm.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

#include <cctype>
#include <cstring>
#include <linux/limits.h>

using namespace std;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Constructors & destructor

Statm::Statm()
{
  this->reset();
}


Statm::Statm(const Statm &entry)
  : _pid(entry._pid),
    ulTotProgSize(entry.ulTotProgSize),
    ulMemPrtsSize(entry.ulMemPrtsSize),
    ulNumShrPgs(entry.ulNumShrPgs),
    ulNumCodePgs(entry.ulNumCodePgs),
    ulNumD_SPgs(entry.ulNumD_SPgs),
    ulNumLibPgs(entry.ulNumLibPgs),
    ulNumDirtyPgs(entry.ulNumDirtyPgs),
    lStatus(entry.lStatus)
{
}

Statm::~Statm()
{
}

//------------------------------------------------------------------------------
// Reset the object data
//------------------------------------------------------------------------------
long
Statm::reset(void)
{
  _pid = MAP_ERR_INVALID_PID;
  ulTotProgSize = ulMemPrtsSize = ulNumShrPgs = ulNumCodePgs =
    ulNumD_SPgs = ulNumLibPgs = ulNumDirtyPgs = 0;
  lStatus = 0;
  return lStatus;
}

//------------------------------------------------------------------------------
// Reload the statm data from the statm file
//------------------------------------------------------------------------------
long
Statm::update(void)
{
  lStatus = MAP_ERR_OK;

  // check if we have proper process ID
  if (getPID() < 0)
    return (lStatus = MAP_ERR_INVALID_PID);

  // read the statm file, there is no need to wait
  char fname[40];
  ifstream statmfile;
  sprintf(fname,"/proc/%d/statm",getPID());
  statmfile.open(fname);

  if ((statmfile.rdstate() & ifstream::failbit ) != 0 ) {
    lStatus = statmfile.rdstate();
    return lStatus;
  }

  statmfile >> skipws >> ulTotProgSize >> ulMemPrtsSize >> ulNumShrPgs >> ulNumCodePgs
      >> ulNumD_SPgs >> ulNumLibPgs >> ulNumDirtyPgs;

  statmfile.close();

  return lStatus;
}

//------------------------------------------------------------------------------
// store the statm file for a process (for debugging purposes).
// The storage is in the current running directory;
// no errors are reported
//------------------------------------------------------------------------------
int Statm::StoreDebug(void)
{
  char command[PATH_MAX*2];
  sprintf(command, "cp -f /proc/%d/statm ./statm_%d", getPID(), getPID());
  system(command);

  return 0;
}

//==============================================================================

