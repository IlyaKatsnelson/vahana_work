// Emacs type: -*- C++ -*-
/*
 * Header file for the Statm class
 *
 * This class abstracts data manipulation from the statm file for each process
 * 
 *   statm  Provides information about memory status in pages. The
 *                   columns are:
 *                      size       total program size
 *                      resident   resident set size
 *                      share      shared pages
 *                      trs        text (code)
 *                      drs        data/stack
 *                      lrs        library
 *                      dt         dirty pages
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: Statm.h,v 1.4 2006/09/25 22:37:33 w30205 Exp $
 * -----------------------------------------------------------------------------
 */


#ifndef __STAT_M_H__
#define __STAT_M_H__

#include <iosfwd>
#include <string>

//------------------------------------------------------------------------------
class Statm
{
protected:
  pid_t _pid;
  unsigned long ulTotProgSize;
  unsigned long ulMemPrtsSize;
  unsigned long ulNumShrPgs;
  unsigned long ulNumCodePgs;
  unsigned long ulNumD_SPgs;
  unsigned long ulNumLibPgs;
  unsigned long ulNumDirtyPgs;
  long lStatus;

public:
  // C'tors & D'tor
  Statm();
  Statm(const Statm &entry);

  ~Statm();

  // Accessor methods
  inline const pid_t getPID() const { return _pid; };
  inline unsigned long getTotProgSize(void) const { return ulTotProgSize; };
  inline unsigned long getMemPrtsSize(void) const { return ulMemPrtsSize; };
  inline unsigned long getNumShrPgs(void) const { return ulNumShrPgs; };
  inline unsigned long getNumCodePgs(void) const { return ulNumCodePgs; };
  inline unsigned long getNumD_SPgs(void) const { return ulNumD_SPgs; };
  inline unsigned long getNumLibPgs(void) const { return ulNumLibPgs; };
  inline unsigned long getNumDirtyPgs(void) const { return ulNumDirtyPgs; };
  inline long getStatus(void) const { return lStatus; };

  inline pid_t setPID(pid_t pid) { _pid=pid; return _pid; };

  long update(void);
  long reset(void);

  int StoreDebug(void);
};

#endif // __STAT_M_H__

//==============================================================================

