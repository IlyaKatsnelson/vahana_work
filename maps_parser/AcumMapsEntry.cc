// Emacs type:  -*- C++ -*-
/*
 * Implementation file for AcumMapsEntry class
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * AcumMapsEntry.cc,v 1.2 2007/05/23 22:00:07 w30205 Exp
 * -----------------------------------------------------------------------------
 */

#include <iostream>
#include <iomanip>
#include <string>

#include <cctype>
#include <cstring>
#include <assert.h>

#include "main.h"
#include "AcumMapsEntry.h"
#include "ProcMap.h"

using namespace std;

//==============================================================================
//------------------------------------------------------------------------------
// AcumMapsEntry - child of MapsEntry with some extra features.
//------------------------------------------------------------------------------

// Constructors & destructor

AcumMapsEntry::AcumMapsEntry()
  : MapsEntry()
{
  reset();
}

AcumMapsEntry::AcumMapsEntry(const std::string &name)
  : MapsEntry()
{
  reset();
  setName(name);
}

AcumMapsEntry::AcumMapsEntry(const AcumMapsEntry& entry)
  : MapsEntry(entry),
    _maxCodeRSS(entry._maxCodeRSS),
    _minCodeRSS(entry._maxCodeRSS),
    _maxPRSharedRSS(entry._maxPRSharedRSS),
    _minPRSharedRSS(entry._minPRSharedRSS),
    _maxSRSharedRSS(entry._maxSRSharedRSS),
    _minSRSharedRSS(entry._minSRSharedRSS),
    _maxSWSharedRSS(entry._maxSWSharedRSS),
    _minSWSharedRSS(entry._minSWSharedRSS),
    _sharedCount(entry._sharedCount),
    _reset(entry._reset)
{
  // No implementation
}

AcumMapsEntry::~AcumMapsEntry()
{
  // No implementation
}

void
AcumMapsEntry::add(const MapsEntry &entry)
{
  // add the filenames unless they are equal
  // this will take care of the adding to the default init value
  // if the name is empty, this entry is new
  if (_filename.length() == 0) {
    _filename = entry.getName();
  } else {
    _unique = false;
    _sharedCount++;
  }

  // store the offset values if this is the first time
  // otherwise the procedure is more complecated
  int i;
  int fileOffsPtr[MapsEntry::AllTypes];

  for_each_type(i)
    fileOffsPtr[i] = i;

#ifndef NO_SHR_SEGMENTS

  int j;
  unsigned int tmp_offset = OFFST_DEF;

  if (_reset == true){
    _reset = false;
    for_each_utype(i){
      set_offsetForType(i, entry.get_offsetForType(i));
    }
  } else {
    // verify if the entry is actually correct = no two same offsets
    for_each_utype(i){
      if (entry.get_offsetForType(i) == OFFST_DEF){
	continue;
      }else{
	// **************************************************************
	// WARNING: this algorithm isn't complete. It works reliably
	// for the segments that are not code or data. Actually it works only for the mmaped segments.
	// Also, it works reliably in the case when segment only has one area in memory.
	// there are some cases when code & data have the same offset
	// but they still need to be investigated
	if ((tmp_offset == entry.get_offsetForType(i)) && 
	    (i != MapsEntry::Data && i != MapsEntry::Code && i != MapsEntry::BSS)){
	  std::cerr << "MapsEntry for " << entry.getName() << " has invalid offset format. "
		    << tmp_offset << "|" << entry.get_offsetForType(i) << "|" << i <<endl;
	  break;
	}
	tmp_offset = entry.get_offsetForType(i);
      }
      // do the comparisson
      for_each_utype(j){
	if (get_offsetForType(j) == entry.get_offsetForType(i))
	  fileOffsPtr[j] = i;
      }
    }
  }
#else
  if (_reset == true){
    _reset = false;
    for_each_utype(i){
      set_offsetForType(i, entry.get_offsetForType(i));
    }
  }
#endif // NO_SHR_SEGMENTS

  //=====
  // add the values of the nonshared segments
  ((MapsEntry *)this)->add(entry);

  // perform the accumulation procedure
  // we have to account for the shared memory pages that this entry shares
  // between several processes
  // The fact is that we don't know exactly what memory ranges are shared.
  // Thus two numbers will be generated - best case and worst case

  // === Code
  // find the maximum possible VM space that this module can occupy
  // but it cannot be larger then the code size
  if ((_maxCodeRSS + entry.get_rss_sizeForType(fileOffsPtr[Code])) < entry.get_vm_sizeForType(fileOffsPtr[Code]))
    _maxCodeRSS += entry.get_rss_sizeForType(fileOffsPtr[Code]);
  else
    _maxCodeRSS = entry.get_vm_sizeForType(fileOffsPtr[Code]);

  // find the maximum physical memory space that this module actually occupies
  if (_minCodeRSS < entry.get_rss_sizeForType(fileOffsPtr[Code]))
    _minCodeRSS = entry.get_rss_sizeForType(fileOffsPtr[Code]);

  // === PShared
  if ((_maxPRSharedRSS + entry.get_rss_sizeForType(fileOffsPtr[PRShared])) < entry.get_vm_sizeForType(fileOffsPtr[PRShared]))
    _maxPRSharedRSS += entry.get_rss_sizeForType(fileOffsPtr[PRShared]);
  else
    _maxPRSharedRSS = entry.get_vm_sizeForType(fileOffsPtr[PRShared]);

  // find the maximum physical memory space that this module actually occupies
  if (_minPRSharedRSS < entry.get_rss_sizeForType(fileOffsPtr[PRShared]))
    _minPRSharedRSS = entry.get_rss_sizeForType(fileOffsPtr[PRShared]);

  // === RShared
  if (entry.get_vm_sizeForType(fileOffsPtr[SRShared]) != 0){
    if ((_maxSRSharedRSS + entry.get_rss_sizeForType(fileOffsPtr[SRShared])) < entry.get_vm_sizeForType(fileOffsPtr[SRShared]))
      _maxSRSharedRSS += entry.get_rss_sizeForType(fileOffsPtr[SRShared]);
    else
      _maxSRSharedRSS = entry.get_vm_sizeForType(fileOffsPtr[SRShared]);

    // find the maximum physical memory space that this module actually occupies
    if (_minSRSharedRSS < entry.get_rss_sizeForType(fileOffsPtr[SRShared]))
      _minSRSharedRSS = entry.get_rss_sizeForType(fileOffsPtr[SRShared]);
  }

  // === WShared
  if (entry.get_vm_sizeForType(fileOffsPtr[SWShared]) != 0){
    if ((_maxSWSharedRSS + entry.get_rss_sizeForType(fileOffsPtr[SWShared])) < entry.get_vm_sizeForType(fileOffsPtr[SWShared]))
      _maxSWSharedRSS += entry.get_rss_sizeForType(fileOffsPtr[SWShared]);
    else
      _maxSWSharedRSS = entry.get_vm_sizeForType(fileOffsPtr[SWShared]);

    // find the maximum physical memory space that this module actually occupies
    if (_minSWSharedRSS < entry.get_rss_sizeForType(fileOffsPtr[SWShared]))
      _minSWSharedRSS = entry.get_rss_sizeForType(fileOffsPtr[SWShared]);
  }

  // NOTE: this part is the GROSS oversimplification.
  // It takes care on only one corner case with same-named RShared and WShared segments
  if ((get_offsetForType(SRShared) == entry.get_offsetForType(SWShared) && get_offsetForType(SRShared) != OFFSET_DEF) ||
      (get_offsetForType(SWShared) == entry.get_offsetForType(SRShared) && get_offsetForType(SWShared) != OFFSET_DEF)){

    // move to SWShared because this segment cannot be swapped out = worst case
    // find the maximum virtual memory space that this module actually occupies
    if (_maxSWSharedRSS + _minSRSharedRSS < get_vm_sizeForType(SRShared))
      _maxSWSharedRSS += _minSRSharedRSS;

    // find the maximum physical memory space that this module actually occupies
    if (_minSWSharedRSS < get_rss_sizeForType(SRShared))
      _minSWSharedRSS = get_rss_sizeForType(SRShared);

    _minSRSharedRSS = _maxSRSharedRSS = 0;
    set_sizeForType(SRShared,0);
    set_sizeForType(SWShared,entry.get_vm_sizeForType(SRShared));
  }
}

unsigned long int
AcumMapsEntry::set_offsetForType(long type, unsigned long offset)
{
  if (type < Code || type > Unknown){
    std::cerr << "Error: invalid call to MapsEntry::set_offsetForType(" << type << ")\n";
    exit(-1);
  }

  _fileOffsetArr[type] = offset;

  return offset;
}

void AcumMapsEntry::reset(void)
{
  MapsEntry::reset();
  _maxCodeRSS = 0;
  _minCodeRSS = 0;
  _maxSRSharedRSS = 0;
  _minSRSharedRSS = 0;
  _maxSWSharedRSS = 0;
  _minSWSharedRSS = 0;
  _maxPRSharedRSS = 0;
  _minPRSharedRSS = 0;
  _sharedCount = 1;
  _reset = true;
}

AcumMapsEntry&
AcumMapsEntry::operator=(const AcumMapsEntry &entry)
{
  this->reset();
  (*this) += entry;

  _reset = false;
  return *this;
}

const AcumMapsEntry
AcumMapsEntry::operator+=(const MapsEntry &entry)
{
  // use the add method
  this->add(entry);

  return *this;
}

const AcumMapsEntry
AcumMapsEntry::operator+=(const AcumMapsEntry &entry)
{
  // use the add method from the parent
  MapsEntry::add(entry);

  _maxCodeRSS += entry.getCodeMax();
  _minCodeRSS += entry.getCodeMin();
  _maxSRSharedRSS += entry.getSR_SharedVM();
  _minSRSharedRSS += entry.getSR_SharedRSS();
  _maxSWSharedRSS += entry.getSW_SharedVM();
  _minSWSharedRSS += entry.getSW_SharedRSS();
  _maxPRSharedRSS += entry.getPR_SharedVM();
  _minPRSharedRSS += entry.getPR_SharedRSS();
  _sharedCount += entry.getSharedCount();

  return *this;
}

int AcumMapsEntry::table_print(ostream& out)
{
  // "Best case|Worst case| Data   | Heap   | Stack  |PSh best |PSh worst|
  //  Sh best | Sh worst| ShWbest | ShWworst| Other  |U|Cnt |Module name|\n";

  out << dec << setiosflags(ios::left)
      << setw(8) << getCodeMin() << "|"
      << setw(8) << getCodeMax() << "|"
      << setw(8) << getDataRSS() << "|"
      << setw(8) << getBSSRSS() << "|"
      << setw(8) << getHeapRSS() << "|"
      << setw(8) << getStackRSS() << "|"
      << setw(8) << getPR_SharedRSS() << "|"
      << setw(8) << getPR_SharedVM() << "|"
      << setw(8) << getSR_SharedRSS() << "|"
      << setw(8) << getSR_SharedVM() << "|"
      << setw(8) << getSW_SharedRSS() << "|"
      << setw(8) << getSW_SharedVM() << "|"
      << setw(8) << getUnknownRSS() << "|"
      << setw(4) << getSharedCount() << "|";
  out << _filename << "\n";

  return 0;
}
int AcumMapsEntry::table_print_DP(ostream& out)
{
  // "Best case|Worst case| Data   | Heap   | Stack  |PSh best |PSh worst|
  //  Sh best | Sh worst| ShWbest | ShWworst| Other  |U|Cnt |Module name|\n";

  out << dec << setiosflags(ios::left)
      << setw(8) << getCodeMin() << "|"
      << setw(8) << getCodeMax() << "|"
      << setw(8) << getDataRSS() << "|"
      << setw(8) << getBSSRSS() << "|"
      << setw(8) << getHeapRSS() << "|"
      << setw(8) << getStackRSS() << "|"
      << setw(8) << getPR_SharedRSS() << "|"
      << setw(8) << getPR_SharedVM() << "|"
      << setw(8) << getSR_SharedRSS() << "|"
      << setw(8) << getSR_SharedVM() << "|"
      << setw(8) << getSW_SharedRSS() << "|"
      << setw(8) << getSW_SharedVM() << "|"
      << setw(8) << getUnknownRSS() << "|"
      << setw(8) << totalDP() << "|"
      << setw(4) << getSharedCount() << "|";
  out << _filename << "\n";

  return 0;
}

int AcumMapsEntry::table_head_print(std::ostream& out)
{
  out << " Cd Best| Cd Wrst|"
      << " Data   | BSS    | Heap   | Stack  |"
      << " PR Best| PR Wrst|"
      << " SR Best| SR Wrst|"
      << " SW Best| SW Wrst|"
      << " Other  |Cnt |Module name\n";
  return 0;
}

int AcumMapsEntry::table_head_print_DP(std::ostream& out)
{
  out << " Cd Best| Cd Wrst|"
      << " Data   | BSS    | Heap   | Stack  |"
      << " PR Best| PR Wrst|"
      << " SR Best| SR Wrst|"
      << " SW Best| SW Wrst|"
      << " Other  | Dt tot |Cnt |Module name\n";
  return 0;
}

//==============================================================================
