#
# -----------------------------------------------------------------------------
# Makefile for the Linux system memory analysis tool.
# See the man pages for detailed information.
# -----------------------------------------------------------------------------
#
# $Id: Makefile,v 1.20 2007/06/01 21:27:09 w30205 Exp $
# -----------------------------------------------------------------------------
# General configuration

CMDNAME = maps_parser
COLLNAME = maps_coll
MPOBJS	= BaseMapsEntry.o MapsEntry.o AcumMapsEntry.o ProcMap.o MemInfo.o Statm.o Stat.o SlabInfo.o \
	MemMapInfo.o SystemInfo.o ParserModule.o main.o
CLOBJS 	= ParserModule.o maps_coll.o

# -----------------------------------------------------------------------------
# products grouped by processor
MP_PRODUCT_LIST_I386 = RH REDHAT
MP_PRODUCT_LIST_IWMMXT = A780 E680 Sumatra A910
MP_PRODUCT_LIST_ARM11 = ascension scma11evb scma11ref buteref butebb elba lido pico saipan zeusevb marco
MP_PRODUCT_LIST = $(MP_PRODUCT_LIST_IWMMXT) $(MP_PRODUCT_LIST_ARM11) \
		$(MP_PRODUCT_LIST_I386)
MP_PRODUCT_LIST_PHONE = $(MP_PRODUCT_LIST_IWMMXT) $(MP_PRODUCT_LIST_ARM11)

ifeq ($(origin PRODUCT), undefined)
  PRODUCT	= RH
endif

MP_PROD_ARM11 	= $(filter $(PRODUCT),$(MP_PRODUCT_LIST_ARM11))
MP_PROD_IWMMXT 	= $(filter $(PRODUCT),$(MP_PRODUCT_LIST_IWMMXT))
MP_PROD_I386 	= $(filter $(PRODUCT),$(MP_PRODUCT_LIST_I386))

# Simple product verification
ifeq ($(filter $(PRODUCT),$(MP_PRODUCT_LIST)),)
  PRODUCT_CHK_MSG= "The specified product - $(PRODUCT) - is not supported yet."
  PRODUCT_CHK 	= $(PRODUCT)
  $(error $(PRODUCT_CHK_MSG))
endif

ifneq ($(filter $(PRODUCT),$(MP_PRODUCT_LIST_PHONE)),)
  BLD_TARGET	= PHONE
endif

# -----------------------------------------------------------------------------
ifneq ($(strip $(MP_PROD_ARM11)),)
# all configuration for SCM-A11 is set in this file

	include ${ROOT}/vobs/linuxjava/platform/Makefile.options
	include $(BOOTSTRAP)

	PROPFILES = $(PLATFORM_DIR)/build/os/properties
#	include ${ROOT}/vobs/jem/os/Makefile.env
	CXX     = $(CC)
	ifdef STRIP
		ifeq ($(STRIP),true)
			STRIP_TOOL 	= $(TOOLPREFIX)strip
		endif
	endif
else

  ifneq ($(MP_PROD_IWMMXT),)
	ARCH 	= arm
	ifeq ($(origin TOOLCHAIN_HOME), undefined)
		TOOLCHAIN_HOME = /vobs/jem/tools
	endif

	ifeq ($(origin GCC), undefined)
		GCC = iwmmxt_le-gcc
	endif

	CXX     = $(TOOLCHAIN_HOME)/bin/$(GCC)

	ifdef STRIP
		ifeq ($(STRIP),true)
			STRIP_TOOL 	= $(TOOLCHAIN_HOME)/bin/iwmmxt_le-strip
		endif
	endif

  else
	ARCH 	= i386
	CXX     = gcc -DHOST
  endif

endif

ifeq ($(origin QUANTIFY), undefined)
  QCXX	=
else
  QCXX	= $(QUANTIFY_LOC)
endif

# -----------------------------------------------------------------------------
ifneq ($(MP_PROD_IWMMXT),)
  VERSION 	= 2
  PATCHLEVEL 	= 4
  ifdef BLD_TARGET
	ifeq ($(origin SUBLEVEL), undefined)
		SUBLEVEL 	= 20_mvlcee31
	endif
	ifeq ($(origin EXTRAVERSION), undefined)
		EXTRAVERSION 	=
	endif
  else
	ifeq ($(origin SUBLEVEL), undefined)
		SUBLEVEL 	= 21
	endif
	ifeq ($(origin EXTRAVERSION), undefined)
		EXTRAVERSION 	= -20.EL
	endif
  endif
endif

# -----------------------------------------------------------------------------
ifeq ($(origin DEBUG), undefined)
	CXXFLAGS  	= -time -O2 -std=c++17 -frerun-loop-opt -frerun-cse-after-loop -Wall -s
else
  ifeq ($(DEBUG), true)
	CXXFLAGS  	= -DDEBUG -std=c++17 -O1 -pg -g -Wall
  else
	CXXFLAGS  	= -time -O2 -std=c++17 -frerun-loop-opt -frerun-cse-after-loop -Wall -s
  endif
endif

DEFS   		=
LDFLAGS 	=
ifeq ($(origin DEBUG), undefined)
	LIBS    = -lstdc++
else
  ifeq ($(origin QUANTIFY), undefined)
	LIBS   	= -lstdc++ -lc
	LDFLAGS	= -pg -g
  else
	LIBS   	= -lstdc++
	LDFLAGS	= -g
  endif
endif

ifeq ($(origin INCLUDE_TOP), undefined)
  ifeq ($(origin BLD_TARGET), undefined)
	INCLUDE_TOP 	= /lib/modules/$(shell echo `uname -r`)/build
	INCLUDE 	= -isystem $(INCLUDE_TOP)/include
	CXXFLAGS 	+= -DREDHAT
  else
	INCLUDE_TOP = /vobs/jem/hardhat/linux-2.4.x
	INCLUDE	= -isystem $(INCLUDE_TOP)/include
  endif
else
	INCLUDE	= -isystem $(INCLUDE_TOP)/include
endif

# -----------------------------------------------------------------------------
# Unconfigured kernel source directory error message text
#
configured_kernel_src_txt = "Please configure the linux kernel source directory. \
	Building Product/build specific kernel is one way, e.g. \
	build PRODUCT=SUMATRA linux_kernel in the /vobs/ezx_platform/code directory \
	will build the kernel for EZX/Sumatra phones or \
	clearmake PRODUCT=scma11ref in the /vobs/jem/os directory \
	will build the kernel for SCM-A11 phones."
#
# the links needed for build from the configured kernel source tree
#
ifneq ($(MP_PROD_ARM11),)
  KERNEL_SRC_LN_1 = $(INCLUDEDIR)/asm
  KERNEL_SRC_LN_2 = $(INCLUDEDIR)/asm-$(ARCH)/arch
else
  KERNEL_SRC_LN_1 = $(INCLUDE_TOP)/include/asm
  ifneq ($(ARCH), i386)
	KERNEL_SRC_LN_2 = $(INCLUDE_TOP)/include/asm-$(ARCH)/arch
	KERNEL_SRC_LN_3 = $(INCLUDE_TOP)/include/asm-$(ARCH)/proc
  endif
endif

# -----------------------------------------------------------------------------
# Flags for the kernel module
#
MODULE_NAME	= parser_module
obj-m 		+= $(MODULE_NAME).o

WARN    	= -W -Wall -Wstrict-prototypes -Wmissing-prototypes

MP_MOD_STRIP 	=
MP_MP_STRIP 	=
MP_MC_STRIP 	=
ifeq ($(STRIP),true)
	MP_MOD_STRIP 	= $(STRIP_TOOL) --strip-unneeded $(MODULE_NAME).ko
	MP_MP_STRIP 	= $(STRIP_TOOL) $(CMDNAME);
	MP_MC_STRIP 	= $(STRIP_TOOL) $(COLLNAME);
endif

# The setup is different for 2.4 and 2.6 kernels
ifneq ($(MP_PROD_ARM11),)
  KM_COMPILE = /vobs/linuxjava/platform/tools/bin/cleargmake -A /vobs/linuxjava/platform/build/os/properties \
	-C /vobs/linuxjava/platform/api/dir/linux_build ARCH=$(ARCH) CC=$(CC) LD=$(LD) \
	O=$(LINUXBUILD) M=$(shell pwd) PROPFILES=$(PROPFILES) modules
  HAS_MEMMAP_SUPPORT =
else
  ifeq ($(origin DEBUG), undefined)
	KCFLAGS = -time -O2 -D__KERNEL__ -DMODULE ${WARN} ${INCLUDE}
  else
	KCFLAGS = -time -g -DDEBUG -D__KERNEL__ -DMODULE ${WARN} ${INCLUDE}
  endif

  ifeq ($(origin BLD_TARGET), undefined)
	KCFLAGS += -DREDHAT
  else
	KCFLAGS +=
  endif

  KM_COMPILE = $(CXX) $(KCFLAGS) -c $(MODULE_NAME).c -o $(MODULE_NAME).ko
endif

ifneq ($(origin HAS_MEMMAP_SUPPORT), undefined)
  CXXFLAGS	+= -DHAS_MEMMAP_SUPPORT
endif

# -----------------------------------------------------------------------------
all: $(CMDNAME) mod coll

$(CMDNAME): configured_kernel_src $(MPOBJS)
	$(QCXX) $(CXX) $(LDFLAGS) -o $(CMDNAME) $(MPOBJS) $(LIBS)
	$(MP_MP_STRIP)
	chmod a+x $(CMDNAME)

coll: $(CLOBJS)
	$(QCXX) $(CXX) $(LDFLAGS) -o $(COLLNAME) $(CLOBJS) $(LIBS)
	$(MP_MC_STRIP)
	chmod a+rx $(COLLNAME)

.c.o:
	$(CXX) $(CXXFLAGS) $(DEFS) -c $<

mod: configured_kernel_src
	$(KM_COMPILE)
	$(MP_MOD_STRIP)
	chmod a+rx $(MODULE_NAME).ko

configured_kernel_src:
#$(KERNEL_SRC_LN_1) $(KERNEL_SRC_LN_2) $(KERNEL_SRC_LN_3)

$(KERNEL_SRC_LN_1):
	$(error "Can't find directory: $(KERNEL_SRC_LN_1). $(configured_kernel_src_txt)")

$(KERNEL_SRC_LN_2):
	$(error "Can't find directory: $(KERNEL_SRC_LN_2). $(configured_kernel_src_txt)")

$(KERNEL_SRC_LN_3):
	$(error "Can't find directory: $(KERNEL_SRC_LN_3). $(configured_kernel_src_txt)")

clean:
	rm -f *.o
	rm -f *.ko
	rm -f *~
	rm -f *.mod.c .*.*.cmd
	rm -f core $(CMDNAME) $(COLLNAME)
	rm -rf .tmp_versions

purge:
	rm -f *.o
	rm -f *.*~
	rm -f core
	rm -f *.mod.c .*.*.cmd
	rm -rf .tmp_versions

help:
	@@echo ""
	@@echo "This makefile supports the following configurations:"
	@@echo "    ${MP_PRODUCT_LIST}"
	@@echo ""
	@@echo "Makefile Usage:"
	@@echo "    make [PRODUCT=<product>]"
	@@echo "    make [PRODUCT=<product>] mod"
	@@echo "    make [PRODUCT=<product>] $(CMDNAME)"
	@@echo "    make all"
	@@echo "    make help"
	@@echo "    make clean"
	@@echo "    make purge"
	@@echo ""
	@@echo "Target 'all' will build all components for the configured product."
	@@echo "Target 'clean' will clean all the generated files."
	@@echo "Target 'purge' will remove all built files from all configurations."
	@@echo "Target 'help' prints this message."
	@@echo ""
	@@echo ""

# -----------------------------------------------------------------------------
