// Emacs type: -*- C++ -*-
/*
 * Header file for the MemInfo class
 *
 * This class abstracts data manipulation from the meminfo file
 * 
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: MemInfo.h,v 1.2 2006/09/25 22:37:33 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#ifndef __MEM_INFO_H__
#define __MEM_INFO_H__

#include <iosfwd>
#include <string>

//------------------------------------------------------------------------------
class MemInfo
{
 public:
  typedef enum _mem_info_fields
    {
      INFO_FNUM = 7
    };

 protected:
  unsigned long ulTotMem;
  unsigned long ulTotUsed;
  unsigned long ulTotFree;
  unsigned long ulShared;
  unsigned long ulBuffers;
  unsigned long ulCached;
  unsigned long ulActive;
  long lStatus;

 public:
  // C'tors & D'tor
  MemInfo();
  MemInfo(const MemInfo &entry);

  ~MemInfo();

  // Accessor methods
  inline unsigned long getTotMem(void) const { return ulTotMem; };
  inline unsigned long getTotUsed(void) const { return ulTotUsed; };
  inline unsigned long getTotFree(void) const { return ulTotFree; };
  inline unsigned long getShared(void) const { return ulShared; };
  inline unsigned long getBuffers(void) const { return ulBuffers; };
  inline unsigned long getCached(void) const { return ulCached; };
  inline unsigned long getActive(void) const { return ulActive; };
  inline long getStatus(void) const { return lStatus; };

  long update(void);
};

#endif // __MEM_INFO_H__

//==============================================================================

