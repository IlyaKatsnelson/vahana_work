// Emacs type:  -*- C++ -*-
/*
 * Implementation file for ProcMap class
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: ProcMap.cc,v 1.19 2007/05/24 01:13:01 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

#include <cctype>
#include <cstring>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <linux/limits.h>

#include "ProcMap.h"
#include "ParserModule.h"
#include "MemMapInfo.h"
#include "main.h"

using namespace std;

// global system information object
extern SystemInfo hostSystemInfo;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Constructors & destructor
ProcMap::ProcMap()
  : _pid(0),
    _cmdLine(""),
    _errorMsg(""),
    _totalSize(0),
    _LWPList(),
    _StackVector(),
    _map(),
    _uniqueAccumMap("[unique total]"),
    _totAccumMap("[process total]"),
    _statm(),
    _parserModule(NULL)
{
  // No implementation
}

ProcMap::ProcMap(pid_t pid)
{
  this->reset();
  setPID(pid);
  _statm.setPID(pid);
}

ProcMap::ProcMap(ParserModule *pmodule)
{
  this->reset();
  _parserModule = pmodule;
}

ProcMap::ProcMap(const ProcMap& entry)
  : _pid(entry._pid),
    _cmdLine(entry._cmdLine),
    _errorMsg(entry._errorMsg),
    _totalSize(entry._totalSize),
    _LWPList(entry._LWPList),
    _StackVector(entry._StackVector),
    _map(entry._map),
    _uniqueAccumMap(entry._uniqueAccumMap),
    _totAccumMap(entry._totAccumMap),
    _statm(entry._statm),
    _parserModule(entry._parserModule)
{
}

ProcMap::~ProcMap()
{
  // No implementation
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void ProcMap::reset(void)
{
  for (Entries::iterator iter = _map.begin(); iter != _map.end(); ++iter){
    (iter->second).reset();
  }
  _uniqueAccumMap.reset();
  _uniqueAccumMap.setName("[unique total]");
  _totAccumMap.reset();
  _totAccumMap.setName("[process total]");
  _statm.reset();
  _pid = 0;
  _cmdLine.clear();
  _errorMsg.clear();
  _LWPList.clear();
  _StackVector.clear();
  _totalSize = 0;
  _parserModule = NULL;
}

//------------------------------------------------------------------------------
// Populate the header information for the specified PID
//------------------------------------------------------------------------------
int ProcMap::HeadPopulate(int pid){

  // do the work on the PID that was sent from above, if needed
  if (this->_pid != pid)
    this->_pid = pid;

  // Record the actual command line, for better output
  if (WorkCmdLine() != 0)
    return 1;

  // Record the data from the stat file
  if (WorkStat() != 0)
    return 1;

  // Record the data from the statm file
  if (WorkStatm() != 0)
    return 1;

  // Record the memory segment data
  if (WorkMemSegment() != 0)
    return 1;

  // get the stack vector
  if (_parserModule != NULL){
    _parserModule->readStackMapVector(pid,_StackVector);
  }

  return 0;
}

//------------------------------------------------------------------------------
// Populate the main memory map information for the specified PID
//------------------------------------------------------------------------------
int ProcMap::MapsPopulate(int pid, const bool rss_ovr){

  char filepath[PROC_PATH_MAX];

  // Check if the maps file is there
  sprintf(filepath, "/proc/%d/maps", pid);

  ifstream mapsfile(filepath);
  if (! mapsfile) {
    _errorMsg += "Unable to open file [" + string(filepath) + "]: " + string(strerror(errno));
    return 1;
  }

  MapsEntry maps_tmp(this);
  unsigned int index = 0;
  string segment_name, psegment_name;
  MapsEntry::_map_type type, ptype;

  // Populate the map of this process modules
  // if memmaps are not supported, just get the RSS array from the kernel.
  vector<unsigned long > RSSv;
  vector<unsigned long > DPv;
  vector<unsigned long >::size_type vsize1, vsize2;

  if (_parserModule != NULL){
    if ((rss_ovr == true) && (hostSystemInfo.getMemMapSupported() == true)){
      // get the RSS from MemMap if the user wants to verify
      // This event shouldn't be run often, as a result MemMapInfo is used
      MemMapInfo mem_map = MemMapInfo();
      mem_map.RSSPopulate(pid);
      mem_map.getRSS_vector_copy(RSSv);
    } else {
      _parserModule->readRSSMapVector(pid,RSSv);
    }
    _parserModule->readDirtyMapVector(pid,DPv);
  }
  vsize1 = RSSv.size();
  vsize2 = DPv.size();
  assert(vsize1 == vsize2);

  ptype = MapsEntry::Unknown;
  while (maps_tmp.WorkLine(mapsfile) == 0)
  {
    type = figure_segment(maps_tmp, ptype);
    if (type == MapsEntry::Data)
      psegment_name = maps_tmp.getName();

    // In the case of 'empty' mane we either set the generic name
    // or use the previous segment's name for BSS
    if (maps_tmp.getName().empty() == true){
      if (type == MapsEntry::BSS){
	maps_tmp.setName(psegment_name);
	psegment_name.erase();
      }else{
	segment_name.erase();
	maps_tmp.setName(get_gen_name(type, segment_name));
      }
    }

    maps_tmp.update_sizes(type, ((index < vsize1) ? RSSv[index] : 0),
			  ((index < vsize1) ? DPv[index] : 0), rss_ovr);

    _map[maps_tmp.getName()].add_type(maps_tmp, type);

    // leave if the end of file was found
    if (mapsfile.eof())
      break;

    maps_tmp.reset();
    index++;
    ptype = type;
  }

  if ((mapsfile.bad()) || (mapsfile.fail())){
    _errorMsg += "Error parsing file [" + string(filepath) + "]";
    return 1;
  }
  mapsfile.close();

  return 0;
}

//------------------------------------------------------------------------------
// Populate this map with the entries
//------------------------------------------------------------------------------
int ProcMap::Populate(int pid, const bool rss_ovr){

  // 1. Get the header info
  if (HeadPopulate(pid) != 0)
    return 1;

  // 2. Get the maps meat data
  if (MapsPopulate(pid, rss_ovr) != 0)
    return 1;

  Entries::const_iterator end = _map.end();
  for (Entries::const_iterator iter = _map.begin(); iter != end; ++iter){
    _totalSize += (iter->second).get_rss_sizeForType(MapsEntry::Data);
    _totalSize += (iter->second).get_rss_sizeForType(MapsEntry::Heap);
    _totalSize += (iter->second).get_rss_sizeForType(MapsEntry::Stack);
    _totalSize += (iter->second).get_rss_sizeForType(MapsEntry::PRShared);
    _totalSize += (iter->second).get_rss_sizeForType(MapsEntry::SRShared);
    _totalSize += (iter->second).get_rss_sizeForType(MapsEntry::SWShared);
  }
  return 0;
}

//------------------------------------------------------------------------------
// Populate the unique information about this process: all the memory
// that only this process has
//------------------------------------------------------------------------------
int ProcMap::CollectMemory(void){

  if (getMapC().size() == 0)
    return -1;

  Entries::const_iterator iter;
  Entries::const_iterator end = getMapC().end();
  for (iter = getMapC().begin(); iter != end; ++iter){
    // collect the unique memory
    _uniqueAccumMap.addUData(iter->second);
    // collect all memory
    _totAccumMap.add(iter->second);
  }

  return 0;
}

//------------------------------------------------------------------------------
// Do all the work on the cmdLine file
//------------------------------------------------------------------------------
int ProcMap::WorkCmdLine(void){

  char filepath[PROC_PATH_MAX];
  char letter = 0;
  sprintf(filepath, "/proc/%d/cmdline", getPID());
  ifstream cmdLineFile(filepath, ios::in);

  // process the cmdLine
  // the kernel stores the command line as a set of zero-separated strings
  // this loop just replaces the zeros with spaces.
  // The first chunk is the actual executable.
  letter = cmdLineFile.get();
  while (cmdLineFile.eof() != true && letter != '\n'){
    if(letter == 0 || letter == '\n')
      letter = ' ';

    _cmdLine += letter;
    letter = cmdLineFile.get();
  }

  return 0;
}

//------------------------------------------------------------------------------
// Do all the work on the stat file.
// Format: [pid %d] [comm %s] [state %c] [ppid %d] [pgrp %d] [session %d] [tty_nr %d]
//         [tpgid %d] [flags %lu] [minflt %lu] [cminflt %lu] [majflt %lu] [cmajflt %lu]
//         [utime %lu] [stime %lu] [cutime %ld] [cstime %ld] [priority %ld]
//         [nice %ld] [0 %ld] [itrealvalue %ld] [starttime %lu] [vsize %lu] [rss %ld]
//         [rlim %lu] [startcode %lu] [endcode %lu] [startstack %lu] [kstkesp %lu]
//         [kstkeip %lu] [signal %lu] [blocked %lu] [sigignore %lu] [sigcatch %lu]
//         [wchan %lu] [nswap %lu] [cnswap %lu] [exit_signal %d] [processor %d]
//------------------------------------------------------------------------------
int ProcMap::WorkStat(void){

  _stat.setPID(this->getPID());
  _stat.update();

  return 0;
}

//------------------------------------------------------------------------------
// Do all the work on the statm file
//------------------------------------------------------------------------------
int ProcMap::WorkStatm(void){

  _statm.setPID(this->getPID());
  _statm.update();

  return 0;
}

//------------------------------------------------------------------------------
// Do all the work on the process memory structure information
// Format: start_code
//         end_code
//         start_data
//         end_data
//         start_brk
//         brk
//         start_stack
//         arg_start
//         arg_end
//         env_start
//         env_end
//         rss
//         total_vm
//         locked_vm
//
// start_code, end_code - The start and end address of the code section.
// start_data, end_data - The start and end address of the data section.
//       start_brk, brk - The start and end address of the heap.
//          start_stack - Predictably enough, the start of the stack region.
//   arg_start, arg_end - The start and end address of command-line arguments.
//   env_start, env_end - The start and end address of environment variables.
//                  rss - RSS is the number of resident pages for this process.
//                      It should be noted that the global zero page is not accounted for by RSS.
//             total_vm - The total memory space occupied by all VMA regions in the process.
//            locked_vm - The number of resident pages locked in memory.
//------------------------------------------------------------------------------
int ProcMap::WorkMemSegment(void){

  int status = -1;

  if (getParserModule() != NULL){
    char *buf;
    unsigned int buf_size = 1024;

    buf = (char *)malloc(buf_size);
    if (buf == NULL){
      fprintf(stderr,"pid=%d.cannot allocate data buffer.\n",getPID());
      return -1;
    }
    status = _parserModule->readMemSegmentMap(getPID(), buf, buf_size);
    if (_parserModule->iserror()){
      fprintf(stderr,"Error reading memory segment: %d.\n", status);
      _parserModule->clearerror();
      free(buf);
      return status;
    }

    status = 0;
    status = sscanf(buf, "%lX %lX %lX %lX %lX %lX %lX %lX %lX %lX %lX %ld %ld %ld\n",
        &_start_code, &_end_code, &_start_data, &_end_data,
        &_start_brk, &_brk, &_start_stack,
        &_arg_start, &_arg_end, &_env_start, &_env_end,
        &_rss, &_total_vm, &_locked_vm);
    if (status < 14)
      fprintf(stderr,"Warning: error reading the memory segment info: %s.\n", buf);

    free(buf);
  }

  return 0;
}

//------------------------------------------------------------------------------
// Print the information out
//------------------------------------------------------------------------------
ostream& operator<<(ostream& out, const ProcMap& entry)
{
  for (Entries::const_iterator iter = entry.getMapC().begin(); iter != entry.getMapC().end(); ++iter){
    out << iter->second;
  }

  out << "\nEstimated total unique process memory usage: " << entry.getTotalSize() << " bytes ("
      << (entry.getTotalSize() / 1024) << "k)\n"
      << "(Based on Total RSS for executable + (data + stack + heap + shared RSSs from each library)" << endl;

  return out;
}

//------------------------------------------------------------------------------
// Print the data in the nice table form
// Flag values:
//  MAP_UNIQUE_OUT - print the total unique used memory
//------------------------------------------------------------------------------
int ProcMap::table_print(ostream& out, int flag)
{
  // accumulate the unshared (unique) segments that this process has

  out << "\n";
  out << " " <<  getCmdName() << "\n " << getCmdLine() << "\n"
      << "pid: " << getPID() << "\t pgrid: " << getPGRP() << "\t ppid: " << getPPID();
  if (getLWPList().empty() != true)
    out << "\tLWPs:" << getLWPListStr();

  out << "\n";
  if((flag & MAP_DIRTY_PGS) == 0){
    MapsEntry::table_head_print(out);
  }else{
    MapsEntry::table_head_print_RSS_DP(out);
  }
  MapsEntry::table_head_print_line(out);

  Entries::iterator iter;
  Entries::iterator end = getMap()->end();
  for (iter = getMap()->begin(); iter != end; ++iter){
    if((flag & MAP_DIRTY_PGS) == 0){
      (iter->second).table_print(out);
    }else{
      (iter->second).table_print_dp(out);
    }
  }
  MapsEntry::table_head_print_line(out);

  // print the accumulated values
  if((flag & MAP_DIRTY_PGS) == 0){
    _uniqueAccumMap.table_print(out);
  }else{
    _uniqueAccumMap.table_print_dp(out);
  }
  MapsEntry::table_head_print_line(out);
  out << " Unique RSS total:\t" << (_uniqueAccumMap.totalRSS()) << "\n";

  if((flag & MAP_DIRTY_PGS) != 0){
    out << "         DP total:\t" << (_totAccumMap.totalDP()) << "\n";
  }
  MapsEntry::table_head_print_line(out);

  return 0;
}

//------------------------------------------------------------------------------
// Add one more LWP to the list in this process
//------------------------------------------------------------------------------
int ProcMap::addLWP(int lwp)
{
  _LWPList.insert(lwp);
  return 0;
}

//------------------------------------------------------------------------------
// Add one more LWP to the list in this process
//------------------------------------------------------------------------------
string ProcMap::getLWPListStr()
{
  char num[10];
  string out("");

  LWPSet::const_iterator i = getLWPList().begin();
  LWPSet::const_iterator end = getLWPList().end();
  for (;i!=end;i++){
    sprintf(num," %9d",*i);
    out += num;
  }

  return out;
}

//------------------------------------------------------------------------------
// Compare two maps: this and the second.
// Return 0 if different and 1 if the same
//------------------------------------------------------------------------------
int ProcMap::cmpMap(const ProcMap &second)
{
  //simple initial check - the size
  // maps cannot contain duplicate entries, as a result they must have the same size
  const Entries second_map = second.getMapC();
  if (getMapC().size() != second_map.size())
    return 0;

  // the maps have to be exact, i.e. all the lines in the same order
  Entries::const_iterator iter1 = getMapC().begin();
  Entries::const_iterator iter2 = second_map.begin();
  for (; iter1 != getMapC().end() && iter2 != second_map.end(); ++iter1, ++iter2){

    if (iter1->second != iter2->second)
      return 0;
  }

  return 1;
}

//------------------------------------------------------------------------------
// Compare two process trees: this and the second.
// Return 0 if not compartible and 1 if in the same tree.
// 	The function checks if the two maps being compared are actually LWPs in one process
// 	The function uses the assumption that the LWPs in one process will be
// 	arranged in a tree structure with the initial process as the root parent.
//	The function assumes that the processes are processed in sequence from lower PID
//	to higher.
//------------------------------------------------------------------------------
int ProcMap::cmpTree(const ProcMap &second)
{
  // simple initial check - order of the numbers
  // the second's PID should be smalller then the first's
  if (getPID() < second.getPID())
    return 0;

  // the PIDs should never be the same
  assert(getPID() != second.getPID());

  // the main check loop
  // the second's parent is either the first itself or one of
  // the processes in the first's LWP list.
  if (getPID() == second.getPPID())
    return 1;

  LWPSet::const_iterator s1_Iter = getLWPList().find(second.getPPID());
  if (s1_Iter != getLWPList().end())
    return 1;

  return 0;
}

//------------------------------------------------------------------------------
// store the maps and statm files for a process (for debugging purposes).
// The storage is in the current running directory;
// no errors are reported
//------------------------------------------------------------------------------
int ProcMap::StoreDebug(void)
{
  char command[PATH_MAX];
  sprintf(command, "cp -f /proc/%d/maps ./maps_%d", getPID(), getPID());
  system(command);

  _statm.StoreDebug();
  _stat.StoreDebug();

  if (hostSystemInfo.getMemMapSupported()){
    sprintf(command, "cp -f /proc/%d/memmap ./memmap_%d", getPID(), getPID());
    system(command);
  }

  return 0;
}

//------------------------------------------------------------------------------
// Figure out what segment the current line in a maps file belongs to
// the naming here is for the general segment usage.
//------------------------------------------------------------------------------
MapsEntry::map_type
ProcMap::figure_segment(const BaseMapsEntry &bm, MapsEntry::map_type ptype)
{
  // figure out what we know
  // a. code
  if ((strcmp(bm.get_flags_c(),"r-xp") == 0) && (bm.getName().size() != 0)){
    return (MapsEntry::Code);
  }

  // b. data
  if ((strcmp(bm.get_flags_c(),"rw-p") == 0) && (bm.getName().size() != 0)){
    return (MapsEntry::Data);
  }

  // c. heap
  if ((bm.get_vm_start() >= _start_brk) && (bm.get_vm_end() <= _brk) &&
      (bm.getName().size() == 0))
    return (MapsEntry::Heap);

  // d. stack
  int size = _StackVector.size();
  for (int i=0;i<size;i=i+2){
    if ((bm.get_vm_start() >= _StackVector[i]) && (bm.get_vm_end() <= _StackVector[i+1]) &&
	(bm.getName().size() == 0))
      return (MapsEntry::Stack);
  }

  // e. BSS and something else
  if (strcmp(bm.get_flags_c(),"rw-p") == 0){
    // BSS segment depends on who came before
    if ((bm.getName().size() == 0) && (ptype == MapsEntry::Data))
      return (MapsEntry::BSS);

    // if the segment is Unnamed with this attributes and not BSS,
    // it will concidered as data
    return (MapsEntry::Data);
  }

  // f. shared segments
  if (strcmp(bm.get_flags_c(),"r--p") == 0){
    return (MapsEntry::PRShared);
  }
  if (strcmp(bm.get_flags_c(),"r--s") == 0){
    return (MapsEntry::SRShared);
  }
  if (strcmp(bm.get_flags_c(),"rw-s") == 0){
    return (MapsEntry::SWShared);
  }

  if (strcmp(bm.get_flags_c(),"---p") == 0 || strcmp(bm.get_flags_c(),"---s") == 0){
    // "---p" is the separation page between segments
    // set it to the unknown bunch, but no warning
    return(MapsEntry::Unknown);
  }

  // It gets tricky here. Sometimes thread stacks are not set to grow down
  // we will set them to stack if they have "rwxp" permission and
  // are not heap and have virtual address with bits in the left most octet
  if (strcmp(bm.get_flags_c(),"rwxp") == 0){
    if ((bm.getName().empty() == true) &&
	((bm.get_vm_start() >> (sizeof(unsigned long)*8 - 4)) & 0xF) != 0)
      return (MapsEntry::Stack);
  }

  // if we are here then we encountered some unknown segment
  cerr << "Warning: found segment with unsupported attributes " << bm.get_flags()
       << ". PID: " << getPID() << " Address range: "
       << setw(8) << hex << bm.get_vm_start() << "-" << bm.get_vm_end() << " " << dec;
  cerr << bm.getName();

  cerr << "\n\tThe data will be summed to the Other category.";
  cerr << "\n\tPlease contact the program designer.\n";

  return (MapsEntry::Unknown);
}

//------------------------------------------------------------------------------
// Get the name for this type of segment
//------------------------------------------------------------------------------
string&
ProcMap::get_gen_name(const MapsEntry::map_type type, string &name_str)
{
  name_str.clear();
  if (type == MapsEntry::PRShared || type == MapsEntry::SRShared || type == MapsEntry::Code){
    name_str = "RO_";
  }else{
    char tmp_str[12];
    sprintf(tmp_str,"_%d_",this->getPID());
    name_str += (this->getCmdName() + tmp_str);
  }
  name_str += GENERAL_STR;

  return name_str;
}

//------------------------------------------------------------------------------
// Print the process table of the RSS data
//------------------------------------------------------------------------------
int ProcMap::table_printRSS_tot(ostream& out)
{
  // print the core data
  _uniqueAccumMap.table_printRSS2(out);

  // print the tail
  out << setiosflags(ios::left) << "|"
      << setw(8) << _uniqueAccumMap.totalRSS() << "|"
      << setw(8) << _statm.getNumDirtyPgs() << "|"
      << getCmdName() << "\n";

  return 0;
}

//------------------------------------------------------------------------------
// Print the process table of the RSS data with the separation on
// Unique and Shared segments
//------------------------------------------------------------------------------
int ProcMap::table_print_U_S(ostream& out)
{
  // print the core data
  out << setiosflags(ios::left)
      << setw(8) << _uniqueAccumMap.getCodeRSS() << "|"
      << setw(8) << (_totAccumMap.getCodeRSS() - _uniqueAccumMap.getCodeRSS()) << "|"
      << setw(8) << _uniqueAccumMap.getDataRSS() << "|"
      << setw(8) << _uniqueAccumMap.getBSSRSS() << "|"
      << setw(8) << _uniqueAccumMap.getHeapRSS() << "|"
      << setw(8) << _uniqueAccumMap.getStackRSS() << "|"
      << setw(8) << _uniqueAccumMap.getPR_SharedRSS() << "|"
      << setw(8) << (_totAccumMap.getPR_SharedRSS() - _uniqueAccumMap.getPR_SharedRSS()) << "|"
      << setw(8) << _uniqueAccumMap.getSR_SharedRSS() << "|"
      << setw(8) << (_totAccumMap.getSR_SharedRSS() - _uniqueAccumMap.getSR_SharedRSS()) << "|"
      << setw(8) << _uniqueAccumMap.getSW_SharedRSS() << "|"
      << setw(8) << (_totAccumMap.getSW_SharedRSS() - _uniqueAccumMap.getSW_SharedRSS()) << "|"
      << setw(8) << _uniqueAccumMap.getUnknownRSS();

  // print the tail
  out << setiosflags(ios::left) << "|"
      << setw(8) << _uniqueAccumMap.totalRSS() << "|" << setw(8) << _totAccumMap.totalRSS() << "|"
      << setw(8) << _statm.getNumDirtyPgs() << "|"
      << getCmdName() << "\n";

  return 0;
}

//------------------------------------------------------------------------------
// Print the process table of the RSS data with the separation on
// Unique and Shared segments and the Swappable summary.
//------------------------------------------------------------------------------
int ProcMap::table_print_U_S_Sw(ostream& out)
{
  // print the core data
  out << setiosflags(ios::left)
      << setw(8) << _uniqueAccumMap.getCodeRSS() << "|"
      << setw(8) << (_totAccumMap.getCodeRSS() - _uniqueAccumMap.getCodeRSS()) << "|"
      << setw(8) << _uniqueAccumMap.getDataRSS() << "|"
      << setw(8) << _uniqueAccumMap.getBSSRSS() << "|"
      << setw(8) << _uniqueAccumMap.getHeapRSS() << "|"
      << setw(8) << _uniqueAccumMap.getStackRSS() << "|"
      << setw(8) << _uniqueAccumMap.getPR_SharedRSS() << "|"
      << setw(8) << (_totAccumMap.getPR_SharedRSS() - _uniqueAccumMap.getPR_SharedRSS()) << "|"
      << setw(8) << _uniqueAccumMap.getSR_SharedRSS() << "|"
      << setw(8) << (_totAccumMap.getSR_SharedRSS() - _uniqueAccumMap.getSR_SharedRSS()) << "|"
      << setw(8) << _uniqueAccumMap.getSW_SharedRSS() << "|"
      << setw(8) << (_totAccumMap.getSW_SharedRSS() - _uniqueAccumMap.getSW_SharedRSS()) << "|"
      << setw(8) << _uniqueAccumMap.getUnknownRSS();

  // print the tail
  unsigned long ulSwappableU = _uniqueAccumMap.getCodeRSS() + _uniqueAccumMap.getPR_SharedRSS() +
    _uniqueAccumMap.getSR_SharedRSS();
  unsigned long ulSwappableU_S = _totAccumMap.getCodeRSS() + _totAccumMap.getPR_SharedRSS() +
    _totAccumMap.getSR_SharedRSS();

  out << setiosflags(ios::left) << "|"
      << setw(8) << _uniqueAccumMap.totalRSS() << "|" << setw(8) << _totAccumMap.totalRSS() << "|"
      << setw(8) << ulSwappableU << "|" << setw(8) << ulSwappableU_S << "|"
      << setw(8) << _totAccumMap.totalDP() << "|"
      << getCmdName() << "\n";

  return 0;
}

//------------------------------------------------------------------------------
// A couple of functions to output the headers of the tables
// They are put here so the users will not have to be concerned with these items
// All the functions are static so that they can be called from anywhere
//------------------------------------------------------------------------------
int ProcMap::table_head_printRSS(std::ostream& out)
{
  out << " Code   | Data   | Heap   | Stack  |PRShared|SRShared|SWShared| Other  | Total  |Dirty pg| Module name     \n";

  return 0;
}

int ProcMap::table_head_print_U_S(std::ostream& out)
{
  out << " Code   |        | Data   | BSS    | Heap   | Stack  |PRShared|        |SRShared|        |SWShared|        | Other  | Total  |        |Dirty pg| Module name     \n";
  out << " U      | S      | U      | U      | U      | U      | U      | S      | U      | S      | U      | S      | U      | U      | U+S    |        |\n";
  return 0;
}

int ProcMap::table_head_print_U_S_Sw(std::ostream& out)
{
  out << " Code   |        | Data   | BSS    | Heap   | Stack  |PRShared|        |SRShared|        |SWShared|        | Other  | Total  |        |Swappable|       |Dirty pg| Process name     \n";
  out << " U      | S      | U      | U      | U      | U      | U      | S      | U      | S      | U      | S      | U      | U      | U+S    | U      | U+S    |        |\n";
  return 0;
}

//------------------------------------------------------------------------------
//==============================================================================
//------------------------------------------------------------------------------
// KernProcMap - child of ProcMap specifically designed to work with kernel processes
//------------------------------------------------------------------------------

// Constructors & destructor
KernProcMap::KernProcMap()
  : ProcMap()
{
  // No implementation
}

KernProcMap::KernProcMap(int pid)
  : ProcMap(pid)
{
}

KernProcMap::KernProcMap(ParserModule *pmodule)
  : ProcMap(pmodule)
{
}

KernProcMap::~KernProcMap()
{
  // No implementation
}


//------------------------------------------------------------------------------
// Populate the header information for the specified kernel PID
//------------------------------------------------------------------------------
int KernProcMap::HeadPopulate(int pid)
{
  // do the work on the PID that was sent from above, if needed
  if (this->_pid != pid)
    this->_pid = pid;

  // Record the data from the stat file
  if (WorkStat() != 0)
    return 1;

  return 0;
}

//------------------------------------------------------------------------------
// Output the map information
//------------------------------------------------------------------------------
ostream& operator<<(std::ostream& out, KernProcMap& entry)
{
  entry.table_print(out);
  return out;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int KernProcMap::table_print(std::ostream& out)
{
  out << setiosflags(ios::left)
      << setw(8) << getPID() << "|"
      << setw(8) << getState() << "|"
      << getCmdName() << "\n";

  return 0;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// static header printing functions
int KernProcMap::table_head_print(std::ostream& out)
{
  out << " PID    | State  | CmdName \n";
  return 0;
}

//==============================================================================
