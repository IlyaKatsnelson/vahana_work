// Emacs type: -*- C++ -*-
/*
 * Driver program for maps_parser
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: main.cc,v 1.29 2007/05/03 00:06:10 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <fstream>

#include <cstring> // for strerror()
#include <cerrno>  // for errno
#include <cstdlib> // for realpath()
#include <sys/time.h>
#include <unistd.h>

#include <linux/limits.h>
#include <sys/types.h>
#include <dirent.h>
#include <assert.h>

#include "MapsEntry.h"
#include "AcumMapsEntry.h"
#include "ProcMap.h"
#include "MemInfo.h"
#include "SlabInfo.h"
#include "ParserModule.h"
#include "SystemInfo.h"
#include "main.h"

using namespace std;

// we could have used the Multimap instead of vector to decrease the search time
// However, I think that we don't have that many processes to justify the added complexity
typedef vector<ProcMap* > USysProcVector;
typedef vector<KernProcMap* > KSysProcVector;
typedef map<string, AcumMapsEntry> AcEntries;

// global system information object
SystemInfo hostSystemInfo;

// global timer interval
long interval = 0;

extern char *optarg;
extern int optind, opterr, optopt;

//------------------------------------------------------------------------------
// SIGALRM signal handler
//------------------------------------------------------------------------------
void
alarm_handler(int sig)
{
   signal(SIGALRM, alarm_handler);
   alarm(interval);
}

//------------------------------------------------------------------------------
// Print the help information.
//------------------------------------------------------------------------------
static void
usage(const string &progname)
{
  cout << "v." << MAP_VERSION_FULL << "\n";
  cout << "Usage: " << progname << " [-dr][-aluki] [ -p pid ] [ -o <filename> ]\n"
       << "\t[ interval {sec} [ count ]]\n"
       << "\t    -d - debug, stores the processed maps file the for verification\n"
       << "\t\t purposes.\n"
       << "\t    -r - debug, force  the  program to use  the RSS  data from some\n"
       << "\t\t other source instead of the kernel module.  The  source can be\n"
       << "\t\t either memmap files or patched maps files, if available.\n"
       << "\t    -a - accumulate memory usage for all processes.\n"
       << "\t    -l - output the memory information in the long form.\n"
       << "\t\t The memory for all processes separate will be printed  as well\n"
       << "\t\t as the accumulated values.\n"
       << "\t    -u - (deprecated) output  each  process'  unique  and  shared\n"
       << "\t\t module information.\n"
       << "\t    -k - output the list of the kernel processes.\n"
       << "\t    -i - output the dirty page statistics.\n"
       << "\t    -p - analyze only the process specified by pid.\n"
       << "\t    -o - store the results in the specified file. If several \n"
       << "\t\t measurements have to be taken, they will be  stored  in  files\n"
       << "\t\t with the same name with the measurement number as a suffix.\n"
       << "\t\t The program has to run with the root privileges in  order  for\n"
       << "\t\t it to show all the processes on the system.\n" << endl;

  cout << "\t\t The program uses the kernel module to find wich  processes are\n"
       << "\t\t LWPs. This method is much more  reliable  than the direct maps\n"
       << "\t\t files comparison." << endl;

  cout << "\t\t The program  also  uses  the heuristic method to find the text\n"
       << "\t\t memory that is  shared between different processes by the same\n"
       << "\t\t loaded libraries.For each module it calculates the maximum RSS\n"
       << "\t\t size, the sum of RSS sizes, and the maximum VM size (it cannot\n"
       << "\t\t be larger then the file size of the module).  Then the program\n"
       << "\t\t takes  the  best  possible  shared text segment size to be the\n"
       << "\t\t maximum  RSS  and  the  worst  to  be  the lowest  between the\n"
       << "\t\t accumulated RSS and the maximum VM size.\n";

  cout << "\t\t All the values are in bytes unless stated otherwise.\n";
}

//------------------------------------------------------------------------------
// Accumulate the memory information from all the read processes for the entire system
// We will use the child of MapsEntries for the final values
//------------------------------------------------------------------------------
static int
sys_accumulate(const USysProcVector &procs, AcEntries *accum)
{
  int cnt  = procs.size();

  // go through the vector and process each process' information
  ProcMap *curr;
  Entries::const_iterator iter;
  Entries::const_iterator end;
  for (int i=0;i<cnt;i++){
    curr = procs[i];
    assert(curr != NULL);

    // get the entries from the module map of the current process
    iter = curr->getMapC().begin();
    end = curr->getMapC().end();
    for (; iter != end; ++iter){
      (*accum)[iter->second.getName()].add(iter->second);
    }
  }
  return 0;
}

//------------------------------------------------------------------------------
// Set the unique/shared flags for all the modules in each process
//------------------------------------------------------------------------------
static int
proc_accumulate(USysProcVector &procs, const AcEntries &accumMap)
{
  ProcMap *curr;
  Entries::iterator m1;
  int cnt  = procs.size();

  // get the entries from the accumulated module map that are shared
  AcEntries::const_iterator iter = accumMap.begin();
  AcEntries::const_iterator end = accumMap.end();
  for (; iter != end; ++iter){
    if (iter->second.isUnique() == false){
      string &tmpName = iter->second.getName();
      // go through the vector and process each process' information
      for (int i=0;i<cnt;i++){
	curr = procs[i];
	assert(curr != NULL);

	// set the shared flag
	m1 = (curr->getMap())->find(tmpName);
	if (m1 != (curr->getMap())->end())
	  m1->second.setShared(true);
      }
    }
  }

  // go through the vector and process each process' maps
  for (int i=0;i<cnt;i++){
    curr = procs[i];
    assert(curr != NULL);
    curr->CollectMemory();
  }
  return 0;
}

//------------------------------------------------------------------------------
// Pretty print the final data
//------------------------------------------------------------------------------
int
print_accumulate(ostream& out, const USysProcVector &procs, AcEntries *accum, int flag)
{
  AcumMapsEntry totMap(" System Total");
  AcumMapsEntry sharedMap("Shared total");

  for (AcEntries::const_iterator iter = accum->begin(); iter != accum->end(); ++iter){
    // count the segments
    totMap += iter->second;

    if (iter->second.isUnique() == false){
      sharedMap += iter->second;
    }
  }

  // print the summary data
  // the Unique values are (total - shared)
  int cnt  = procs.size();
  out << " Num of processes: " << cnt << endl;
  AcumMapsEntry::table_head_print_line(out);
  out << " System executive summary (kB):\n";
  AcumMapsEntry::table_head_print_line(out);
  AcumMapsEntry::table_head_print_sum(out);
  AcumMapsEntry::table_head_print_line(out);

  MemInfo *system = new MemInfo();
  SlabInfo *slabs = new SlabInfo();
  // non-code unique size
  unsigned long CodeU = (totMap.getCodeMin() - sharedMap.getCodeMin());
  unsigned long NCodeURO = ((totMap.getPR_SharedRSS() - sharedMap.getPR_SharedRSS()) +
			    (totMap.getSR_SharedRSS() - sharedMap.getSR_SharedRSS()));
  unsigned long NCodeUWR = (totMap.getHeapRSS() + totMap.getStackRSS() + totMap.getDataRSS() +
			    (totMap.getSW_SharedRSS() - sharedMap.getSW_SharedRSS()) +
			    totMap.getUnknownRSS());
  unsigned long NSysTotB = CodeU + NCodeURO + NCodeUWR +
    (sharedMap.getCodeMin() + sharedMap.getPR_SharedRSS() +
     sharedMap.getSR_SharedRSS() + sharedMap.getSW_SharedRSS() +
     slabs->getTotAllocMem());
  unsigned long NSysTotW = CodeU + NCodeURO + NCodeUWR +
    (sharedMap.getCodeMax() + sharedMap.getPR_SharedVM() +
     sharedMap.getSR_SharedVM() + sharedMap.getSW_SharedVM() +
     slabs->getTotAllocMem());

  unsigned long vmCodeU = (totMap.getCodeSize() - sharedMap.getCodeSize());
  unsigned long vmNCodeURO = ((totMap.getPR_SharedSize() - sharedMap.getPR_SharedSize()) +
			      (totMap.getSR_SharedSize() - sharedMap.getSR_SharedSize()));
  unsigned long vmNCodeUWR = (totMap.getHeapSize() + totMap.getStackSize() + totMap.getDataSize() +
			      (totMap.getSW_SharedSize() - sharedMap.getSW_SharedSize()) +
			      totMap.getUnknownSize());
  unsigned long vmNSysTot = vmCodeU + vmNCodeURO + vmNCodeUWR +
    (sharedMap.getCodeSize() + sharedMap.getPR_SharedSize() +
     sharedMap.getSR_SharedSize() + sharedMap.getSW_SharedSize());

  out << setw(17) << NSysTotB << "| Best   |" << setw(8) << CodeU << "|"
      << setw(8) << NCodeURO << "|" << setw(8) << NCodeUWR << "|"
      << setw(8) << sharedMap.getCodeMin() << "|"
      << setw(8) << sharedMap.getPR_SharedRSS() << "|"
      << setw(8) << sharedMap.getSR_SharedRSS() << "|"
      << setw(8) << sharedMap.getSW_SharedRSS() << "|"
      << setw(8) << system->getCached() << "|" << setw(8) << slabs->getTotAllocMem()/1024 << "|"
      << setw(8) << system->getTotFree() <<"\n";
  out << setw(17) << NSysTotW << "| Worst  ||||"
      << setw(8) << sharedMap.getCodeMax() << "|"
      << setw(8) << sharedMap.getPR_SharedVM() << "|"
      << setw(8) << sharedMap.getSR_SharedVM() << "|"
      << setw(8) << sharedMap.getSW_SharedVM() << "|||" <<"\n";

  // print the VM sizes
  out << setw(17) << vmNSysTot << "| VM     |" << setw(8) << vmCodeU << "|"
      << setw(8) << vmNCodeURO << "|" << setw(8) << vmNCodeUWR << "|"
      << setw(8) << sharedMap.getCodeSize() << "|"
      << setw(8) << sharedMap.getPR_SharedSize() << "|"
      << setw(8) << sharedMap.getSR_SharedSize() << "|"
      << setw(8) << sharedMap.getSW_SharedSize() << "\n";

  //-------------------------------------------------------------------------------

  // print the table of all used modules
  AcumMapsEntry::table_head_print_line(out);
  out << " Total memory consumption by all the modules (kB)\n";
  AcumMapsEntry::table_head_print_line(out);
  if((flag & MAP_DIRTY_PGS) == 0){
    AcumMapsEntry::table_head_print(out);
  }else{
    AcumMapsEntry::table_head_print_DP(out);
  }
  AcumMapsEntry::table_head_print_line(out);

  if((flag & MAP_DIRTY_PGS) == 0){
    for (AcEntries::iterator iter = accum->begin(); iter != accum->end(); ++iter){
      (iter->second).table_print(out);
    }
  } else {
    for (AcEntries::iterator iter = accum->begin(); iter != accum->end(); ++iter){
      (iter->second).table_print_DP(out);
    }
  }

  AcumMapsEntry::table_head_print_line(out);
  out << sharedMap << "\n";

  // print the table of all processes
  AcumMapsEntry::table_head_print_line(out);
  out << " Total memory consumption unique by processes (kB)\n";
  AcumMapsEntry::table_head_print_line(out);
  ProcMap::table_head_print_U_S_Sw(out);
  AcumMapsEntry::table_head_print_line(out);

  // go through the vector and process each process' information
  for (int i=0;i<cnt;i++){
    procs[i]->table_print_U_S_Sw(out);
  }

  //-------------------------------------------------------------------------------
  // small useful utility - read the total system memeory info
  // and print it here also
  AcumMapsEntry::table_head_print_line(out);
  out << " System:\n";
  out << "\tSystem Total\t| " << system->getTotMem() <<"\n";
  out << "\tSystem used\t| " << system->getTotUsed() <<"\n";
  out << "\tSystem free\t| " << system->getTotFree() <<"\n";

  AcumMapsEntry::table_head_print_line(out);
  out << " User space:\n";
  out << setw(25) << "\tText Total [best,worst]\t| " << totMap.getCodeMin() << "|"
      << totMap.getCodeMax() <<"\n";
  out << setw(25) << "\tText Shared [best,worst]\t| " << sharedMap.getCodeMin() << "|"
      << sharedMap.getCodeMax() <<"\n";
  out << setw(25) << "\tTotal data \t| " << totMap.getDataRSS() <<"\n";
  out << setw(25) << "\tTotal heap \t| " << totMap.getHeapRSS() <<"\n";
  out << setw(25) << "\tTotal stack\t| " << totMap.getStackRSS() <<"\n";
  out << setw(25) << "\tTotal pshared [best,worst]\t| " << totMap.getPR_SharedRSS() << "|"
      << totMap.getPR_SharedVM() <<"\n";
  out << setw(25) << "\tTotal shared R [best,worst]\t| " << totMap.getSR_SharedRSS() << "|"
      << totMap.getSR_SharedVM() <<"\n";
  out << setw(25) << "\tTotal shared W [best,worst]\t| " << totMap.getSW_SharedRSS() << "|"
      << totMap.getSW_SharedVM() <<"\n";
  out << setw(25) << "\tTotal other\t| " << totMap.getUnknownRSS() <<"\n";

  AcumMapsEntry::table_head_print_line(out);
  out << " Kernel space:\n";
  out << "\tPage cache\t|" << system->getCached() <<"\n";
  out << "\tSlabs [active]\t|" << slabs->getTotAllocMem() << "|" << slabs->getTotActiveMem() << "\n";
  AcumMapsEntry::table_head_print_line(out);

  delete system;
  delete slabs;

  return 0;
}

//------------------------------------------------------------------------------
// Pretty print the kernel data
//------------------------------------------------------------------------------
int
printKProcs(ostream& out, const KSysProcVector &kprocs)
{
  // print the table of all used modules
  AcumMapsEntry::table_head_print_line(out);
  out << " Kernel processes in the system\n";
  AcumMapsEntry::table_head_print_line(out);
  KernProcMap::table_head_print(out);
  AcumMapsEntry::table_head_print_line(out);

  int cnt  = kprocs.size();
  for (int i=0;i<cnt;i++){
    kprocs[i]->table_print(out);
  }
  AcumMapsEntry::table_head_print_line(out);

  return 0;
}

//------------------------------------------------------------------------------
// Find if we have already read one process with the same pid, and grid
// This function finds if the passed process is LWP and adds it to the main
// process' LWP list.
// The function returns 0 if the supplied pid is not LWP
//  1 - if the pid is LWP
// -1 - if an error occured
//------------------------------------------------------------------------------
int
addLWPProc(const USysProcVector &procs, const int lwp_pid,
	  ParserModule &kmodule, int flag)
{
  // stupid test - what if the vector is empty
  if (procs.empty() == true)
    return -1;

  // get the root process for this thread
  pid_t pid_root = kmodule.readMainPID(lwp_pid);
  if (pid_root == lwp_pid){
    return 0;
  }else{
    for (int i=(procs.size()-1);i>=0;i--){
      if (procs[i]->getPID() == pid_root){
	procs[i]->addLWP(lwp_pid);
	break;
      }
    }
  }

  // if we got here the PID is LWP.
  return 1;
}

//------------------------------------------------------------------------------
// Utility function that dumps the current time
//------------------------------------------------------------------------------
inline void
outputTime(ostream& out)
{
  struct timeval tv1;
  struct timezone tz;
  gettimeofday(&tv1, &tz);

  char prev = out.fill ('0');
  out << " " << tv1.tv_sec << "." << setiosflags(ios::right) << setw(6) << tv1.tv_usec;
  out.fill(prev);
}

//------------------------------------------------------------------------------
// Utility function that creates the file output stream for this iteration
// The function opens the output fstream, it is the job of the caller to
// make sure that the stream is closed.
//------------------------------------------------------------------------------
static int
makeIterFile(int iteration, ofstream& outfile, const char *out_fname)
{
  if (out_fname == NULL)
    return 0;

  if (outfile.is_open()){
#ifdef DEBUG
    cerr << "File was left open.\n";
#endif
    outfile.close();
  }

  if (iteration == 0){
    outfile.open(out_fname, ios::out|ios::trunc);
    if (outfile.fail() == true)
      cerr << "Error opening file: " << out_fname << endl;

  } else {
    char *fname = (char *)malloc(strlen(out_fname)+10);
    sprintf(fname, "%s.%d", out_fname, iteration);
    outfile.open(fname, ios::out|ios::trunc);
    if (outfile.fail() == true)
      cerr << "Error opening file: " << fname << endl;

    free(fname);
  }

  return 0;
}

//------------------------------------------------------------------------------
// store the maps file for a process (for debugging purposes).
// The storage is in the current running directory;
// no errors are reported
//------------------------------------------------------------------------------
static void
storeFile(char *name, int cnt)
{
  char command[PATH_MAX*2];
  time_t timep = time(NULL);
  struct tm *ltm = localtime(&timep);
  sprintf(command, "cp -f /proc/%s ./%s_%d-%d-%d_%02dh%02dm%02ds_%d",
                  name, name, ltm->tm_mon, ltm->tm_mday, 1900+ltm->tm_year,
                  ltm->tm_hour, ltm->tm_min, ltm->tm_sec, cnt);
  system(command);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int
main(int argc, char *argv[])
{
  int flag = 0;
  int debug = 0;
  int opt;
  int measure_pid = -1;
  int measure_count = 0;
  int iteration = 0;
  char* out_fname = NULL;
  ofstream out_fstream;

  /* Parse any options */
  if (argc == 1){
    usage(argv[0]);
    return 0;
  }

  while ((opt=getopt(argc, argv, "alhvukidrp:o:")) != -1) {
    switch(opt) {
    case 'a':
      flag |= MAP_ACCUMULATE;
      break;
    case 'l':
      flag |= MAP_LONG_OUT;
      break;
    case 'u':
      // Nothing to do here - this flag is ignored
      // flag |= MAP_UNIQUE_OUT;
      break;
    case 'r':
      flag |= MAP_RSS_OVR;
      break;
    case 'k':
      flag |= MAP_KERNEL_P_OUT;
      break;
    case 'i':
      flag |= MAP_DIRTY_PGS;
      break;
    case 'p':
      measure_pid = atoi(optarg);
      break;
    case 'o':
      out_fname = optarg;
      break;
    case 'd':
      debug = 1;
      break;
    case 'v':
    case 'h':
      usage(argv[0]);
      return 0;
      break;
    default:
      usage(argv[0]);
      return 0;
    }
  }

  // check the tail of the input after the options
  // if there was no tail in the first place - just do one sample
  if (optind < argc){
    interval = atol(argv[optind]);
    if (optind+1 < argc)
      measure_count = atol(argv[optind+1]);
  }

  // just for fun we check how long it took to run this
  struct timeval tv1, tv2;
  struct timezone tz;
  gettimeofday(&tv1, &tz);

  // --------------------------------------------------
  // Init module
  ParserModule kmodule(false);
  if(kmodule.loadModule() != 0){
    cerr << "Cannot load kernel module " << kmodule.getKModuleName()
        << ". " << strerror(errno) << "\n";
    return 1;
  }

  // --------------------------------------------------
  // main measure loop
  ofstream& out_stream = ((out_fname != NULL) ? out_fstream : (ofstream &)cout);
  do {

    // Fire up the alarm
    alarm_handler(0);

    // configure the output destination
    iteration = (measure_count == 0 ? 0 : (iteration+1));
    makeIterFile(iteration, out_fstream, out_fname);

    hostSystemInfo.pretty_print(out_stream);

    // if the pid is specified, then we are dealing with one process information
    // if not, then we have to loop through all of them
    if (measure_pid != -1){
      int pgid;

      // see if we are dealing with the kernel process.
      if (((pgid = getpgid(measure_pid)) == 1) || (pgid == 0)){
        cerr << "Cannot process kernel process: " << measure_pid << endl;
        return 1;
      }

      // see if the target process actually exists on thsi iteration
      if (kill(measure_pid,0) == -1 && errno == ESRCH){
        cerr << "The process with PID=" << measure_pid << " does not exist.\n";
        break;
      }

      ProcMap onemap(&kmodule);

      if (onemap.Populate(measure_pid, flag & MAP_RSS_OVR) != 0){
        cerr << onemap.getErrorMsg() << endl;
        return 1;
      }
      onemap.CollectMemory();
      onemap.table_print(out_stream, flag);

      // do the debugging, if required
      if (debug == 1)
        onemap.StoreDebug();
    } else {
      DIR *procDir;
      struct dirent *entry;
      int cnt, pgid, pid;

      // vector (array) of the ProcMaps with some initial number of elements
      USysProcVector pArr(0);
      KSysProcVector kArr(0);
      ProcMap *manymap;
      KernProcMap *kmap;

      if ((procDir = opendir("/proc")) == NULL){
        cerr << "Can't open /proc directory: " + string(strerror(errno)) + "\n";
        return 1;
      }

      // read the data for all the processes in the /proc directory
      while ((entry = readdir(procDir)) != NULL) {
        pid = atoi(entry->d_name);
        if (pid == 0)
          continue;

        // see if we are dealing with the kernel process.
        // Kernel processes belong to group 0 or 1, or has memory structure at 0x0 address.
        pgid = getpgid(pid);
        if ((pgid == 1) || (pgid == 0) || (kmodule.isKProcess(pid) == true)){
          if ((flag & MAP_KERNEL_P_OUT) != 0){
            kmap = new KernProcMap(&kmodule);
            kmap->HeadPopulate(pid);
            kArr.push_back(kmap);
          }
          continue;
        }

        // verify what we are dealing with. - LWP or kernel process
        if (addLWPProc(pArr, pid, kmodule, 0) == 1)
          continue;

        // we found a directory entry with a digit in its name
        // that may be a process
        manymap = new ProcMap(&kmodule);

        // for this part we will use a slightly different algorithm then for a single process
        // a. get the head information - cmd line, ppid, & grid
        if (manymap->HeadPopulate(pid) != 0 ) {
          cerr << manymap->getErrorMsg() << endl;
          delete manymap;
          continue;
        } else {

        // b. process the maps files
        if (manymap->MapsPopulate( pid, flag & MAP_RSS_OVR ) != 0 ) {
          cerr << manymap->getErrorMsg() << endl;
          delete manymap;
          continue;
        }

          pArr.push_back(manymap);

          // do the debugging, if required
          if (debug == 1)
            manymap->StoreDebug();
        }
      }

      closedir(procDir);

      // do the accumulation part
      AcEntries acum;
      sys_accumulate(pArr, &acum);
      proc_accumulate(pArr, acum);

      // print the info
      print_accumulate(out_stream, pArr, &acum, flag);

      if (debug == 1){
        storeFile("slabinfo",1);
        storeFile("meminfo",1);
      }

      // if the user requested the kernel information, print it here
      if ((flag & MAP_KERNEL_P_OUT) != 0)
        printKProcs(out_stream, kArr);

      // if the user requested the details, print them here
      cnt = pArr.size();
      if ((flag & MAP_LONG_OUT) != 0){
        for (int i=0;i<cnt;i++){
          pArr[i]->table_print(out_stream, flag);
        }
      }

      // clean up the allocated objects
      for (int i=0;i<cnt;i++){
        delete pArr[i];
      }
      if ((flag & MAP_KERNEL_P_OUT) != 0){
        cnt = kArr.size();
        for (int i=0;i<cnt;i++)
          delete kArr[i];
      }
    }

    if (out_fname != NULL){
      out_fstream.close();
    }
    if (--measure_count > 0)
      pause();
  } while(measure_count > 0);
  // end of main measure loop
  // --------------------------------------------------

  // just for fun we check how long it took to run this
  gettimeofday(&tv2, &tz);
  cout << " Running time: ";
  if (tv2.tv_sec == tv1.tv_sec)
    cout << "0." << setiosflags(ios::right) << setw(6) << setfill('0') << dec << (tv2.tv_usec-tv1.tv_usec);
  else{
    if ((USEC-tv1.tv_usec)+tv2.tv_usec < USEC){
      cout << (tv2.tv_sec-tv1.tv_sec - 1) << "."
          << setiosflags(ios::right) << setw(6) << setfill('0')
          << ((USEC-tv1.tv_usec)+tv2.tv_usec);
    } else {
      cout << (tv2.tv_sec-tv1.tv_sec) << "."
          << setiosflags(ios::right) << setw(6) << setfill('0')
          << ((USEC-tv1.tv_usec)+tv2.tv_usec - USEC);
    }
  }
  cout << " sec." << endl;

  return 0;
}

//------------------------------------------------------------------------------

