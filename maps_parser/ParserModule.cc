// Emacs type:  -*- C++ -*-
/*
 * Implementation file for ParserModule class
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: ParserModule.cc,v 1.15 2007/05/23 22:00:07 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#include "main.h"
#include "ParserModule.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

#include <cctype>
#include <cstring>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

int ParserModule::iLoadCnt = 0;
char ParserModule::_kmodule_ver[64] = "Not Supported";
const char *ParserModule::kmodule_name = "parser_module";

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Constructors & destructor

ParserModule::ParserModule(bool do_load)
  : lStatus(0),
    bModuleLoaded(false),
    bDoUnLoad(false),
    kmodule_file(NULL)
{
  bModuleLoaded = (getLoadCnt() > 0 ? true : false);
  if (do_load == true)
    lStatus = loadModule();
}

ParserModule::ParserModule(const ParserModule &entry)
  : lStatus(entry.lStatus),
    bModuleLoaded(entry.bModuleLoaded),
    bDoUnLoad(entry.bDoUnLoad),
    kmodule_file(entry.kmodule_file)
{
}

ParserModule::~ParserModule()
{
  // close the module communication file
  if (getKModuleFile() != NULL){
    fclose(getKModuleFile());
    kmodule_file = NULL;
  }
  // unload module only if we loaded it
  loadDecr();
  if (getDoUnLoad() == 1 && getLoadCnt() == 0)
    unloadModule();
}

//------------------------------------------------------------------------------
// Load the kernel module required for this class to work.
// Before doing anything the method checks if the module is already loaded
long
ParserModule::loadModule()
{
  char command[128];
  char module_loc[128] = "";
  int ret;

  if (isModuleLoaded() != 1){
    bModuleLoaded = false;
    bDoUnLoad = true;

    // try to find the module file
    struct stat buf;
    sprintf(command, "%s.ko", kmodule_name);
    ret = stat(command, &buf);
    if (ret == -1){
      sprintf(command, "/lib/modules/%s.ko", kmodule_name);
      ret = stat(command, &buf);
      if (ret == -1){
	return -1;
      } else {
	strcpy(module_loc, "/lib/modules/");
      }
    }

#ifdef HOST
    sprintf(command, "sudo -u root insmod %s%s.ko", module_loc, kmodule_name);
    ret = system(command);
    if(WEXITSTATUS(ret) == 0)
      bModuleLoaded = true;
    else
      return -1;

#else
    sprintf(command, "insmod %s%s.ko", module_loc, kmodule_name);

    ret = system(command);
    if(WEXITSTATUS(ret) == 0)
      bModuleLoaded = true;
    else {
      // let's try again with -f
      sprintf(command, "insmod -f %s%s.ko", module_loc, kmodule_name);
      ret = system(command);
      if(WEXITSTATUS(ret) == 0)
	bModuleLoaded = true;
      else
	return -1;

    }
#endif

  }

  // store the kmodule file handle so we don't have to open+close every time
  // Use the plain old stdlib functions.
  // For some reason they work better then fstream
  sprintf(command, "/proc/%s", kmodule_name);
  kmodule_file = fopen(command, "r+");
  if (kmodule_file == NULL){
    fprintf(stderr, "Error openning module file %s %d\n", command, errno);
    return -1;
  }

  // get the module version, if supported
  if (getLoadCnt() == 0){
    if (readModuleVer(_kmodule_ver, 63) <= 0){
      strcpy(_kmodule_ver, "Not supported");
    }
  }

  loadIncr();
  return 0;
}

//------------------------------------------------------------------------------
// UnLoad the kernel module
// Before doing anything the method checks if the module is already loaded
long
ParserModule::unloadModule()
{
  if (kmodule_file != NULL)
    fclose(kmodule_file);

  if (isModuleLoaded() != 1)
    return lStatus;

  char command[30];
  int ret;

#ifdef HOST
  sprintf(command, "sudo -u root rmmod %s.ko", kmodule_name);
#else
  sprintf(command, "rmmod %s.ko", kmodule_name);
#endif

  ret = system(command);

  if(WEXITSTATUS(ret) == 0)
    bModuleLoaded = 0;

  return WEXITSTATUS(ret);
}

//------------------------------------------------------------------------------
// Before doing anything - check if the module is already loaded
//
// nfsd                   83664   8 (autoclean)
long
ParserModule::isModuleLoaded()
{
  int loaded_flag = 0;

  if (getModuleLoaded() == true)
    return 1;

  ifstream modulesfile;
  modulesfile.open("/proc/modules");
  if ((modulesfile.rdstate() & ifstream::failbit ) != 0 ) {
    lStatus = modulesfile.rdstate();
    return lStatus;
  }

  // get the module line by line until the end of the file or we find ours
  string proc_module_name;
  const basic_string <char>::size_type npos = (size_t)-1;

  do{
    modulesfile >> proc_module_name;

    if (proc_module_name.find(kmodule_name) != npos){
      loaded_flag = 1;
      bModuleLoaded = 1;
      break;
    }

    // Note: at this point we just skip the rest of the line
    modulesfile.ignore(256,'\n');

  } while (modulesfile.eof() != true);

  modulesfile.close();

  return loaded_flag;
}

//------------------------------------------------------------------------------
// Kernel module communication functions
//------------------------------------------------------------------------------
// This method will check if the passed PID belongs to an LWP (thread) or a process
int
ParserModule::isLWP(pid_t pid)
{
  int ret = 0;
  if (isModuleLoaded() == 0)
    return 0;

  // Use the plain old stdlib functions.
  // For some reason they work better then fstream
  fprintf(getKModuleFile(), "LWP %d;\n", pid);
  //  fflush(getKModuleFile());
  fscanf(getKModuleFile(),"%d",&ret);
  if (ret < 0){
    char tmp_char, buf[128];
    fscanf(getKModuleFile(),"%c %127c",&tmp_char,buf);
    fprintf(stderr, "isLWP(%d):Error from kernel module: %s\n", pid, buf);
    return 0;
  }

  return ret;
}

//------------------------------------------------------------------------------
// This method will check if the passed PID belongs to a kernel process
int
ParserModule::isKProcess(pid_t pid)
{
  int ret = 0;
  if (isModuleLoaded() == 0)
    return 0;

  // Use the plain old stdlib functions.
  // For some reason they work better then fstream
  fprintf(getKModuleFile(), "KPROC %d;\n", pid);
  //  fflush(getKModuleFile());
  fscanf(getKModuleFile(),"%d",&ret);
  if (ret < 0){
    char tmp_char, buf[128];
    fscanf(getKModuleFile(),"%c %127c",&tmp_char,buf);
    fprintf(stderr, "isKProcess(%d):Error from kernel module: %s\n", pid, buf);
    return 0;
  }

  return ret;
}

//------------------------------------------------------------------------------
// This method will read the PID of the main process that the passed PID belongs to
// -1 is returned in the case of error
pid_t
ParserModule::readMainPID(pid_t pid)
{
  pid_t main_pid = -1;
  if (isModuleLoaded() == 0)
    return main_pid;

  fprintf(getKModuleFile(), "MAIN_PID %d;\n", pid);
  //  fflush(getKModuleFile());
  fscanf(getKModuleFile(),"%d",&main_pid);
  if (main_pid < 0){
    char tmp_char, buf[128];
    fscanf(getKModuleFile(),"%c %127c",&tmp_char,buf);
    fprintf(stderr, "readMainPID(%d):Error from kernel module: %s\n", pid, buf);
    return main_pid;
  }

  return main_pid;
}

//------------------------------------------------------------------------------
// This method will read the information about the stack if the process in question.
// The stack info is returned as two ulongs start and end separated by space.
// Note that the kernel module returns a map of stacks rather than one pair
// of numbers. However, this function will only read the first two.
int
ParserModule::readStackInfo(pid_t pid, unsigned long *vm_start, unsigned long *vm_end)
{
  int result = -1;

  if (isModuleLoaded() == 0)
    return result;

  result = fprintf(getKModuleFile(), "STACK_MAP %d;\n", pid);
  result = fflush(getKModuleFile());
  if (fscanf(getKModuleFile(),"%lX %lX",vm_start,vm_end) < 2){
    char tmp_char, buf[128];
    fscanf(getKModuleFile(),"%c %127c",&tmp_char,buf);
    fprintf(stderr, "readStackInfo(%d):Error from kernel module: %s\n", pid, buf);
    return result;
  }

  return 0;
}

//------------------------------------------------------------------------------
// This method will read the information about the stack of the process in question.
// The stack info is returned as a map of pairs of VM start and stop addresses.
int
ParserModule::readStackMap(pid_t pid, char *map, size_t msize)
{
  int result = -1;

  if (isModuleLoaded() == 0)
    return result;

  result = fprintf(getKModuleFile(), "STACK_MAP %d;\n", pid);
  result = fflush(getKModuleFile());
  result = fread(map, sizeof(char), msize-1, getKModuleFile());

  return result;
}

//------------------------------------------------------------------------------
// This method will read the information about the stack of the process in question.
// The stack info is returned as a vector of start and end addresses of the stack segments
int
ParserModule::readStackMapVector(pid_t pid, vector<unsigned long > &v)
{
  char *buf;
  char *tokptr;
  int status = -1;
  unsigned int buf_size = sysconf(_SC_PAGESIZE);

  v.clear();
  buf = (char *)malloc(buf_size);
  if (buf == NULL){
    fprintf(stderr,"pid=%d.cannot allocate data buffer.\n",pid);
    return -1;
  }

  status = this->readStackMap(pid, buf, buf_size);
  if (status < 0){
    fprintf(stderr,"Error reading Stack map: %d. PID: %d.\n", pid, status);
    free(buf);
    return status;
  } else if (status == 0) {
    free(buf);
    return status;
  } else if (buf[status-1] != (char)EOF){
    fprintf(stderr,"Error reading Stack map: %d. %d. Too many stack modules don't fit into one "
	    "page. Please save the /proc/%d/maps file and contact the program designer.\n",
	    status, pid, pid);
  }

  // The map came in the form: [start end\n]
  buf[status-1] = 0;
  tokptr = strtok(buf," \n");
  while (tokptr != NULL)
  {
    v.push_back(strtoul(tokptr, NULL, 16));
    tokptr = strtok (NULL, " \n");
  }

  free(buf);
  return 0;
}

//------------------------------------------------------------------------------
// This method will read the memory map of the process that the passed PID belongs to.
// The map is returned in the char array dynamically allocated for this method.
// The method returns the size of the map array. Negative value is returned
// in the case of error.
int
ParserModule::readMemMap(pid_t pid, char *map, size_t msize)
{
  int result = -1;

  if (isModuleLoaded() == 0)
    return result;

  result = fprintf(getKModuleFile(), "MEM_CNT_MAP %d;\n", pid);
  result = fflush(getKModuleFile());
  result = fread(map, sizeof(char), msize-1, getKModuleFile());

  return result;
}

//------------------------------------------------------------------------------
// This method will read the flag map of the process that the passed PID belongs to.
// The map is returned in the char array dynamically allocated for this method.
// The method returns the size of the map array. Negative value is returned
// in the case of error.
int
ParserModule::readFlagMap(pid_t pid, char *map, size_t msize)
{
  int result = -1;

  if (isModuleLoaded() == 0)
    return result;

  result = fprintf(getKModuleFile(), "MEM_FLAG_MAP %d;\n", pid);
  result = fflush(getKModuleFile());
  result = fread(map, sizeof(char), msize-1, getKModuleFile());

  return result;
}

//------------------------------------------------------------------------------
// This method will read the RSS map of the process that the passed PID belongs to.
// The map is returned in the char array dynamically allocated for this method.
// The method returns the size of the map array. Negative value is returned
// in the case of error.
int
ParserModule::readRSSMap(pid_t pid, char *map, size_t msize)
{
  int result = -1;

  if (isModuleLoaded() == 0)
    return result;

  result = fprintf(getKModuleFile(), "MEM_RSS_MAP %d;\n", pid);
  result = fflush(getKModuleFile());
  result = fread(map, sizeof(char), msize-1, getKModuleFile());

  return result;
}

//------------------------------------------------------------------------------
// This method will read the Dirty map of the process that the passed PID belongs to.
// The map is returned in the char array dynamically allocated for this method.
// The method returns the size of the map array. Negative value is returned
// in the case of error.
int
ParserModule::readDirtyMap(pid_t pid, char *map, size_t msize)
{
  int result = -1;

  if (isModuleLoaded() == 0)
    return result;

  result = fprintf(getKModuleFile(), "MEM_DIRTY_MAP %d;\n", pid);
  result = fflush(getKModuleFile());
  result = fread(map, sizeof(char), msize-1, getKModuleFile());

  return result;
}

//------------------------------------------------------------------------------
// This method will read the information about the memory segment of the process in question.
// The stack info is returned as buffer of the following fields:
//     mm->start_code, mm->end_code, mm->start_data, mm->end_data
//     mm->start_brk, mm->brk, mm->start_stack
//     mm->arg_start, mm->arg_end, mm->env_start, mm->env_end
//     mm->rss, mm->total_vm, mm->locked_vm

int
ParserModule::readMemSegmentMap(pid_t pid, char *map, size_t msize)
{
  int result = -1;

  if (isModuleLoaded() == 0)
    return result;

  result = fprintf(getKModuleFile(), "MEM_SEGMENT_MAP %d;\n", pid);
  result = fflush(getKModuleFile());
  result = fread(map, sizeof(char), msize-1, getKModuleFile());

  return result;
}

//------------------------------------------------------------------------------
// This is a low level method that will read the specified number of bites from
// the module to the supplied buffer.
// It is assumed that the user supplied buffer large enough to hold the data
int
ParserModule::readBuf(char *buf, size_t msize)
{
  int result = -1;

  if (isModuleLoaded() == 0)
    return result;

  result = fread(buf, sizeof(char), msize-1, getKModuleFile());
  if (result >= 0)
    buf[result-1] = 0;

  return result;
}

//------------------------------------------------------------------------------
// This method will try to parse the supplied buffer for the sign of an error
// returned from the module.
int
ParserModule::readModError(char *buf, char *err_buf, size_t bsize)
{
  int result;
  char *endptr;

  // The error should be in the form: -##, Description
  result = strtol(buf, &endptr, 10);
  if (endptr != buf && err_buf != NULL){
    strncpy(err_buf, endptr+1, bsize);
  }

  return result;
}

//------------------------------------------------------------------------------
// This method will get the string of RSS values from the module
// then it will parse it and put into the supplied vector
int
ParserModule::readRSSMapVector(pid_t pid, vector<unsigned long > &v)
{
  char *buf;
  char *tokptr;
  int status = -1;
  unsigned int buf_size = sysconf(_SC_PAGESIZE);

  v.clear();
  buf = (char *)malloc(buf_size);
  if (buf == NULL){
    fprintf(stderr,"pid=%d.cannot allocate data buffer.\n",pid);
    return -1;
  }

  status = this->readRSSMap(pid, buf, buf_size);
  if (status < 0){
    fprintf(stderr,"Error reading RSS map: %d. PID: %d.\n", pid, status);
    free(buf);
    return status;
  } else if (status == 0) {
    free(buf);
    return status;
  } else if (buf[status-1] != (char)EOF){
    fprintf(stderr,"Error reading RSS map: %d. %d. Too many modules don't fit into one "
	    "page. Please save the /proc/%d/maps file and contact the program designer.\n",
	    status, pid, pid);
  }

  buf[status-1] = 0;
  tokptr = strtok(buf," ");
  while (tokptr != NULL)
  {
    v.push_back(atol(tokptr));
    tokptr = strtok (NULL, " ");
  }

  free(buf);
  return 0;
}

//------------------------------------------------------------------------------
// This method will get the string of Dirty page counters from the module
// then it will parse it and put into the supplied vector
int
ParserModule::readDirtyMapVector(pid_t pid, vector<unsigned long > &v)
{
  char *buf;
  char *tokptr;
  int status = -1;
  unsigned int buf_size = sysconf(_SC_PAGESIZE);

  v.clear();
  buf = (char *)malloc(buf_size);
  if (buf == NULL){
    fprintf(stderr,"pid=%d.cannot allocate data buffer.\n",pid);
    return -1;
  }

  status = this->readDirtyMap(pid, buf, buf_size);
  if (status < 0){
    fprintf(stderr,"Error reading Dirty map: %d. PID: %d.\n", pid, status);
    free(buf);
    return status;
  } else if (status == 0) {
    free(buf);
    return status;
  } else if (buf[status-1] != (char)EOF){
    fprintf(stderr,"Error reading Dirty map: %d. %d. Too many modules don't fit into one "
	    "page. Please save the /proc/%d/maps file and contact the program designer.\n",
	    status, pid, pid);
  }

  buf[status-1] = 0;
  tokptr = strtok(buf," ");
  while (tokptr != NULL)
  {
    v.push_back(atol(tokptr));
    tokptr = strtok (NULL, " ");
  }

  free(buf);
  return 0;
}

//------------------------------------------------------------------------------
// This method will read the version of the kernel module we are working with
int
ParserModule::readModuleVer(char *ver_str, const size_t msize)
{
  int result = -1;

  if (isModuleLoaded() == 0)
    return result;

  result = fprintf(getKModuleFile(), "VER;\n");
  result = fflush(getKModuleFile());
  result = fread(ver_str, sizeof(char), msize-1, getKModuleFile());

  return result;
}

//==============================================================================



