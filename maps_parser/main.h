// Emacs type: -*- C++ -*-
/*
 * Header file with some global definitions
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: main.h,v 1.12 2007/05/23 22:00:07 w30205 Exp $
 * -----------------------------------------------------------------------------
 */


#ifndef __MAIN_H__
#define __MAIN_H__

#include <iosfwd>
#include <string>

// Global constants
#define MAP_ACCUMULATE         1   // accumulate the memory values for all processes
#define MAP_LONG_OUT           2   // print the output in the long form
#define MAP_MAPS_LWP           4   // use the maps files for LWP check
#define MAP_MAPS_KM            8   // use the kernel module for LWP check
#define MAP_UNIQUE_OUT        16   // print the module unique information
#define MAP_RSS_OVR           32   // overwrite the default RSS data source
#define MAP_KERNEL_P_OUT      64   // Output the list of the kernel processes
#define MAP_DIRTY_PGS        128   // output the dirty pages statistics
#define MAP_SINGLE             0   // just do one process

// -----------------------------------------------------
#define GENERAL_STR            "General"
#define USEC                   1000000
#define MAP_VERSION            "2.6.5"
#define MAP_VERSION_FULL       MAP_VERSION " ["__DATE__ " "__TIME__"]"

#define PROC_PATH_MAX          32
#define NO_SHR_SEGMENTS             // if defined the shared segments comparison is not performed.

#define DFLT_START_STACK       0xb0000000
#define KB(x)                  (x >> 10)

// - Status and Error codes ----------------------------
#define MAP_ERR_OK             0
#define MAP_ERR_INVALID_PID   -1

// -----------------------------------------------------

#endif // __MAIN_H__
