// Emacs type: -*- C++ -*-
/*
 * Header file for the SlabInfo class
 *
 * This class abstracts data manipulation from the meminfo file
 *
 * Copyright (C) 2005, 2006 Motorola Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * -----------------------------------------------------------------------------
 * $Id: SlabInfo.h,v 1.2 2006/09/25 22:37:33 w30205 Exp $
 * -----------------------------------------------------------------------------
 */

#ifndef __SLAB_INFO_H__
#define __SLAB_INFO_H__

#include <iosfwd>
#include <string>

//------------------------------------------------------------------------------
class SlabInfo
{
protected:
  unsigned long ulTotCaches;
  unsigned long ulTotActiveMem;
  unsigned long ulTotAllocMem;
  long lStatus;

public:
  // C'tors & D'tor
  SlabInfo();
  SlabInfo(const SlabInfo &entry);

  ~SlabInfo();

  // Accessor methods
  inline unsigned long getTotCaches(void) const { return ulTotCaches; };
  inline unsigned long getTotActiveMem(void) const { return ulTotActiveMem; };
  inline unsigned long getTotAllocMem(void) const { return ulTotAllocMem; };
  inline long getStatus(void) const { return lStatus; };

  long update(void);
};

#endif // __SLAB_INFO_H__

//==============================================================================

