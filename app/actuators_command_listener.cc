/*
 * actuators_command_listener.cc
 *
 *  Copyright (C) 2016-2017 A^3 - All Rights Reserved
 *
 *  Listener class to receive the DDS data on the VehicleActuatorsCommand topic
 */

#include <iostream>

#include "actuators_proxy/actuators_command_listener.h"

ActuatorsCommandListener::ActuatorsCommandListener() {}

ActuatorsCommandListener::~ActuatorsCommandListener() {}

/**
 * Handle the DDS_DATA_AVAILABLE_STATUS message indicating that
 * one or more new data samples have been received.
 */
void ActuatorsCommandListener::on_data_available(DDSDataReader* reader) {
  ActuatorsCommand::DataReader *data_reader = nullptr;
  ActuatorsCommand::Seq data_seq;
  DDS_SampleInfoSeq info_seq;

  data_reader = ActuatorsCommand::DataReader::narrow(reader);
  if (data_reader == nullptr) {
    std::cerr << "ActuatorsCommand::DataReader::narrow" << std::endl;
    return;
  }

  auto retcode = data_reader->take(
        data_seq, info_seq, DDS_LENGTH_UNLIMITED,
        DDS_ANY_SAMPLE_STATE, DDS_ANY_VIEW_STATE, DDS_ANY_INSTANCE_STATE);
  if (retcode == DDS_RETCODE_NO_DATA) {
    return;
  } else if (retcode != DDS_RETCODE_OK) {
    std::cerr << "take error: " << retcode << std::endl;
    return;
  }

  for (auto i = 0; i < data_seq.length(); ++i) {
    if (info_seq[i].valid_data) {
      std::cout << "Received data" << std::endl;

      // TODO(Ilya) Plug the HW attachment code here instead of just printing!
      ActuatorsCommand::TypeSupport::print_data(&data_seq[i]);
    }
  }
  retcode = data_reader->return_loan(data_seq, info_seq);
  if (retcode != DDS_RETCODE_OK) {
    std::cerr << "return loan error: " << retcode << std::endl;
  }
}
