// Copyright (C) 2017 A^3 - All Rights Reserved

#include <chrono>
#include <vector>

#include "apps/sim_actuators_proxy/sim_bus_worker.h"
#include "apps/sim_actuators_proxy/configuration.h"
#include "apps/sim_actuators_proxy/comm_models/sim_vpf/sim_vpf.h"

#include "gmock/gmock.h"

#include "unittest/lib/mock_objects/mock_serial.h"
#include "vahana_lib/utils/range_scale.h"

using ::testing::Return;
using ::testing::_;
using ::testing::ElementsAreArray;
using ::testing::WithArgs;
using ::testing::SetArrayArgument;
using ::testing::Invoke;
using ::testing::InSequence;

using namespace std::literals;  // NOLINT

namespace sim_bus_worker_test {

//-----------------------------------------------------------------------------
// Test Fixture
//-----------------------------------------------------------------------------
class SimBusWorkerTest : public ::testing::Test {
  static const unsigned kCommandSize = 12;
  static const unsigned kResponseSize = 22;
  static const unsigned kVpfCnt = 8;

  std::unique_ptr<sim_actuators_proxy::SimBusWorker> worker_;

  MockSerialInterface serial_;

 public:
  SimBusWorkerTest() {
    worker_ = std::make_unique<sim_actuators_proxy::SimBusWorker>("-test-",
                                                                  &command_);
  }

  void SetUp() {
  }
  void TearDown() {
  }

  // Set the expected mock function calls. They have to be done in proper
  // sequence on this serial
  void mock(std::vector<uint8_t>&& index) {
    InSequence s;

    for (auto & i : index) {
      EXPECT_CALL(serial_, Write(_, _))
          .With(ElementsAreArray(expected_command_[i]))
          .WillRepeatedly(Invoke(Write));

      EXPECT_CALL(serial_, Read(_, _))
          .Times(3)
          .WillOnce(DoAll(SetArrayArgument<0>(response_[i],
                                              response_[i] + 1),
                    Return(1)))
          .WillOnce(DoAll(SetArrayArgument<0>(response_[i] + 1,
                                              response_[i] + 2),
                    Return(1)))
          .WillOnce(DoAll(SetArrayArgument<0>(response_[i] + 2,
                                              response_[i] + kResponseSize),
                    Return(kResponseSize-2)));
    }
  }

};

}  // namespace sim_bus_worker_test

