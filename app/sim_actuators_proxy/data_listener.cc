// Copyright (C) 2017 A^3 - All Rights Reserved
//
// DDS data reader listener interface implementation
//

#include "sim_actuators_proxy/data_listener.h"

namespace sim_actuators_proxy {

template<class TelemetryType>
SimDataListener<TelemetryType>::SimDataListener(const std::string & topic_name,
                                 std::function<bool(TelemetryType)> callback)
                : topic_name_(topic_name),
                  callback_(callback) {
}

template<class TelemetryType>
bool SimDataListener<TelemetryType>::Init() {
  return true;
}

template<class TelemetryType>
void SimDataListener<TelemetryType>::on_data_available(DDSDataReader* reader) {
  typename TelemetryType::Seq data_seq;
  DDS_SampleInfoSeq info_seq;
  DDS_ReturnCode_t retcode;

  auto local_reader = TelemetryType::DataReader::narrow(reader);
  if (local_reader == nullptr) {
    printf("DataReader narrow error\n");
    return;
  }
  retcode = local_reader->take(
      data_seq, info_seq, DDS_LENGTH_UNLIMITED,
      DDS_ANY_SAMPLE_STATE, DDS_ANY_VIEW_STATE, DDS_ANY_INSTANCE_STATE);
  if (retcode == DDS_RETCODE_NO_DATA) {
    return;
  } else if (retcode != DDS_RETCODE_OK) {
    printf("take error %d\n", retcode);
    return;
  }
  for (auto i = 0; i < data_seq.length(); ++i) {
    if (info_seq[i].valid_data) {
      printf("Received data\n");
      callback_(&data_seq[i]);
    }
  }
  retcode = local_reader->return_loan(data_seq, info_seq);
  if (retcode != DDS_RETCODE_OK) {
    printf("return loan error %d\n", retcode);
  }
}

template<class TelemetryType>
bool SimDataListener<TelemetryType>::subscriber_shutdown
                                     (DDSDomainParticipant* participant) {
  DDS_ReturnCode_t retcode;

  if (participant != NULL) {
    retcode = participant->delete_contained_entities();
    if (retcode != DDS_RETCODE_OK) {
      printf("delete_contained_entities error %d\n", retcode);
      return false;
    }
    retcode = DDSTheParticipantFactory->delete_participant(participant);
    if (retcode != DDS_RETCODE_OK) {
      printf("delete_participant error %d\n", retcode);
      return false;
    }
  }

  /* RTI Connext provides the finalize_instance() method on
  domain participant factory for people who want to release memory used
  by the participant factory. Uncomment the following block of code for
  clean destruction of the singleton. */

  retcode = DDSDomainParticipantFactory::finalize_instance();
  if (retcode != DDS_RETCODE_OK) {
      printf("finalize_instance error %d\n", retcode);
      return false;
  }

  return true;
}

}  // namespace sim_actuators_proxy

