// Copyright (C) 2017 A^3 - All Rights Reserved
//
// DDS data reader listener interface
//

#ifndef APPS_SIM_ACTUATORS_PROXY_DATA_LISTENER_H_
#define APPS_SIM_ACTUATORS_PROXY_DATA_LISTENER_H_

#include <stdlib.h>

#include <functional>
#include <string>

#include "topic_type/Actuators.h"
#include "topic_type/ActuatorsSupport.h"

namespace sim_actuators_proxy {

template<class TelemetryType>
class SimDataListener : public DDSDataReaderListener {
 public:
  explicit SimDataListener(const std::string & topic_name,
                           std::function<bool(TelemetryType)> callback);
  virtual ~SimDataListener();

  virtual void on_requested_deadline_missed(
      DDSDataReader* /*reader*/,
      const DDS_RequestedDeadlineMissedStatus& /*status*/) {}
  virtual void on_requested_incompatible_qos(
      DDSDataReader* /*reader*/,
      const DDS_RequestedIncompatibleQosStatus& /*status*/) {}
  virtual void on_sample_rejected(
      DDSDataReader* /*reader*/,
      const DDS_SampleRejectedStatus& /*status*/) {}
  virtual void on_liveliness_changed(
      DDSDataReader* /*reader*/,
      const DDS_LivelinessChangedStatus& /*status*/) {}
  virtual void on_sample_lost(
      DDSDataReader* /*reader*/,
      const DDS_SampleLostStatus& /*status*/) {}
  virtual void on_subscription_matched(
      DDSDataReader* /*reader*/,
      const DDS_SubscriptionMatchedStatus& /*status*/) {}
  virtual void on_data_available(DDSDataReader* reader);

  bool Init();

 private:
  std::function<bool(TelemetryType*)> callback_;
  std::string topic_name_;

  bool subscriber_shutdown(DDSDomainParticipant *participant);
};

};  // namespace sim_actuators_proxy

#endif  // APPS_SIM_ACTUATORS_PROXY_DATA_LISTENER_H_
