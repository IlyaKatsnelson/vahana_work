/*
 * actuators_command_listener.h
 *
 *  Copyright (C) 2016-2017 A^3 - All Rights Reserved
 *
 *  Listener class to receive the DDS data on the VehicleActuatorsCommand topic
 */

#ifndef APPS_ACTUATORS_PROXY_ACTUATORS_COMMAND_LISTENER_H_
#define APPS_ACTUATORS_PROXY_ACTUATORS_COMMAND_LISTENER_H_

#include <ndds/ndds_cpp.h>
#include <ndds/ndds_namespace_cpp.h>

#include "topic_type/Actuators.h"
#include "topic_type/ActuatorsSupport.h"

class ActuatorsCommandListener : public DDSDataReaderListener{
 public:
  ActuatorsCommandListener();
  virtual ~ActuatorsCommandListener();

  virtual void on_requested_deadline_missed(
      DDSDataReader* /*reader*/,
      const DDS_RequestedDeadlineMissedStatus& /*status*/) {}

  virtual void on_requested_incompatible_qos(
      DDSDataReader* /*reader*/,
      const DDS_RequestedIncompatibleQosStatus& /*status*/) {}

  virtual void on_sample_rejected(
      DDSDataReader* /*reader*/,
      const DDS_SampleRejectedStatus& /*status*/) {}

  virtual void on_liveliness_changed(
      DDSDataReader* /*reader*/,
      const DDS_LivelinessChangedStatus& /*status*/) {}

  virtual void on_sample_lost(
      DDSDataReader* /*reader*/,
      const DDS_SampleLostStatus& /*status*/) {}

  virtual void on_subscription_matched(
      DDSDataReader* /*reader*/,
      const DDS_SubscriptionMatchedStatus& /*status*/) {}

  virtual void on_data_available(DDSDataReader* reader);
};

#endif /* APPS_ACTUATORS_PROXY_ACTUATORS_COMMAND_LISTENER_H_ */
