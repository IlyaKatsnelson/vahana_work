### What is this repository for? ###

* This repository contains container script files and related sources.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Make sure you install Docker on your platform and have is running
* Make sure you have [GIT LFS](https://confluence.atlassian.com/bitbucket/use-git-lfs-with-bitbucket-828781636.html) installed on your platform
* Get the sources from this repository
* The sources should be self-contained and platform independent
    - If they are not, please fix them or notify other people
* The containers assume that all GIT sources are first cloned to the local drive. 
* The local user directory is shared inside a container at the /host/Users/<user-name> mount point.

### Contribution guidelines ###

* File [Jira](https://vahana.atlassian.net/secure/Dashboard.jspa) issue
* Submit pull request once your code is done and tested
* Git Push

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
