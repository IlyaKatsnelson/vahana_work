#!/usr/bin/env python3
import serial
import argparse
import os
import sys
import binascii
import time

MOTOR_CONTROLLER_PAYLOAD_LEN = 18
MOTOR_CONTROLLER_PARAMETERS = 71

MSG_SET_GET_INDEX = 6
MSG_PARAMETER_INDEX = 7
MSG_STATUS_1 = 8
MSG_STATUS_2 = 9
MSG_COUNTER = 11

# Motor off std command
payload = [
    bytearray([0xff, 0xff, 0x80, 0x64, 0x00, 0x00, 0x00, 0x00, 0x02, 0xe2]),
    bytearray([0xff, 0xff, 0x80, 0x64, 0x00,0x00, 0x00, 0x01, 0x02, 0xe3]),
    bytearray([0xff, 0xff, 0x80, 0x64, 0x00,0x00, 0x00, 0x02, 0x02, 0xe4])
]

command_msg = [0xff, 0xff, 0x84, 0x10, 0x00, 0x00, 0x01, 0x03, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00]

# Controller configuration parameters
# parameter_id = value
configuration = [ [61, 102] ]

def payload_crc_walk():
  for crc in range(0x1000):
    msb, lsb = int(crc >> 8 & 0xff), int(crc & 0xff)
    yield bytearray([0xff, 0xff, 0x80, 0x10, 0x00, 0x00, 0x00, 0x00, msb, lsb])

def payload_address_walk():
  for i in range(0x100):
    address = int(i & 0xff)
    msb, lsb = int((0x27e + address) >> 8 & 0xff), int((0x27e + address) & 0xff)
    yield bytearray([0xff, 0xff, 0x80, address, 0x00, 0x00, 0x00, 0x00, msb, lsb])

def is_crc_ok(p):
  try:
    a = iter(p)
    payload_list = [int("".join(e),16) for e in zip(a,a)]
    return sum(payload_list[:-2]) & 0xfff == int(p[-4:],16)
  except Exception as e:
    print(e)
    return False

def set_address_0x10(moog):
  print('SET ADDRESS TO 0x10:', binascii.hexlify(payload_set_address_0x10[0]))
  for p in payload_set_address_0x10:
    moog.write(p)
    moog.flush()
    c = moog.read(100)
    if c:
      print('ADDRESS SET!')
      print('RESPONSE:', binascii.hexlify(c).decode('utf-8'))

def calculate_crc(data_array):
  crc = int(0)
  for d in data_array[:-2]:
    crc = (crc + d) & 0xffff

  crc = crc & 0x0fff

  data_array[-2] = (crc >> 8)
  data_array[-1] = (crc & 0xff)

def get_parameter(serial_port, parameter, verbose=False):
  '''
  '''
  with serial.Serial(serial_port, 921600, timeout=0.008) as moog:  # 0.008s ~ 100Hz, 0.016s ~ 50 Hz
    command_msg[MSG_SET_GET_INDEX] = 2
    command_msg[MSG_PARAMETER_INDEX] = parameter

    command_msg[MSG_COUNTER] = command_msg[MSG_COUNTER] + 1
    calculate_crc(command_msg)

    if (verbose):
      print('Get parameter:', command_msg)

    # It takes two commands to get the real value
    moog.write(bytearray(command_msg))
    moog.flush()
    c = moog.read(100)
    if (c and verbose):
      print('RESPONSE1:', binascii.hexlify(c).decode('utf-8'))

    time.sleep(.2)
    moog.write(bytearray(command_msg))
    moog.flush()
    c = moog.read(100)
    if (c and verbose):
      print('RESPONSE2:', binascii.hexlify(c).decode('utf-8'))

    time.sleep(0.2)
    if (c):
      return (c[MSG_STATUS_1] << 8 | c[MSG_STATUS_2])

def set_parameter(serial_port, parameter, verbose=False):
  '''
  parameter = [parameter,value] tuple
  '''
  with serial.Serial(serial_port, 921600, timeout=0.008) as moog:  # 0.008s ~ 100Hz, 0.016s ~ 50 Hz
    command_msg[MSG_SET_GET_INDEX] = 1
    command_msg[MSG_PARAMETER_INDEX] = int(parameter[0])

    command_msg[MSG_STATUS_1] = (int(parameter[1]) >> 8) & 0xff
    command_msg[MSG_STATUS_2] = int(parameter[1]) & 0xff

    calculate_crc(command_msg)

    if (verbose):
      print('Set parameter:', command_msg)

    # It takes two commands to get the real value
    moog.write(bytearray(command_msg))
    moog.flush()
    c = moog.read(100)
    if (c and verbose):
      print('RESPONSE:', binascii.hexlify(c).decode('utf-8'))

if __name__ == '__main__':

  parser = argparse.ArgumentParser(description='Magicall configuration tool')
  parser.add_argument('port', help='Serial port where the moog slave device is attached (eg. "/etc/ttyUSB0")')
  parser.add_argument('--a', action='store_const', const=0x10, help='Set address to value')
  parser.add_argument('--get-all', action='store_true', default=False, help='Flag to read all parameters')
  parser.add_argument('--get-parameter', type=int, default=None, help='Get this parameter number to')
  parser.add_argument('--set-parameter', nargs=2, help='Set this parameter number to value')
  parser.add_argument('--set-config', action='store_true', default=False, help='Set all the configurable parameters to proper values after boot. The controller looses changed values after power cycle')
  parser.add_argument('-v', action='store_true', default=False, help='Verbose. Print raw data')

  args = parser.parse_args()
  print(args)

  if args.port and os.path.exists(args.port):
    serial_port = args.port

    if (args.set_config):
      # configure the controller
      for parameter in configuration:
        set_parameter(serial_port, parameter, args.v)

    elif (args.get_all):
      # read all parameters
      for i in range(MOTOR_CONTROLLER_PARAMETERS):
        val = get_parameter(serial_port, i, args.v)
        if (val != None):
          print(" %d: %d" % (i, val))

    elif (args.get_parameter != None):
      val = get_parameter(serial_port, args.get_parameter, args.v)
      if (val != None):
        print(" %d: %d" % (args.get_parameter, val))

    elif (args.set_parameter != None):
      set_parameter(serial_port, args.set_parameter, args.v)

  else:
    print("Path \'%s\' doesn't exist" % args.port)
