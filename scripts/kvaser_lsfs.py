#!/usr/bin/env python3
# encoding: utf-8

'''
Copyright (C) 2017 A^3 - All Rights Reserved

Tools for Kvaser CAN channel usage
'''

import os
import re

mhydra_pattern = re.compile("mhydra[0-9]+")

pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]

for pid in pids:
    try:
        can_names = []
        cmd = open(os.path.join('/proc', pid, 'cmdline'), 'r').read()

        # See if fds contain links to mhydra
        fds = [fd for fd in os.listdir('/proc/%s/fd' % pid) if fd.isdigit()]
        for fd in fds:
            path = os.path.realpath(os.path.join('/proc', pid, 'fd', fd))
            find = mhydra_pattern.findall(path)
            if find:
                can_names.append(find[0])

        if len(can_names) > 0:
            print(cmd)
            for can in can_names:
                print(can)

    except IOError: # proc has already terminated
        continue
