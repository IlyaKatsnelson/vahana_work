#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import glob
import matplotlib.pyplot as plt
import numpy

control_surface0 = pd.DataFrame()
control_surface1 = pd.DataFrame()
control_surface2 = pd.DataFrame()
control_surface3 = pd.DataFrame()

#file_pattern = "/home/ilyakatsnelson/Downloads/ActuatorsTelemetry$RecordAll$VehicleDomain.csv"
#file_pattern = "/tmp/data/ActuatorsTelemetry*.csv"
file_pattern = "/home/ilyakatsnelson/Downloads/data/ActuatorsTelemetry*.csv"

for filename in glob.iglob(file_pattern, recursive=True):
    print("Found file: {}".format(filename))
    df = pd.read_csv(filename)

    # If append is done on the empty frame pandas transposes the data
    if len(control_surface0) == 0:
        control_surface0 = df['control_surface[0]$position_deg']
    else:
        control_surface0 = control_surface0.append(df['control_surface[0]$position_deg'])

    print("Total size on 0: {}".format(len(control_surface0)))

    # If append is done on the empty frame pandas transposes the data
    if len(control_surface1) == 0:
        control_surface1 = df['control_surface[1]$position_deg']
    else:
        control_surface1 = control_surface1.append(df['control_surface[1]$position_deg'])

    print("Total size on 1: {}".format(len(control_surface1)))

    # If append is done on the empty frame pandas transposes the data
    if len(control_surface2) == 0:
        control_surface2 = df['control_surface[2]$position_deg']
    else:
        control_surface2 = control_surface2.append(df['control_surface[2]$position_deg'])

    print("Total size on 2: {}".format(len(control_surface2)))

    # If append is done on the empty frame pandas transposes the data
    if len(control_surface3) == 0:
        control_surface3 = df['control_surface[3]$position_deg']
    else:
        control_surface3 = control_surface3.append(df['control_surface[3]$position_deg'])

    print("Total size on 3: {}".format(len(control_surface3)))

c0 = list(filter(lambda x: x > 20.0 or x < -20.0, control_surface0))
c1 = list(filter(lambda x: x > 20.0 or x < -20.0, control_surface1))
c2 = list(filter(lambda x: x > 20.0 or x < -20.0, control_surface2))
c3 = list(filter(lambda x: x > 20.0 or x < -20.0, control_surface3))

pd.DataFrame(c0).to_csv('control_surface0.csv')
pd.DataFrame(c1).to_csv('control_surface1.csv')
pd.DataFrame(c2).to_csv('control_surface2.csv')
pd.DataFrame(c3).to_csv('control_surface3.csv')

h0 = numpy.histogram(control_surface0, 120, (-30.0, 30.0))
h1 = numpy.histogram(control_surface1, 120, (-30.0, 30.0))
h2 = numpy.histogram(control_surface2, 120, (-30.0, 30.0))
h3 = numpy.histogram(control_surface3, 120, (-30.0, 30.0))

print(h0[1])
print(h0[0])

h = pd.DataFrame()
h['buckets'] = h0[1][:-1]
h['0'] = h0[0]
h['1'] = h1[0]
h['2'] = h2[0]
h['3'] = h3[0]
h.to_csv('control_surface_histogram.csv')

print(h)
print(h0)