#!/usr/bin/env python3
import serial
import argparse
import os
import sys
import binascii
import time

MOTOR_CONTROLLER_PAYLOAD_LEN = 18

MSG_PBA_INDEX = 2
MSG_ADDR_INDEX = 3
MSG_ANGLE_1 = 4
MSG_ANGLE_2 = 5
MSG_COUNTER = 7

command_msg = [0xff, 0xff, 0x84, 0x10, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00]

def payload_crc_walk():
  for crc in range(0x1000):
    msb, lsb = int(crc >> 8 & 0xff), int(crc & 0xff)
    yield bytearray([0xff, 0xff, 0x80, 0x10, 0x00, 0x00, 0x00, 0x00, msb, lsb])

def payload_address_walk():
  for i in range(0x100):
    address = int(i & 0xff)
    msb, lsb = int((0x27e + address) >> 8 & 0xff), int((0x27e + address) & 0xff)
    yield bytearray([0xff, 0xff, 0x80, address, 0x00, 0x00, 0x00, 0x00, msb, lsb])

def is_crc_ok(p):
  try:
    a = iter(p)
    payload_list = [int("".join(e),16) for e in zip(a,a)]
    return sum(payload_list[:-2]) & 0xfff == int(p[-4:],16)
  except Exception as e:
    print(e)
    return False

def calculate_crc(data_array):
  crc = int(0)
  for d in data_array[:-2]:
    crc = (crc + d) & 0xffff

  crc = crc & 0x0fff

  data_array[-2] = (crc >> 8)
  data_array[-1] = (crc & 0xff)

def monitor(serial_port, address, verbose=False):
  '''
  Keep running until the user stops
  '''
  with serial.Serial(serial_port, 921600, timeout=0.008) as moog:  # 0.008s ~ 100Hz, 0.016s ~ 50 Hz
    command_msg[MSG_ADDR_INDEX] = addres
    command_msg[MSG_PBA_INDEX] = 0xB0

    while(True):
      command_msg[MSG_COUNTER] = command_msg[MSG_COUNTER] + 1
      calculate_crc(command_msg)

      if (verbose):
        print('Cmd:', command_msg)

      moog.write(bytearray(command_msg))
      moog.flush()
      c = moog.read(100)
      if (c and verbose):
        print('RESPONSE:', binascii.hexlify(c).decode('utf-8'))

      time.sleep(0.2)

def set_value(serial_port, address, angle, brake, verbose=False):
  '''
  parameter = [parameter,value] tuple
  '''
  with serial.Serial(serial_port, 921600, timeout=0.008) as moog:  # 0.008s ~ 100Hz, 0.016s ~ 50 Hz
    command_msg[MSG_ADDR_INDEX] = address
    command_msg[MSG_PBA_INDEX] = 0x80 | (brake ? 0x01 : 0x02)

    calculate_crc(command_msg)

    if (verbose):
      print('Set parameter:', command_msg)

    # It takes two commands to get the real value
    moog.write(bytearray(command_msg))
    moog.flush()
    c = moog.read(100)
    if (c and verbose):
      print('RESPONSE:', binascii.hexlify(c).decode('utf-8'))

if __name__ == '__main__':

  parser = argparse.ArgumentParser(description='Tilt actuator configuration tool')
  parser.add_argument('port', help='Serial port where the moog slave device is attached (eg. "/etc/ttyUSB0")')
  parser.add_argument('--address', action='store_const', const=0x10, help='Tilt address to value')
  parser.add_argument('--set-brake', action='store_true', default=True, help='Tilt brake')
  parser.add_argument('--set-angle', type=int, default=0, help='Tilt angle')
  parser.add_argument('--monitor', action='store_true', default=False, help='Continuously monitor the state of Tilt')
  parser.add_argument('-v', action='store_true', default=False, help='Verbose. Print raw data')

  args = parser.parse_args()
  print(args)

  if args.port and os.path.exists(args.port):
    serial_port = args.port

    if (args.monitor):
      # monitor the state until Ctrl+C
      monitor(serial_port, args.address, args.v)

    elif (args.set_angle != None):
      # set the angle and brake
      set_value(serial_port, args.address, args.set_angle, args.set_brake, args.v)

    else:
      print("Not all parameters set")

  else:
    print("Path \'%s\' doesn't exist" % args.port)
