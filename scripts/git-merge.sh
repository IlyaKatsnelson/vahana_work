#!/bin/bash

#
# Go to develop branch, update it
# Go to a different (or this) branch,
# merge with develop

verbose=0

usage() {
    echo "Update specific [or this] branch with develop/main"
    echo "usage: `basename $0` [-v] <branch name>"
    echo "       `basename $0` [-v]"
    exit 1
}

if [ "$1" == "-h" -o "$1" == "-?" ]; then
    usage
    exit 1
fi

if [ "$1" == "-v" ]; then
    verbose=1
fi

num_arg=`expr $# - $verbose`
if [ $num_arg == 0 ]; then
    branch=`git branch | grep "*" | cut -d " " -f 2`
else
    args=("$@")
    branch=${args[`expr $verbose + 0`]}
fi

echo ""
echo "Synchronizing to branch $branch"
echo ""

git checkout develop
git pull --rebase
git checkout $branch
git merge develop
