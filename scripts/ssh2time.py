#!/usr/bin/env python3
"""
  Copyright (C) 2018 A^3 - All Rights Reserved

  Open the expect shells and read time.
"""

import os, sys, time, getopt, re
import signal
import pexpect

SSH_NEWKEY = '(?i)are you sure you want to continue connecting'
MC_COMMAND_PROMPT = 'root@intel-corei7-64:~# '
GW_COMMAND_PROMPT = 'vahana@vahana-hilsim-gateway:'
GCS_COMMAND_PROMPT = 'vahana@vahana-hilsim-gcs:'

ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')

PASSWORD = '(?i)password:'
mc_password = 'VahFly17\r'
vahana_password = 'n3st\r'

script_filename = "script.log"
fout = open(script_filename, "ab")

# Begin log with
#fout.write ('# %4d%02d%02d.%02d%02d%02d \n' % time.localtime()[:-3])

def open_connection(cmd, prompt, pw):
    p = pexpect.spawn(cmd)
    p.logfile = fout

    i = p.expect([pexpect.TIMEOUT, SSH_NEWKEY, prompt, PASSWORD])
    if i == 0: # Timeout
        print('ERROR! could not login with SSH. Here is what SSH said:')
        print(p.before, p.after)
        print(str(p))
        return None
    elif i == 1: # In this case SSH does not have the public key cached.
        p.sendline ('yes')
        p.expect (PASSWORD)
    elif i == 2:
        # This may happen if a public key was setup to automatically login.
        return p
    elif i == 3:
        p.sendline(pw)
        # Now we are either at the command prompt or
        # the login process is asking for our terminal type.
        i = p.expect ([pexpect.TIMEOUT, prompt])
        if i != 1:
            return None

    #p.sendline ('\n')
    return p

# ------------------------------------------------------------------
print("--- Open MC")
p_mc = open_connection("ssh -p 10002 root@10.1.2.147", MC_COMMAND_PROMPT, mc_password)
if (p_mc == None):
    print("ERROR! couldn't open MC connection")
    sys.exit (1)

print("--- Open GW")
p_gw = open_connection("ssh vahana@10.1.2.147", GW_COMMAND_PROMPT, vahana_password)
if (p_gw == None):
    print("ERROR! couldn't open GW connection")
    sys.exit (1)

print("--- Open SIM")
p_sim = open_connection("ssh -p 10023 root@10.1.2.147", MC_COMMAND_PROMPT, mc_password)
if (p_sim == None):
    print("ERROR! couldn't open SIM connection")
    sys.exit (1)

print("--- Open GCS")
# Open another connection to the GW and redirect
p_gcs = open_connection("ssh vahana@10.1.2.147", GW_COMMAND_PROMPT, vahana_password)
if (p_gcs == None):
    print("ERROR! couldn't open GW connection")
    sys.exit (1)
p_gcs.sendline("ssh vahana@192.168.1.2\r")
i = p_gcs.expect([pexpect.TIMEOUT, SSH_NEWKEY, GCS_COMMAND_PROMPT, PASSWORD])
if i == 0: # Timeout
    print('ERROR! could not login with SSH. Here is what SSH said:')
    print(p_gcs.before, p_gcs.after)
    print(str(p_gcs))
    sys.exit (1)
elif i == 1: # In this case SSH does not have the public key cached.
    p_gcs.sendline ('yes')
    p_gcs.expect (PASSWORD)
elif i == 2:
    # This may happen if a public key was setup to automatically login.
    pass
elif i == 3:
    p_gcs.sendline(vahana_password)
    # Now we are either at the command prompt or
    # the login process is asking for our terminal type.
    i = p_gcs.expect ([pexpect.TIMEOUT, GCS_COMMAND_PROMPT])
    if i != 1:
        sys.exit (1)

#p_gcs.sendline ('\n')

# ------------------------------------------------------------------
# Start processing

# GW and GCS systems add the ANSI coloring characters

while(True):
    p_mc.sendline("date -u\n")
    p_mc.expect ([pexpect.TIMEOUT, MC_COMMAND_PROMPT])
    if (len(p_mc.before.splitlines()) > 1):
        print("MC:\t{}".format(p_mc.before.splitlines()[-1]))
    p_mc.expect ([pexpect.TIMEOUT, MC_COMMAND_PROMPT])
    p_mc.sendline("hwclock -u\n")
    p_mc.expect ([pexpect.TIMEOUT, MC_COMMAND_PROMPT])
    if (len(p_mc.before.splitlines()) > 1):
        print("MC:\t{}".format(p_mc.before.splitlines()[-1]))
    p_mc.expect ([pexpect.TIMEOUT, MC_COMMAND_PROMPT])

    p_gw.sendline("date -u\n")
    p_gw.expect ([pexpect.TIMEOUT, GW_COMMAND_PROMPT])
    if (len(p_gw.before.splitlines()) > 1):
        print("GW:\t{}".format(p_gw.before.splitlines()[-2]))
    p_gw.expect ([pexpect.TIMEOUT, GW_COMMAND_PROMPT])

    p_sim.sendline("date -u\n")
    p_sim.expect ([pexpect.TIMEOUT, MC_COMMAND_PROMPT])
    if (len(p_sim.before.splitlines()) > 1):
        print("SIM:\t{}".format(p_sim.before.splitlines()[-1]))
    p_sim.expect ([pexpect.TIMEOUT, MC_COMMAND_PROMPT])
    p_sim.sendline("hwclock -u\n")
    p_sim.expect ([pexpect.TIMEOUT, MC_COMMAND_PROMPT])
    if (len(p_sim.before.splitlines()) > 1):
        print("SIM:\t{}".format(p_sim.before.splitlines()[-1]))
    p_sim.expect ([pexpect.TIMEOUT, MC_COMMAND_PROMPT])

    p_gcs.sendline("date -u\n")
    p_gcs.expect ([pexpect.TIMEOUT, GCS_COMMAND_PROMPT])
    if (len(p_gcs.before.splitlines()) > 1):
        print("GCS:\t{}".format(p_gcs.before.splitlines()[-2]))
    p_gcs.expect ([pexpect.TIMEOUT, GCS_COMMAND_PROMPT])

    print("# ------------------------------- #")

    time.sleep(1)

fout.close()

