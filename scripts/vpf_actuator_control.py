#!/usr/bin/env python3
import serial
import argparse
import os
import sys
import binascii
import time

MSG_PBA_INDEX = 2
MSG_ADDR_INDEX = 3
MSG_PARAMETER_INDEX = 4
MSG_DATA_1 = 5
MSG_DATA_2 = 6
MSG_DATA_3 = 7
MSG_COUNTER = 9

eeprom_command_msg = [0xFF, 0xFF, 0x01, 0xFF, 0x7D, 0x00, 0x00, 0x04, 0x03, 0x7F]
command_msg = [0xFF, 0xFF, 0x82, 0xFF, 0x06, 0xDB, 0x00, 0x03, 0xC4, 0x00, 0x05, 0x27]

# Controller configuration parameters
# parameter_id = value
configuration = [ [61, 102] ]

# Controller register values
registers = [ 0x00, 0x01, 0x02, 0x03, 0x04, 0x7D, 0x7E, 0x7F ]

def payload_crc_walk():
  for crc in range(0x1000):
    msb, lsb = int(crc >> 8 & 0xff), int(crc & 0xff)
    yield bytearray([0xff, 0xff, 0x80, 0x10, 0x00, 0x00, 0x00, 0x00, msb, lsb])

def payload_address_walk():
  for i in range(0x100):
    address = int(i & 0xff)
    msb, lsb = int((0x27e + address) >> 8 & 0xff), int((0x27e + address) & 0xff)
    yield bytearray([0xff, 0xff, 0x80, address, 0x00, 0x00, 0x00, 0x00, msb, lsb])

def is_crc_ok(p):
  try:
    a = iter(p)
    payload_list = [int("".join(e),16) for e in zip(a,a)]
    return sum(payload_list[:-2]) & 0xfff == int(p[-4:],16)
  except Exception as e:
    print(e)
    return False

def calculate_crc(data_array):
  crc = int(0)
  for d in data_array[:-2]:
    crc = (crc + d) & 0xffff

  crc = crc & 0x0fff

  data_array[-2] = (crc >> 8)
  data_array[-1] = (crc & 0xff)

def get_parameter(serial_port, address, parameter, verbose=False):
  '''
  '''
  if (not parameter in registers):
    print("Unsupported register 0x%X specified" % parameter)
    return

  with serial.Serial(serial_port, 921600, timeout=0.008) as moog:  # 0.008s ~ 100Hz, 0.016s ~ 50 Hz
    eeprom_command_msg[MSG_PBA_INDEX] = 0
    eeprom_command_msg[MSG_ADDR_INDEX] = address
    eeprom_command_msg[MSG_PARAMETER_INDEX] = parameter

    calculate_crc(eeprom_command_msg)

    if (verbose):
      print('Get parameter:', eeprom_command_msg)

    # It takes two commands to get the real value
    moog.write(bytearray(eeprom_command_msg))
    moog.flush()
    c = moog.read(100)
    if (c and verbose):
      print('RESPONSE:', binascii.hexlify(c).decode('utf-8'))

    time.sleep(0.2)
    if (c):
      return (c[MSG_DATA_1] << 16 | c[MSG_DATA_2] << 8 |c[MSG_DATA_3])

def set_parameter(serial_port, address, parameter, verbose=False):
  '''
  parameter = [parameter,value] tuple
  '''
  if (not parameter in registers):
    print("Unsupported register 0x%X specified" % parameter)
    return

  with serial.Serial(serial_port, 921600, timeout=0.008) as moog:  # 0.008s ~ 100Hz, 0.016s ~ 50 Hz
    eeprom_command_msg[MSG_PBA_INDEX] = 1
    eeprom_command_msg[MSG_ADDR_INDEX] = address
    eeprom_command_msg[MSG_PARAMETER_INDEX] = int(parameter[0])

    eeprom_command_msg[MSG_DATA_1] = (int(parameter[1]) >> 16) & 0xff
    eeprom_command_msg[MSG_DATA_2] = (int(parameter[1]) >> 8) & 0xff
    eeprom_command_msg[MSG_DATA_3] = int(parameter[1]) & 0xff

    calculate_crc(eeprom_command_msg)

    if (verbose):
      print('Set parameter:', eeprom_command_msg)

    # It takes two commands to get the real value
    moog.write(bytearray(eeprom_command_msg))
    moog.flush()
    c = moog.read(100)
    if (c and verbose):
      print('RESPONSE:', binascii.hexlify(c).decode('utf-8'))

def set_angle(serial_port, address, angle, verbose=False):
  '''
  '''
  with serial.Serial(serial_port, 921600, timeout=0.008) as moog:  # 0.008s ~ 100Hz, 0.016s ~ 50 Hz
    command_msg[MSG_PBA_INDEX] = 0x82
    command_msg[MSG_ADDR_INDEX] = address

    command_msg[4] = (int(angle/0.01709) >> 8) & 0x0f
    command_msg[5] = (int(angle/0.01709)) & 0xff

    command_msg[MSG_COUNTER] = command_msg[MSG_COUNTER] + 1
    calculate_crc(command_msg)

    if (verbose):
      print('Set parameter:', command_msg)

    # It takes two commands to get the real value
    moog.write(bytearray(command_msg))
    moog.flush()
    c = moog.read(100)
    if (c and verbose):
      print('RESPONSE:', binascii.hexlify(c).decode('utf-8'))

if __name__ == '__main__':

  parser = argparse.ArgumentParser(description='Tilt actuator configuration tool')
  parser.add_argument('port', help='Serial port where the moog slave device is attached (eg. "/etc/ttyUSB0")')
  parser.add_argument('--address', type=int, default=0xff, help='VPF address to value')
  parser.add_argument('--set-angle', type=float, default=0.0, help='VPF angle')
  parser.add_argument('--get-all', action='store_true', default=False, help='Flag to read all parameters')
  parser.add_argument('--get-parameter', type=int, default=None, help='Get this parameter number to')
  parser.add_argument('--set-parameter', nargs=2, help='Set this parameter number to value')
  parser.add_argument('--set-config', action='store_true', default=False, help='Set all the configurable parameters to proper values after boot')
  parser.add_argument('-v', action='store_true', default=False, help='Verbose. Print raw data')

  args = parser.parse_args()
  print(args)

  if (args.address == None):
    print("VPF address must be specified")
    sys.exit(0)

  if args.port and os.path.exists(args.port):
    serial_port = args.port

    if (args.set_config):
      # configure the controller
      for parameter in configuration:
        set_parameter(serial_port, args.address, parameter, args.v)

    elif (args.get_all):
      # read all parameters
      for register in registers:
        val = get_parameter(serial_port, args.address, register, args.v)
        if (val != None):
          print(" %d: %d" % (register, val))

    elif (args.get_parameter != None):
      val = get_parameter(serial_port, args.address, args.get_parameter, args.v)
      if (val != None):
        print(" %d: %d" % (args.get_parameter, val))

    elif (args.set_parameter != None):
      set_parameter(serial_port, args.address, args.set_parameter, args.v)

    elif (args.set_angle != None):
      set_angle(serial_port, args.address, args.set_angle, args.v)

  else:
    print("Path \'%s\' doesn't exist" % args.port)

  sys.exit(0)

  with serial.Serial(args.port, 921600, timeout=0.008) as moog:  # 0.008s ~ 100Hz, 0.016s ~ 50 Hz
    command_msg = [0xFF, 0xFF, 0x01, 0xFF, 0x7D, 0x00, 0x00, 0x04, 0x03, 0x7F]
    command_msg = [0xFF, 0xFF, 0x01, 0xFF, 0x7E, 0x00, 0x8F, 0x34, 0x04, 0x3F]
    command_msg = [0xFF, 0xFF, 0x01, 0xFF, 0x7F, 0x00, 0x3A, 0x4B, 0x04, 0x02]
    command_msg = [0xFF, 0xFF, 0x01, 0xFF, 0x00, 0x00, 0xAF, 0xC8, 0x04, 0x75]
    command_msg = [0xFF, 0xFF, 0x01, 0xFF, 0x01, 0x00, 0x01, 0x90, 0x03, 0x90]
    command_msg = [0xFF, 0xFF, 0x01, 0xFF, 0x02, 0x00, 0x00, 0x1D, 0x03, 0x1D]

    #moveto 30
    #command_msg = [0xFF, 0xFF, 0x82, 0xFF, 0x06, 0xDB, 0x00, 0x03, 0xC4, 0x00, 0x05, 0x27]
    #command_msg[MSG_ADDR_INDEX] = 0x20

    #calculate_crc(command_msg)

    if (args.v):
      print('Set parameter:', command_msg)

    # It takes two commands to get the real value
    moog.write(bytearray(command_msg))
    moog.flush()
    c = moog.read(100)
    if (c and args.v):
      print('RESPONSE:', binascii.hexlify(c).decode('utf-8'))
